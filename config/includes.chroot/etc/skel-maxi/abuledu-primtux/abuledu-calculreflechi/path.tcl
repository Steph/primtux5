global iwish progaide basedir Home abuledu prof baseHome

set abuledu 0
set prof 0
set basedir [file dir $argv0]
cd $basedir
if {$env(HOME) == "c:\\"} {
    set Home [file join $basedir]
    
} else {
    set Home [file join $env(HOME) leterrier oeufs]
}
set baseHome $Home


switch $tcl_platform(platform) {
    unix {
	set progaide runbrowser
	#set progaide dillo
	set iwish wish
	}
    windows {
	set progaide shellexec.exe
	set iwish wish
	}
	}

proc setwindowsusername {} {
    global user LogHome
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm title .utilisateur [mc {Utilisateur}]
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text [mc {Quel est ton nom?}] -background grey
    pack .utilisateur.frame.labobj -side top 

    listbox .utilisateur.frame.listsce -yscrollcommand ".utilisateur.frame.scrollpage set" -width 15 -height 10
    scrollbar .utilisateur.frame.scrollpage -command ".utilisateur.frame.listsce yview" -width 7
    pack .utilisateur.frame.listsce .utilisateur.frame.scrollpage -side left -fill y -expand 1 -pady 10
    bind .utilisateur.frame.listsce <ButtonRelease-1> "verifnom %x %y"
    foreach i [lsort [glob [file join $LogHome *.log]]] {
    .utilisateur.frame.listsce insert end [string map {.log \040} [file tail $i]]
    }

    button .utilisateur.frame.ok -background gray75 -text [mc {Ok}] -command "interface; destroy .utilisateur"
    pack .utilisateur.frame.ok -side top -pady 70 -padx 10
}

proc verifnom {x y} {
    global env user LogHome

set ind [.utilisateur.frame.listsce index @$x,$y]
set nom [string trim [.utilisateur.frame.listsce get $ind]]
    if {$nom !=""} {
	set env(USER) $nom
	set user [file join $LogHome $nom.log]
    }
}

proc inithome {} {
    global baseHome basedir Home
    variable repert
    set f [open [file join $baseHome reglages repert.conf] "r"]
    set repert [gets $f]
    close $f
    
    switch $repert {
	0 {set Home [file join $baseHome]}
	1 {set Home [file join $basedir]}
    }
}


proc loadlib {plateforme} {


switch $plateforme {
    unix {
    package require Img
    }
    windows {
    package require Img
    }
}

}


proc initapp {plateforme} {
    global Home basedir baseHome
    
    
    if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file copy -force [file join reglages] [file join $Home]
	file copy -force [file join data] [file join $Home]
    }
    
    switch $plateforme {
	unix {
	    if {![file exists [file join $baseHome log]]} {
		file mkdir [file join $baseHome log]
	    }
	    
	}
	windows {
	    if {![file exists [file join oeufs log]]} {
		file mkdir [file join oeufs log]
	    }
	    
    	}
    }
    
}

proc changehome {} {
global Home basedir baseHome
variable repert
set f [open [file join $baseHome reglages repert.conf] "w"]
puts $f $repert
close $f

switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}

}


proc initlog {plateforme ident} {
global LogHome user Home baseHome
switch $plateforme {
    unix {
	set LogHome [file join $baseHome log]

    }
    windows {
	set LogHome [file join oeufs log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome oeufs.log]
     }
if {![file exists [file join $user]]} {
set f [open [file join $user] "w"]
close $f
}
}
