#!/bin/sh
#contour.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 André Connes <andre.connes@wanadoo.fr>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : contour.tcl
#  Author  : André Connes <andre.connes@wanadoo.fr>
#  Modifier:
#  Date    : 15/04/2003 modifié le 06/12/2004
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: contour.tcl,v 1.16 2006/03/25 00:55:15 abuledu_francois Exp $
#  @author     André Connes
#  @modifier   
#  @project    Le terrier
#  @copyright  André Connes
# 
#***********************************************************************
source contour.conf
source msg.tcl
source aider.tcl
source fonts.tcl
source lanceapplication.tcl

  # gestion de l'espace de l'utilisateur

  #
  # langue par defaut
  #
  set f [open [file join [pwd] reglages lang.conf] "r"]
  gets $f lang
  close $f

  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

#source fin_sequence.tcl

  #
  # taille des numeros par defaut
  #
  set f [open [file join [pwd] reglages charsize.conf] "r"]
  gets $f glob(charSize)
  close $f

  # couleur des numeros et des traits
  set f [open [file join $glob(home_reglages) couleur.conf] "r"]
  gets $f glob(couleur)
  close $f

  #
  # Etat des boutons des menus
  #
  set glob(etat_boutons) normal

  #set nom_utilisateur defaut

  #set classe defaut
  
  set glob(etat_boutons) normal

proc setlang {lang} {
  global env glob
  set env(LANG) $lang
  catch {set f [open [file join [pwd] reglages lang.conf] "w"]}
  puts $f [file rootname [file tail $lang]]
  close $f
}

#########################################################

proc setRegler {} {
  # utilise par contour comme la commande touch
  set f [open [file join [pwd] reglages a_regler.conf] "w"]
  puts $f [clock seconds]
  close $f
} ;# setRegler


##########################################################

proc tailleNumeros { t } {
  global glob
  set f [open [file join $glob(home_reglages) charsize.conf] "w"]
  puts $f $t
  close $f
  set glob(charSize) $t
}

##########################################################

proc epaisseurTraits { e } {
  global glob
  set f [open [file join $glob(home_reglages) epaisseur.conf] "w"]
  puts $f $e
  close $f
  set glob(epaisseur) $e
} ;# epaisseurTraits 

##########################################################

proc color { c } {
  # traduction fr <-> en
  global glob
  set colors [list black yellow red green blue]
  set i [lsearch $glob(couleurs) $c]
  set color [lindex $colors $i]
  return $color
}

proc couleurNumerosTraits { c } {
  global glob
  set f [open [file join $glob(home_reglages) couleur.conf] "w"]
  puts $f $c
  close $f
  set glob(couleur) $c
} ;# couleurNumerosTraits 

#########################################################

proc ok_nouveauDepart { } {
  global glob var_depart
  # valeur numerique attendue
  if {[catch {expr $var_depart} r]} {
    set depart 1
  } else {
    set depart $r
  }
  set f [open [file join $glob(home_reglages) depart.conf] "w"]
  puts $f $depart
  close $f
  set glob(depart) $depart
}

proc nouveauDepart { } {
  destroy .top_taille
  set tt [toplevel .top_depart]
  raise .top_depart
  wm title $tt [mc "NouveauDepart"]
  wm geometry $tt 420x140+380+560
  entry $tt.ent_depart -width 5 -textvariable var_depart
  label $tt.lab_valide -text "[mc Valider_svp]"
  focus $tt.ent_depart
  $tt.ent_depart delete 0 end
  bind $tt.ent_depart <Return> "ok_nouveauDepart; destroy .top_depart"
  bind $tt.ent_depart <KP_Enter> "ok_nouveauDepart; destroy .top_depart"
  pack $tt.ent_depart -padx 20 -pady 30
  pack $tt.lab_valide -padx 20 -pady 5
} ;# nouveauDepart 

##########################################################

proc ok_pasProgression { } {
  global glob var_progression
  # valeur numerique attendue
  if {[catch {expr $var_progression} r]} {
    set progression 1
  } else {
    set progression $r
  }
  set f [open [file join $glob(home_reglages) progression.conf] "w"]
  puts $f $progression
  close $f
  set glob(progression) $progression
}

proc pasProgression { } {
  destroy .top_taille
  set tt [toplevel .top_progression]
  raise .top_progression
  wm title $tt [mc "PasProgression"]
  wm geometry $tt 420x140+380+560
  entry $tt.ent_progression -width 5 -textvariable var_progression
  label $tt.lab_valide -text "[mc Valider_svp]"
  focus $tt.ent_progression
  $tt.ent_progression delete 0 end
  bind $tt.ent_progression <Return> "ok_pasProgression; destroy .top_progression"
  bind $tt.ent_progression <KP_Enter> "ok_pasProgression; destroy .top_progression"
  pack $tt.ent_progression -padx 20 -pady 30
  pack $tt.lab_valide -padx 20 -pady 5
} ;# pasProgression 

##########################################################

proc setsouris { ms } {
  global glob
  set f [open [file join $glob(home_reglages) mode_souris.conf] "w"]
  puts $f $ms
  close $f
}

#########################################################

proc charge_vrac {} {
  global tcl_platform
  set types {{{Fichiers images} {.jpg .png .gif}}}
  set file [tk_getOpenFile -initialdir [file join images _vrac] -filetypes $types]
  if {$file != ""} {
    if {![file exists [file join images _vrac [file tail $file]]]} {
      file copy $file [file join images _vrac [file tail $file]]
      if { $tcl_platform(platform) == "unix" } {
        catch {exec mogrify -geometry 600x600 [file join images _vrac [file tail $file]] }
      }
    } else {
      #tk_messageBox -message "Le fichier ne sera pas copi� dans le r�pertoire images car il existe d�j� un fichier du m�me nom"
    }
    lanceappli editeur_contour.tcl _vrac [file tail $file]
  }
}

#########################################################

proc charge_image {} {
  set types {{{Fichiers images} {.jpg .png .gif}}}
  set file [tk_getOpenFile -initialdir [file join images] -filetypes $types]
  set dossier_images [lindex [split [file dirname $file] "/"] end ]
  set fichier_image [file tail $file]
  lanceappli editeur_contour.tcl $dossier_images $fichier_image
}

#########################################################
#
#               main_loop
#
#########################################################

proc main_loop {} {
  global . glob env

  #
  # Creation du menu utilise comme barre de menu:
  #
  catch {destroy .menu}
  catch { destroy .frame.c}
  catch {destroy .frame}
  menu .menu -tearoff 0

  #
  # Creation du menu Fichier
  #
  menu .menu.fichier -tearoff 0
  .menu add cascade -state $glob(etat_boutons) \
	-label [mc Fichier] -menu .menu.fichier

  set etat_fichier "normal"

  .menu.fichier add command -label [mc Ouvrir] -command charge_image
  .menu.fichier add command -label [mc Ouvrir_vrac] -command charge_vrac
  .menu.fichier add command -label [mc gerer_images] -command "lanceappli gerer_images.tcl"
  .menu.fichier add command -label [mc Quitter] -command exit

  #
  # Creation du menu Reglages
  #
  menu .menu.reglages -tearoff 0
  .menu add cascade -state $glob(etat_boutons) \
	-label [mc Reglages] -menu .menu.reglages

  set etat_reglages "normal"

  # imposer les reglages aux utilisateurs
  menu .menu.reglages.aRegler -tearoff 0 
  .menu.reglages add cascade -label "[mc ARegler]" -menu .menu.reglages.aRegler

    .menu.reglages.aRegler add radio -label "Oui" -command "setRegler"
    .menu.reglages.aRegler add radio -label "Non" -command ""

  # taille des numeros
  menu .menu.reglages.numeros -tearoff 0
  .menu.reglages add cascade -label [mc "TailleNumeros"] -menu .menu.reglages.numeros

     foreach i [list 10 12 14 16] {
      .menu.reglages.numeros add radio -label $i -variable i -command "tailleNumeros $i"
    }  

  # couleur des numeros et des traits
  menu .menu.reglages.couleur -tearoff 0
  .menu.reglages add cascade -label [mc "CouleurNumerosTraits"] -menu .menu.reglages.couleur

     foreach i $glob(couleurs) {
      .menu.reglages.couleur add radio -label $i -variable i -command "couleurNumerosTraits $i"
    }  
  
  # epaisseur des traits
  menu .menu.reglages.epaisseur -tearoff 0
  .menu.reglages add cascade -label [mc "EpaisseurTraits"] -menu .menu.reglages.epaisseur

     foreach i [list 1 2 3 4 5] {
      .menu.reglages.epaisseur add radio -label $i -variable i -command "epaisseurTraits $i"
    }  
  
  # translation du depart
  .menu.reglages add command -label [mc "NouveauDepart"] -command "nouveauDepart"
  
  # pas de progression
  .menu.reglages add command -label [mc "PasProgression"] -command "pasProgression"
  
  # souris
  menu .menu.reglages.souris -tearoff 0 
  .menu.reglages add cascade -label "[mc Souris]" -menu .menu.reglages.souris

    set ms "Survoler"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"
    set ms "Cliquer"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"
    set ms "Double-cliquer"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"
    set ms "Pas-de-souris"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"

  # langues
  menu .menu.reglages.lang -tearoff 0 
  .menu.reglages add cascade -label "[mc Langue]" -menu .menu.reglages.lang

  foreach i [glob [file join  [pwd] msgs *.msg]] {
    set langue [string map {.msg "" } [file tail $i]]
    .menu.reglages.lang add radio -label $langue -variable langue -command "setlang $langue; lanceappli editeur.tcl 0"
  }

  #
  # Creation du menu aide
  #
  menu .menu.aide -tearoff 0
  .menu add cascade -state $glob(etat_boutons) \
	-label [mc Aide] -menu .menu.aide
  set l_langues [glob  [file join [pwd] aides aide.*.html]]
  foreach langue $l_langues {
    set lang [lindex [split $langue "."] end-1]
    .menu.aide add command -label "[mc Aide] $lang" -command "aider $lang"
  }
  .menu.aide add command -label [mc {A_propos ...}] -command "source apropos.tcl"

  . configure -menu .menu

  #######################################################################"
  frame .frame -background blue
  pack .frame -side top -fill both -expand yes
  ###################On crée un canvas####################################

  canvas .frame.image -bg blue -highlightbackground blue
  pack .frame.image -expand true

  #
  # afficher l'image du terrier
  #
  set myimage [image create photo -file sysdata/background.png]
  label .frame.image.imagedisplayer -image $myimage -background blue
  grid .frame.image.imagedisplayer -column 3 -row 0 -sticky e

} ;# main_loop

########################################################################"

  # Nom de l'utilisateur par défaut sous windows
  if {$glob(platform) == "windows"} {
    set nom eleve
    # sauver le réglage du nom
    catch {set f [open [file join $glob(home_contour) reglages trace_user] "w"]}
    puts $f "$glob(trace_dir)/$nom"
    close $f
  }

bind . <Control-q> {exit}

#wm resizable . 0 0
wm geometry . 400x400+0+0
. configure -background blue
wm title . "[mc title_ed]"

main_loop
