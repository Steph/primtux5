$c bind enveloppe <1> "startdim $c %x %y"
$c bind enveloppe <B1-Motion> "dimDrag $c %x %y"
$c bind enveloppe <ButtonRelease-1> "stopdim $c %x %y"

####################################"
proc startdim {c x y} {
global envparam lcord
#@ global envparam startx lcord
set lcord [$c coords enveloppe]
#on ne redimensionne l'enveloppe que si on a affaire � un seul objet, pas un groupe
if {[llength [$c find withtag cible]] < 2} {
sideenveloppe $c $x $y
}
}
#################################################


proc dimDrag {c x y} {
global envparam startx lcord cible tid starty

if {[llength [$c find withtag cible]] < 2} {

            set lcord [$c coords enveloppe]
            set imgcor [$c bbox cible]
            set id [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
#suivant le c�t�,on redimensionne l'enveloppe, si x et y ne d�passent pas les valeurs limites, et par pas de 10
                switch $envparam {
                    0 {
                       if {$x < [lindex $lcord 2] } {
                             if {[expr abs($x - $startx)] > 10} {
                                     $c coords enveloppe [expr [lindex $lcord 0]+ $x - $startx] [expr [lindex $lcord 1] ] [expr [lindex $lcord 2] ] [expr [lindex $lcord 3] ] 
                                     set startx $x
                                     }
                       }
                       }

                    1 {
                       if {$y < [lindex $lcord 3] } {
                             if {[expr abs($y - $starty)] > 10} {
                                     $c coords enveloppe [expr [lindex $lcord 0]] [expr [lindex $lcord 1]  + $y - $starty] [expr [lindex $lcord 2] ] [expr [lindex $lcord 3] ] 
                                     set starty $y
                                     }
                       }
                       }

                    2 {
                       if {$x > [lindex $lcord 0] } {
                             if {[expr abs($x - $startx)] > 10} {
                                   $c coords enveloppe [expr [lindex $lcord 0]] [expr [lindex $lcord 1] ] [expr [lindex $lcord 2]+ $x - $startx] [expr [lindex $lcord 3] ] 
                                   set startx $x
                                   }
                       }
                       }

                     3 {
                       if {$y > [lindex $lcord 1] } {
                             if {[expr abs($y - $starty)] > 10} {
                                   $c coords enveloppe [expr [lindex $lcord 0]] [expr [lindex $lcord 1] ] [expr [lindex $lcord 2]] [expr [lindex $lcord 3] + $y - $starty] 
                                   set starty $y
                                   }
                       }
                       }

                 }


}
}

proc stopdim {c x y} {
global lcord tid
if {[llength [$c find withtag cible]] < 2} {
# pour chaque objet du groupe
   foreach tg [$c find withtag cible] {
   if {[lsearch -regexp [$c gettags $tg] uident*]!=-1} {
	set id [lindex [$c gettags $tg] [lsearch -regexp [$c gettags $tg] uident*]]
	set typ [lindex [$c gettags $tg] [lsearch -regexp [$c gettags $tg] type*]]
#on redessine l'objet
      switch $typ {
         typetext {
         retailletexte $c $lcord $id
         }
         typeimage {
         stretchimage $c $x $y
         }
	 typebouton2 {
	stretchbouton2 $c $x $y
	 }
	 typepolygon {
         stretchpolygon $c $x $y
         }
	 typebouton1 {
         stretchpolygon $c $x $y
         }
         default {
         }
     }
  }
}
}
}

proc redessineenveloppe {c id} {
$c coords enveloppe [expr [lindex [$c bbox $id] 0] -3] [expr [lindex [$c bbox $id] 1]-3] [expr [lindex [$c bbox $id] 2]+3] [expr [lindex [$c bbox $id] 3]+3]  
}


proc createenveloppe {c id} {
#id identificateur de l'objet
$c addtag enveloppe withtag [$c create rect [expr [lindex [$c bbox $id] 0] -3] [expr [lindex [$c bbox $id] 1]-3] [expr [lindex [$c bbox $id] 2]+3] [expr [lindex [$c bbox $id] 3]+3]  -outline red -activewidth 6]
}

proc sideenveloppe {c x y} {
global envparam
global startx starty
set envparam 4
#positionne la variable envparam en fonction de la position de la souris pour d�terminer le c�t� concern�
   if {[expr abs($x- [lindex [$c coords enveloppe] 2])] < 3} {
      set envparam 2
      }
   if {[expr abs($x- [lindex [$c coords enveloppe] 0])] < 3} {
      set envparam 0
      }
   if {[expr abs($y- [lindex [$c coords enveloppe] 1])] < 3} {
      set envparam 1
      }
   if {[expr abs($y- [lindex [$c coords enveloppe] 3])] < 3} {
      set envparam 3
      }

   set startx $x  
   set starty $y  
}


