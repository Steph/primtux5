Le secret de ma�tre Cornille (8).
Dans la vie de ma�tre Cornille il y avait quelque chose qui n'�tait pas clair. Depuis longtemps, personne, au village, ne lui portait plus de bl�, et pourtant les ailes de son moulin allaient toujours leur train comme devant ... Le soir, on rencontrait par les chemins le vieux meunier poussant devant lui son �ne charg� de gros sacs de farine. 
� - Bonnes v�pres, ma�tre Cornille ! lui criaient les paysans ; �a va donc toujours, la meunerie ? 
- Toujours, mes enfants, r�pondait le vieux d'un air gaillard. Dieu merci, ce n'est pas l'ouvrage qui nous manque. �
Alphonse Daudet.