Le secret de ma�tre Cornille (17).
Puis se tournant vers nous : � Ah ! je savais bien que vous me reviendriez... Tous ces minotiers sont des voleurs. �
Nous voulions l'emporter en triomphe au village : 
� Non, non, mes enfants ; il faut avant tout que j'aille donner � manger � mon moulin... Pensez donc ! il y a si longtemps qu'il ne s'est rien mis sous la dent ! �
Et nous avions tous des larmes dans les yeux de voir le pauvre vieux se d�mener de droite et de gauche, �ventrant les sacs, surveillant la meule, tandis que le grain s'�crasait et que la fine poussi�re de froment s'envolait au plafond.
Alphonse Daudet.