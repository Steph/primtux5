#!/bin/sh
#eval.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************


proc enregistreval {titre categorie quoi score home acti user exoconf dossier} {
global listexo
     	set date_heure "[clock format [clock seconds] -format "%d/%m/%Y"] - [clock format [clock seconds] -format "%H:%M"]"
set listeval \173$titre\175\040$categorie\040\173$quoi\175\040\173$score\175\040\173$home\175\040\173$acti\175\040\173$date_heure\175\040\173$exoconf\175\040\173$dossier\175


    #exec /usr/bin/leterrier_log --message=$listeval --logfile=$user
    wm title . $user
    set f [open [file join $user] "a+"]
    puts $f $listeval
    close $f
}




