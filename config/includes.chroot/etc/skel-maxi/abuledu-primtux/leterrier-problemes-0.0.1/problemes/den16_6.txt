set etapes 1
set niveaux {1}
::1
set niveau 1
set ope {{1 3} {0 0} {1 9}}
set interope {{1 10 1} {0 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set operations {{0+0}}
set volatil 0
set editenon "([expr $ope1*100 + $ope2*10 + $ope3] euros sont représentés.)"
set enonce "La monnaie.\nCombien ai-je d'euros?"
set cible {{0 0 {} 0}}
set intervalcible 0
set taillerect 80
set orgy 0
set orgxorig 0
set orgsourcey 100
set orgsourcexorig 100
set source [list [list euro100.gif [expr $ope1] 10 1] [list euro10.gif [expr $ope2] 10 1] [list euro1.gif [expr $ope3] 10 1]]
set orient 0
set labelcible {{}}
set quadri 0
set ensembles {}
set reponse [list [list {1} [list {J'ai} [expr $ope1*100 + $ope2*10 + $ope3] {euros.}]]]
set dessin 0
set canvash 340
set c1height 60
set opnonautorise {}
::
