#!/bin/sh
#problemes.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2007 David Lucardi <davidlucardi@aol.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier:
#  Date    : 01/06/2007
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi 01/06/2007
# 
#  *************************************************************************

source utils.tcl

proc verifdessin {} {
global flagok log resultdessin
#####################################################################
		if {$resultdessin != [.frame.c.scale get]} {
		.frame.c configure -bg red
		lappend log [concat Erreur sur le dessin.]
		set flagok 0
		} else {
		.frame.c configure -bg yellow
		}

########################################################################
if {$flagok == 1} {lappend log "Le dessin est juste."}
}

proc set_arrive {scaleorig scaledelta} {
set coord [expr int(([.frame.c.scale get]-$scaleorig)*780/($scaledelta-$scaleorig))  + (800/($scaledelta-$scaleorig))*($scaledelta-[.frame.c.scale get] )/($scaledelta + 0)]
.frame.c coords arriv�e $coord 20 [expr $coord] 50
}

proc tracecanvas {scale0 scale1 scale2 dpt placearriv} {
global sysFont
.frame.c create line 0 0 0 30 -fill green -width 4 -tags "depart canvas"
.frame.c create image 0 0 -image [image create photo -file [file join sysdata borne.gif]] -tags "borne canvas"
.frame.c create line 0 0 0 30 -fill blue -width 4 -tags "arriv�e canvas"
scale .frame.c.scale -orient horizontal -length 800 -from $scale0 -to $scale1 -tickinterval $scale2 -font $sysFont(vs) 
.frame.c create window 400 80 -window .frame.c.scale -tags canvas
.frame.c.scale set $dpt
set coord [expr int($dpt*780/$scale1) + (800/($scale1-$scale0))*($scale1-[.frame.c.scale get])/$scale1 + 00]


set coord [expr int(($dpt-$scale0)*780/($scale1-$scale0)) + (800/($scale1-$scale0))*($scale1-[.frame.c.scale get])/($scale1 + 0)]
.frame.c coords depart $coord 20 $coord 50
.frame.c coords borne  [expr int((0-$scale0)*780/($scale1-$scale0)) + (800/($scale1-$scale0))*($scale1-0)/($scale1 + 0)] 45
if {$placearriv != "none"} {
set coord [expr int(($placearriv-$scale0)*780/($scale1-$scale0)) + (800/($scale1-$scale0))*($scale1-$placearriv)/($scale1 + 0)]
.frame.c coords arriv�e  $coord 20 $coord 50
} else {
set_arrive $scale0 $scale1
bind .frame.c.scale <ButtonRelease-1> "set_arrive $scale0 $scale1"
}
}

proc montredessin {} {
global reponse operations sysFont
.framebottom0.verif configure -text "V�rifier" -command "verif"
.frame.c itemconfigure canvas -state normal
button .framebottom0.corrige -command "corrige" -text "Corriger" -font $sysFont(tb)
pack .framebottom0.corrige -side left -padx 60 -pady 10
if {[lindex $operations 0] != "0+0"} {
catch {.framebottom.ope1 configure -state disabled}
}
catch {
for {set z 0} {$z < [llength $reponse]} {incr z} {
set repons [lindex $reponse $z]
for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
.framebottom$z.reponse$i configure -state disabled
}
}
}
}

proc corrige {} {
global reponse operations sysFont
.frame.c itemconfigure canvas -state hidden
.framebottom0.verif configure -command "montredessin" -text "Continuer" -font $sysFont(tb)
catch {
destroy button .framebottom0.corrige
if {[lindex $operations 0] != "0+0"} {
.framebottom.ope1 configure -state normal
}
for {set z 0} {$z < [llength $reponse]} {incr z} {
set repons [lindex $reponse $z]
for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
.framebottom$z.reponse$i configure -state normal
}
}
}
}

##################################################################################
proc main {} {
global sysFont niveau operations reponse arg page etapes niv enonce resultdessin user source Home
wm geometry . +0+0

. configure -bg grey
catch {
destroy .frametop
destroy .frame
destroy .framebottom
}
frame .frametop -width 800 -bg grey
pack .frametop -side top -fill both -expand yes
frame .frame -width 800 -bg grey
pack .frame -side top -fill both -expand yes
frame .framebottom -width 800 -bg grey
pack .framebottom -side top -fill both -expand yes

set f [open [file join $Home problemes $arg] "r"]

set tmp [gets $f]
eval $tmp
	while {$tmp != "::$page"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {eval $tmp}
	}
close $f
catch {
destroy .frametop.enonce
}

label .frametop.enonce -bg grey -justify left -font $sysFont(tb) -text $enonce
pack .frametop.enonce -expand true -side left -anchor w
set texte [string map {\n \040 \| \040 \< \040 \> \040} $enonce]
bind .frametop.enonce <ButtonRelease-1> "speaktexte \173$texte\175"
if {$niv != "none"} {set niveau $niv}
	if {$niveau <= 4} {
	catch {
	destroy .frame.c
	}
	canvas .frame.c -width 800 -height $canvash -bg grey
	pack .frame.c -expand true
	.frame.c create text 400 10 -font $sysFont(tb) -text "D�place le curseur" -tags "canvas sp1"
	.frame.c bind sp1 <ButtonRelease-1> "speaktexte \173D�place le curseur\175"

	}

	for {set z 0} {$z < [llength $reponse]} {incr z} {
	catch {
	destroy .framebottom$z
	}
	}
	for {set z 0} {$z < [llength $reponse]} {incr z} {
	frame .framebottom$z -width 800 -bg grey
	pack .framebottom$z -side top -fill both -expand yes
	}

	if {$niveau >= 2 && [lindex $operations 0] != "0+0"} {
	label .framebottom.consigne1 -bg grey -justify left -font $sysFont(tb) -text "Ecris l'operation :"
	pack .framebottom.consigne1 -expand true -side top -anchor w -pady 10
	bind .framebottom.consigne1 <ButtonRelease-1> "speaktexte \173Ecris l'op�ration\175"
	entry .framebottom.ope1 -justify center -font $sysFont(tb) 
	pack .framebottom.ope1 -expand true -side top -anchor w -pady 10
	}

	if {$niveau >= 1} {
	label .framebottom.consigne2 -bg grey -justify left -font $sysFont(tb) -text "Ecris la reponse :"
	pack .framebottom.consigne2 -expand true -side top -anchor w
	bind .framebottom.consigne2 <ButtonRelease-1> "speaktexte \173Ecris la r�ponse\175"

		for {set z 0} {$z < [llength $reponse]} {incr z} {
		set repons [lindex $reponse $z]
		set compt 0
		set ind [lindex [lindex $repons 0] $compt]
			for {set i 0} {$i < [llength [lindex $repons 1]]} {incr i} {
 				if {$i != $ind } {
				label .framebottom$z.reponse$i -bg grey -justify left -font $sysFont(tb) -text [lindex  [lindex $repons 1] $i]
				bind .framebottom$z.reponse$i <ButtonRelease-1> "speaktexte \173[lindex  [lindex $repons 1] $i]\175"
				} else {
				entry .framebottom$z.reponse$i -justify center -font $sysFont(tb) -width 4
				incr compt
				set ind [lindex [lindex $repons 0] $compt]
				}
			pack .framebottom$z.reponse$i -side left -anchor w -padx 2
			}
		}
	}
if {$niveau <=4} {
	tracecanvas [lindex $scaleb 0] [lindex $scaleb 1] [lindex $scaleb 2] $dpt $placearriv
	}
if {$niveau != "3"} {
button .framebottom0.verif -command "verif" -text "V�rifier" -font $sysFont(tb)
pack .framebottom0.verif -side left -padx 60 -pady 10
} else {
button .framebottom0.verif -command "montredessin" -text "Continuer" -font $sysFont(tb)
pack .framebottom0.verif -side left -padx 60 -pady 10
.frame.c itemconfigure canvas -state hidden
}
if {$niveau >=4} {
for {set z [expr [llength $opnonautorise] -1]} {$z >= 0} {incr z -1} {
set operations [lreplace $operations [lindex  $opnonautorise $z] [lindex  $opnonautorise $z]]
}
}
wm title . "Probl�mes - $arg - Niveau : $niveau - Utilisateur : [string map {.log \040} [file tail $user]]"
}
############################################################################
global arg page niv parcours indparcours log nbverif okdessinniv4
set indparcours 0
set okdessinniv4 0
set log ""
set nbverif 0
set arg [lindex $argv 0]
set niv [lindex $argv 1]
set parcours [lindex $argv 2]
set page 1
main
