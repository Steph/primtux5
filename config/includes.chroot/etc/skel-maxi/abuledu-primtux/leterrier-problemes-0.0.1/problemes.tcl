#*************************************************************************
#  Copyright (C) 2007 David Lucardi <davidlucardi@aol.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier:
#  Date    : 01/06/2007
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi 01/06/2007
# 
#  *************************************************************************
#!/bin/sh
#problemes1.tcl
# \
exec wish "$0" ${1+"$@"}


global sysFont basedir Homeconf plateforme env baseHome filuser
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}

source fonts.tcl
source path.tcl
source msg.tcl
set lang fr


init $plateforme
inithome
initlog $plateforme $ident
source menus.tcl
if {[lindex $argv 0] != ""} {set filuser [lindex $argv 0]}
wm title . "PROBLEMES - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"


proc interface {} {
global user 
wm title . "PROBLEMES - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"
}


proc execp {whatex what niv parcours} {
#integrer ici la gestion du niveau (rajouter param niveau)
catch {exec wish $whatex $what $niv $parcours &}
}

proc showparcours {} {
global Home
catch {destroy .framebottom}
frame .framebottom -width 400 -bg grey
pack .framebottom -side top -fill both -expand yes
set ind 0
set what parcours
set ext .par
foreach i [glob [file join $Home problemes  $what*$ext]] {
set f [open [file join  $Home problemes [file tail $i]] "r"]
set parcours [gets $f]
close $f

set tmp [lindex [lindex $parcours 0] 0]
regsub -all {.txt} $tmp "" tmp
set tit [file tail $i]
regsub -all {.txt} $tit "" tit

set indp [string range $tmp end end]
button .framebottom.enonce$ind -bg grey -text "$ind - $tit" -width 50 -command ".framebottom.enonce$ind configure -fg blue; catch {execp problemes$indp.tcl [lindex [lindex $parcours 0] 0] [lindex [lindex $parcours 0] 1] [file tail $i]}"
grid .framebottom.enonce$ind -row [expr int($ind/2)] -column [expr int(fmod($ind,2))]
incr ind
}
}

proc show {what} {
global Home
catch {destroy .framebottom}
frame .framebottom -width 400 -bg grey
pack .framebottom -side top -fill both -expand yes
set ind 0
set ext .txt
foreach i [glob [file join  $Home problemes  $what*$ext]] {
set f [open [file join  $Home problemes [file tail $i]] "r"]
set tmp [gets $f]
eval $tmp
set tmp [gets $f]
eval $tmp
	while {$tmp != "::1"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {catch {eval $tmp}}
	if {[string first enonce $tmp]!= -1} {break}
	}
close $f
set tmp [file tail $i]
regsub -all {.txt} $tmp "" tmp
set indp [string range $tmp end end]
set indtit [string first "." $enonce]
set titre [string range $enonce 0 $indtit]
button .framebottom.enonce$ind -bg grey -text "$ind - $titre" -width 50 -command ".framebottom.enonce$ind configure -fg blue; catch {execp problemes$indp.tcl [file tail $i] none none}"
grid .framebottom.enonce$ind -row [expr int($ind/2)] -column [expr int(fmod($ind,2))]
incr ind
}
}

wm geometry . +0+0

. configure -bg grey
frame .frametop -bg grey -width 400
pack .frametop -side top -fill both -expand yes
frame .frame -width 400 -bg grey
pack .frame -side top -fill both -expand yes
button .frametop.b1 -command "show den" -image [image create photo -file [file join sysdata den.gif]] -width 150
pack .frametop.b1 -side left -padx 10 -pady 10 -expand yes
label .frametop.lab -image [image create photo -file [file join sysdata background.png]] -width 150 -bg grey
pack .frametop.lab -side left -padx 10 -expand yes
button .frametop.b2 -command "show adsous" -image [image create photo -file [file join sysdata adsous.gif]] -width 150
pack .frametop.b2 -side left -padx 10 -pady 10 -expand yes
button .frame.b3 -command "show ligneg" -image [image create photo -file [file join sysdata ligneg.gif]]  -width 150
pack .frame.b3 -side left -padx 10 -pady 10 -expand yes
button .frame.b4 -command "show multip" -image [image create photo -file [file join sysdata multip.gif]] -width 150
pack .frame.b4 -side left -padx 10 -pady 10 -expand yes
button .frame.b5 -command "showparcours" -image [image create photo -file [file join sysdata parcours.gif]] -width 150
pack .frame.b5 -side left -padx 10 -pady 10 -expand yes
