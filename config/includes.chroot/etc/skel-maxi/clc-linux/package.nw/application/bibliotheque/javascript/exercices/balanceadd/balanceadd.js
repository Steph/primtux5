var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.balanceadd = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aDonnee = [], tabMasse = [],tabx = [],taby = [],champReponse,sommeTotale,massMax, massMin,nbMasse,q,etiquette,fondbalance,textBalance;
// Référencer les ressources de l'exercice (textes, image, son)
exo.oRessources = { 
    txt : "balanceadd/textes/balanceadd_fr.json",
    balance : "balanceadd/images/balance.png",
    masse1: "balanceadd/images/masse.png",
    illustration : "balanceadd/images/illustration.png"
}

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        temps_exo:0,
        temps_question:0,
        typeCalcul:0,
        //nbreCaisse :2
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    //Creation des donnees
	var i, nA,nB,nC,nD;
	aDonnee = [];
	// somme de 3 centaines
	if ( exo.options.typeCalcul == 0 ) {
		for(i=0;i<exo.options.totalQuestion;i++){
			nA = (5 + Math.floor(Math.random()*(9-5+1)))*100;
			nB = (5 + Math.floor(Math.random()*(9-5+1)))*100;
			nC = (2 + Math.floor(Math.random()*(5-2+1)))*100;
			if ( 
				nA!=nB && nA!=nC && nB!=nC 
				&& nA+nB+nC%1000!=0 && 
				!testePresence([nA,nB,nC],aDonnee)
				) 
			{
				aDonnee.push([nA,nB,nC]);
			} 
			else {
				i--
			}
		}
		
	}
	// somme de 4 centaines
	else if ( exo.options.typeCalcul == 1 ) {
		for(i=0;i<exo.options.totalQuestion;i++){
			nA = (5 + Math.floor(Math.random()*(9-5+1)))*100;
			nB = (5 + Math.floor(Math.random()*(9-5+1)))*100;
			nC = (2 + Math.floor(Math.random()*(5-2+1)))*100;
			nD = (2 + Math.floor(Math.random()*(5-2+1)))*100;
			if ( 
				nA!=nB && nA!=nC && nB!=nC 
				&& nA+nB+nC%1000!=0 && 
				!testePresence([nA,nB,nC],aDonnee)
				) 
			{
				aDonnee.push([nA,nB,nC,nD]);
			} 
			else {
				i--
			}
		}
		
	}
	//somme de 2 centaines et 2 dizaines
	else if ( exo.options.typeCalcul == 2 ){
		for(i=0;i<exo.options.totalQuestion;i++){
			nA = (5 + Math.floor(Math.random()*(9-5+1)))*100;
			nB = (2 + Math.floor(Math.random()*(5-2+1)))*10;
			nC = (5 + Math.floor(Math.random()*(9-5+1)))*100;
			nD = (2 + Math.floor(Math.random()*(5-2+1)))*10;
			if ( 
				nA!=nC && nB!=nD
				&& 
				!testePresence([nA,nB,nC],aDonnee)
				) 
			{
				aDonnee.push([nA,nB,nC,nD]);
			} 
			else {
				i--
			}
		}
	}
    function testePresence(arrayNeedle,arrayStack) {
	return arrayStack.some(function(element,index,arr){
		return element.every(function(e,n,a){
			return e==arrayNeedle[n];
		});
	});
    }
}
//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration)
}
//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
		numeric : "enabled ",
		float : "enabled ",
		delete : "enabled ",
		arrow : "disabled ",
		large : "disabled ",
		valider : "enabled "
    });
    exo.blocInfo.css({
        left:560,
        top:140
    });
    //typeQ : 0 somme 1 écart
    var typeQ=0;
    //reponse en g 0 ou kg 1
    var typeRep=0;
    //valeurs utilisées dans cette question
    massMax=0;
    massMin=0;
    //
    var afficheB="";
    q = exo.indiceQuestion;
    nbMasse=aDonnee[q].length;
    console.log(nbMasse);
    typeQ=1; //on demande l'écart dans cette version
    typeRep=0;
    //massMax=Math.max.apply(this,aDonnee[q]);
    //massMin=Math.min.apply(this,aDonnee[q]);
    sommeTotale=0;
    //la consigne
    etiquetteQuestion = disp.createTextLabel(exo.txt.consigne1);
    exo.blocAnimation.append(etiquetteQuestion);
    etiquetteQuestion.css({
        left:20,
        top:320
    });
    champReponse = disp.createTextField(exo,5);
    exo.blocAnimation.append(champReponse);
    champReponse.position({
            my:'left top',
            at:'right+5 top-5',
            of:etiquetteQuestion
    });
    champReponse.focus();
    champReponse.on("touchstart",function(e){
		    champReponse.focus();
    });
    var symboleG = disp.createTextLabel("kg");
    exo.blocAnimation.append(symboleG);
    symboleG.position({
            my:'left top',
            at:'left+100 top+5',
            of:champReponse
    });
    //la balance
    fondbalance = disp.createMovieClip(exo,"balance",190,105,100);
    fondbalance.gotoAndStop(2);
    fondbalance.conteneur.css({
        left:300,
        top:200
    });
    exo.blocAnimation.append(fondbalance.conteneur);
    textBalance = disp.createTextLabel("0 g");
    textBalance.css({
        fontSize:'22',
        fontWeight:'bold',
        textAlign:'right',
        //backgroundColor:'333',
        width:'120',
        //borderRadius:'4',
        color:'#9F9',
        left:350,
        top:235
    });
    exo.blocAnimation.append(textBalance);
    //les paquets
    tabx=[50,150,250,350];
    taby=[12,12,12,12];
    for (i=0; i<nbMasse; i++) {
        tabMasse[i] = disp.createMovieClip(exo,"masse1",90,130,100);
        tabMasse[i].gotoAndStop(Math.floor(5*Math.random())+1);
        tabMasse[i].conteneur.data("valeur",aDonnee[q][i]);
        tabMasse[i].conteneur.data("place",false);
        tabMasse[i].conteneur.data("indice",i);
        etiquette = disp.createTextLabel(aDonnee[q][i]+" g");
        etiquette.css({
            opacity:'0',
            x:10,
            y:90
        });
        tabMasse[i].conteneur.append(etiquette);
        tabMasse[i].conteneur.css({x:tabx[i],y:taby[i]});
        tabMasse[i].conteneur.on("touchstart.clc mousedown.clc",sourisCliquee);
        exo.blocAnimation.append(tabMasse[i].conteneur);
	sommeTotale+=aDonnee[q][i];
    }
    function sourisCliquee(e) {
        var cible = $(this);
        if (cible.data("place")==false) {
            fondbalance.gotoAndStop(2);
            textBalance.text("0 g");
            for (i=0; i<nbMasse; i++) {
                if (tabMasse[i].conteneur.data("indice")!=cible.data("indice")) {
                    if (tabMasse[i].conteneur.data("place")==true) {
                        tabMasse[i].conteneur.data("place",false);
                        tabMasse[i].conteneur.transition({x:tabx[i],y:taby[i]},500,"linear");
                    }
                } else {
                    tabMasse[i].conteneur.data("place",true);
                }
            }
            //textBalance.text(cible.data("valeur")+" g");
            afficheB = cible.data("valeur")+" g";
            cible.transition({x:350,y:113},500,"linear",paquetPoser);
        } else {
            fondbalance.gotoAndStop(2);
            textBalance.text("0 g");
            for (i=0; i<nbMasse; i++) {
                if (tabMasse[i].conteneur.data("place")==true) {
                    tabMasse[i].conteneur.data("place",false);
                    tabMasse[i].conteneur.transition({x:tabx[i],y:taby[i]},500,"linear");
                }
            }
        }
        champReponse.focus();
    }
    function paquetPoser() {
        textBalance.text(afficheB);
        fondbalance.gotoAndStop(1);
    }
}

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    if (champReponse.val()=="") {
	    return "rien";
    }
    if (util.strToNum(champReponse.val())*1000 == sommeTotale){
	return "juste";
    } else {
	return "faux";
    }
}

// Correction (peut rester vide)
exo.corriger = function() {
    fondbalance.gotoAndStop(2);
    textBalance.text("0 g");
    var correction = disp.createCorrectionLabel(util.numToStr(sommeTotale/1000));
    exo.blocAnimation.append(correction)
    correction.position({
        my:"center center",
        at:"center center+30",
        of:champReponse,
    })
    var barre1 = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre1);
    var textSolution = "";
    for (i=0; i<nbMasse; i++) {
        tabMasse[i].conteneur.css({x:tabx[i],y:taby[i]});
        etiquette = disp.createTextLabel(aDonnee[q][i]+" g");
        etiquette.css({
            opacity:'1',
            x:10,
            y:90
        });
	if (i == 0) {
	    textSolution += " "+aDonnee[q][i]+" g ";
	} else {
	    textSolution += "+ "+aDonnee[q][i]+" g ";
	}
        tabMasse[i].conteneur.append(etiquette);
	tabMasse[i].conteneur.css({opacity:1});
    }
    textSolution += " = "+sommeTotale+" g = "+util.numToStr(sommeTotale/1000)+" kg";
    console.log(textSolution);
    var etiquetteSolution = disp.createTextLabel(textSolution);
    exo.blocAnimation.append(etiquetteSolution);
    etiquetteSolution.css({
        left:20,
        top:160
    });
}

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_question",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeCalcul",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[0,1,2]
    });
    exo.blocParametre.append(controle);
    //
    /*var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"nbreCaisse",
        texte:exo.txt.opt5,
        aLabel:exo.txt.label5,
        aValeur:[2,3,4]
    });
    exo.blocParametre.append(controle);*/
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))