trace ("image 1");
import code.*;
// definir ci-dessous les valeurs par defaut des options de l'exo
options={
	nom:"recette-as3",
	totalQuestion:10,
	totalEssai:1,
	temps_question:0,
	temps_exo:0,
	niveau:3
}

//initialisation de l'exo
initFrame1();
stop();





trace ("image 2");
//initialise la page de titre (le titre,la consigne,l'illustration,le logo IA)
initFrame2();
//
//Définir ci-dessous le titre la consigne et l'illustration
spTitre.tfTitre.text = texteExo.p1;
spTitre.tfConsigne.text = texteExo.p2;
var illustration = new Illustration();
spTitre.addChild(illustration);
addChild(spTitre);
illustration.x = (735-illustration.width)/2;
illustration.y= spTitre.tfConsigne.y + spTitre.tfConsigne.height+20;
//Creation des donnees
var i,j,k,nA,nB,qA,qB,valGradTemp:uint;
var aNA,aQA,aNB,aQB:Array;
var coeff=Number;
var aGrad:Array;
var aCoeff:Array;
var aNombre = new Array();


//rapport entre les grandeurs = x2 x3 x4 ou x5
if ( options.niveau == 0 ) {
	for ( i=0 ; i< options.totalQuestion ; i++ ) {
		nA = 3 + Math.floor(Math.random()*(5-3+1));
		aQA=new Array(20,30,40,50);
		aQA.sort(function(){return Math.floor(Math.random()*3)-1});
		qA = aQA[0];
		coeff = 2 + Math.floor(Math.random()*(5-2+1));
		nB = nA * coeff;
		qB = qA * coeff;
		if(Math.floor(qB/10)%5 == 0 ){
			valGradTemp=25;
		}
		else if(qB < 140){
			valGradTemp=10;
		} 
		else if(qB >= 140 ){
			valGradTemp=20;
		}
		
		aNombre.push([nA,qA,coeff,nB,qB,valGradTemp]);

	}
}
//rapport entre les grandeurs = 1/2,1/4,1/5
else if ( options.niveau == 1 ) {
	for ( i=0 ; i< options.totalQuestion ; i++ ) {
		
		aCoeff =[1/2,1/4,1/5];
		aCoeff.sort(function(){return Math.floor(Math.random()*3)-1});
		coeff = aCoeff[i%3];
		nB = 3 + Math.floor(Math.random()*(5-3+1));
		aQB = [20,30,40,50];
		aQB.sort(function(){return Math.floor(Math.random()*3)-1});
		qB = aQB[0];
		nA = nB/coeff;
		qA = qB/coeff;
		
		if(Math.floor(qB/10)%5 == 0 ){
			valGradTemp=25;
		}
		else if(qB < 140){
			valGradTemp=10;
		} 
		else if(qB >= 140 ){
			valGradTemp=20;
		}
		
		aNombre.push([nA,qA,coeff,nB,qB,valGradTemp]);

	}
}
// propriétés de la linéarite 1 + 1/2
else if ( options.niveau == 2 ) {
	for ( i=0 ; i< options.totalQuestion ; i++ ) {
		aNA = [4,6,8,10,12];
		aNA.sort(function(){return Math.floor(Math.random()*3)-1});
		nA = aNA[i%5];
		aQA=[20,40,50,60,80,120,160];
		//aQA.sort(function(){return Math.floor(Math.random()*3)-1});
		qA = aQA[i%7];
		coeff = 1.5;
		nB = nA * coeff;
		qB = qA * coeff;
		if(qB%5 == 0 && qB%10 != 0){
			valGradTemp=25;
		}
		else if(qB < 140){
			valGradTemp=10;
		} 
		else if(qB >= 140 ){
			valGradTemp=20;
		}
		aNombre.push([nA,qA,coeff,nB,qB,valGradTemp]);

	}
}
// passage par l'unité
else if ( options.niveau == 3 ) {
	for ( i=0 ; i< options.totalQuestion ; i++ ) {
		var aQUnitaire = [10,20,25];
		var qUnitaire = aQUnitaire[i%3];
		aNA = [2,3,4,5];
		aNA.sort(function(){return Math.floor(Math.random()*3)-1});
		nA = aNA[i%4];
		qA=nA*qUnitaire;
		nB = nA +1;
		qB = nB * qUnitaire;
		valGradTemp = qUnitaire;
		aNombre.push([nA,qA,coeff,nB,qB,valGradTemp]);
	}
}


stop();



trace ("image 3");
// initialise la zone d'animation la zone de question,
// la zone d'affichage du score la zone d'infos etc.
initFrame3();
import flash.filters.*
//
var q = indiceQuestion - 1;
var reponse:uint=0;
var espaceGrad:uint=18;
var grad:Graduation;
var aGraduation=new Array();
var soluce = aNombre[q][4];
var valGrad=aNombre[q][5];

//
var recette = new Recette();
recette.textePersonne.text = "Pour "+aNombre[q][0]+" personnes";
recette.texteQuantite.text = aNombre[q][1]+" g de sucre";
spAnimation.addChild(recette);
recette.x = 60;
recette.y = 30;

var filtreOmbre = new DropShadowFilter();
filtreOmbre.blurX=20;
filtreOmbre.blurY = 20;
filtreOmbre.color = 0xcccccc;

recette.filters = [filtreOmbre];

//
var recipient = new Recipient();
if(valGrad==20)
{
	recipient.scaleX = Math.sqrt(2);
}
else if(valGrad==25)
{
	recipient.scaleX = Math.sqrt(2.5);
}
else if(valGrad==50)
{
	recipient.scaleX = Math.sqrt(5);
}

spAnimation.addChild(recipient);
recipient.x = 315;
recipient.y=30;
recipient.farine.y = recipient.height-3;
recipient.farine.height = 0;


for(i=0;i<14;i++)
{
	grad=new Graduation();
	aGraduation.push(grad);
	grad.etiquette.text = Number(valGrad * (i+1))+" g";
	grad.x = recipient.verre.x + (recipient.verre.width-grad.width)/2;
	grad.y = recipient.verre.height-espaceGrad*(i+1)-3-grad.height/2;
	recipient.verre.addChild(grad);
	recipient.verre.graphics.lineStyle(1,0xcccccc,1);
	recipient.verre.graphics.moveTo(3,recipient.verre.height-espaceGrad*(i+1)-3);
	recipient.verre.graphics.lineTo(grad.x, recipient.verre.height-espaceGrad*(i+1)-3);
	recipient.verre.graphics.moveTo(grad.x+grad.width,recipient.verre.height-espaceGrad*(i+1)-3);
	recipient.verre.graphics.lineTo(recipient.verre.width-3, recipient.verre.height-espaceGrad*(i+1)-3);
}

var boutonPlus = new Bouton();
boutonPlus.buttonMode = true;
boutonPlus.mouseChildren = false;
registerListener(boutonPlus,"click",ajouter);
boutonPlus.etiquette.text ="+";
boutonPlus.x = recipient.x + recipient.width/2 - 30;
boutonPlus.y = recipient.y+recipient.height+10;
spAnimation.addChild(boutonPlus);

var boutonMoins = new Bouton();
boutonMoins.buttonMode = true;
boutonMoins.mouseChildren = false;
registerListener(boutonMoins,"click",retirer);
boutonMoins.etiquette.text ="-";
boutonMoins.x =  recipient.x + recipient.width/2 + 30
boutonMoins.y = recipient.y+recipient.height+10;
spAnimation.addChild(boutonMoins);
//
spQuestion.x = 50;
spQuestion.y = 240;
spQuestion.largeur = 200;
spQuestion.aValeur = [aNombre[q][3]];
spQuestion.texte=texteExo.p3
addChild(spQuestion);
//

function ajouter(evt)
{
	if(recipient.farine.height<recipient.height -espaceGrad)
	{
		
		recipient.farine.y-=espaceGrad;
		recipient.farine.height+=espaceGrad;
		reponse+=valGrad;
	}
}

function retirer(evt)
{
	if(recipient.farine.height > 0)
	{
		recipient.farine.y+=espaceGrad;
		recipient.farine.height-=espaceGrad;
		reponse-=valGrad;
	}
}


trace("image 4");
//Definir ci-dessous les fonctions globales evaluer et corriger 
evaluer = function()
{
	//cette fonction doit obligatoirement
	//retourner 'rien' 'juste' ou 'faux';
	if(reponse == 0)
	{
		return "rien";
	}
	else if(reponse == soluce)
	{
		return "juste";
	}
	else
	{
		return "faux";
	}
	
}

corriger = function()
{
	var formatCorrection:CalcTextFormat = new CalcTextFormat();
	formatCorrection.ajouteGras();
	formatCorrection.size=11;
	formatCorrection.color=0xff0000;
	for(i=0;i<14;i++)
	{
		if(aGraduation[i].etiquette.text == soluce+" g")
		{
			aGraduation[i].etiquette.setTextFormat(formatCorrection);
			recipient.verre.graphics.lineStyle(1,0xff0000,1);
			recipient.verre.graphics.moveTo(3,aGraduation[i].y+aGraduation[i].height/2);
			recipient.verre.graphics.lineTo(aGraduation[i].x, aGraduation[i].y+aGraduation[i].height/2);
			recipient.verre.graphics.moveTo(aGraduation[i].x+aGraduation[i].width,aGraduation[i].y+aGraduation[i].height/2);
			recipient.verre.graphics.lineTo(recipient.verre.width-3, aGraduation[i].y+aGraduation[i].height/2);
		}
				   
	}
}



stop();

