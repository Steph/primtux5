var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.cibles = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var svg, sommeTotale, totalFleche, maxFleche, soluce, totalZone, aFleche, aZone, aSoluce, creerFleche, supprimerFleche;
var aTotalRecherche = new Array();
var aNombres =  new Array();


// Référencer les ressources de l'exercice (image, son)

exo.oRessources = {
    illustration:"cibles/images/illustration.png",
    flecheBleue : "cibles/images/fleche-bleue.png",
    flecheBleueB : "cibles/images/fleche-bleue-b.png",
    flecheRouge : "cibles/images/fleche-rouge.png",
    rature:"cibles/images/rature.png"
}

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        tempsQuestion:0,
        tempsExo:0,
        totalEssai:1,
        typeDonnee:5
        
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    aNombres = new Array();
    aSoluce = new Array();
    aTotalRecherche = new Array();
    genererDonnees(exo.options.totalQuestion,exo.options.typeDonnee);
    
    function genererDonnees(nbreCas,typeDonnee){
        //console.log(nbreCas)
        if(nbreCas>10) nbreCas=10;
        // Toujours deux flèches
        if(typeDonnee == 1)
        {
            // Ajouter des dizaines entières - Cible à 4 zones
            aTemp = new Array(10,20,30,40,50,60,70,80,90);
            totalZone = 4;
            min=60;
            max=180;
        }
        else if (typeDonnee == 2)
        {
            // Ajouter des centaines entières - Cible à 4 zones
            aTemp = new Array(100,200,300,400,500,600,700,800,900);
            totalZone = 4;
            min=200
            max=1800;
        }
        else if(typeDonnee == 3)
        {
            // Ajouter des dizaines entières - Cible à 5 zones
            aTemp = new Array(10,20,30,40,50,60,70,80,90);
            totalZone = 5;
            min=70;
            max=180;
        }
        else if (typeDonnee == 4)
        {
            // Ajouter des centaines entières - Cible à 5 zones
            aTemp = new Array(100,200,300,400,500,600,700,800,900);
            totalZone = 5;
            min=700
            max=1800;
        }
        else if (typeDonnee == 5)
        {
            // Ajouter des entiers - somme < 20 - Cible à 4 zones
            aTemp = new Array(2,3,4,5,6,7,8,9,10);
            totalZone = 4;
            min=9;
            max=20;
        }
        
        else if (typeDonnee == 6)
        {
            //Cible a quatre zones 2 fleches Ajouter entiers <20 
            aTemp = new Array(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
            totalZone = 4;
            min=20;
            max=40;
        };
        for (i=0;i<nbreCas;i++)
        {
            while (true)
            {
                aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
                aSoluceTemp = aTemp.slice(0,2);
                nbreA = aSoluceTemp[0];
                nbreB = aSoluceTemp[1];
                totalRecherche = nbreA + nbreB;
                
                if(totalRecherche >= min && totalRecherche <=max && aTotalRecherche.indexOf(totalRecherche)<0)
                {
                    aSoluce.push([nbreA,nbreB]);
                    //aTotalRecherche[i]=totalRecherche;
                    aTotalRecherche.push(totalRecherche);
                    break;
                }
            }
            var aResteTemp = aTemp.slice(2);
            while (true)
            {
                aResteTemp.sort(function(){return Math.floor(Math.random()*3)-1});
                aNombresTemp = aResteTemp.slice(0,totalZone-2);
                var somme = 0;
                var test=0;
                
                for(j=0;j<aNombresTemp.length;j++)
                {
                    somme+=aNombresTemp[j];
                    if(aNombresTemp[j] == totalRecherche)
                        test++;
                    if (somme ==  totalRecherche)
                        test++;
                }
                
                if(aNombresTemp[0] + aNombresTemp[aNombresTemp.length-1] == totalRecherche)
                    test++;
                if(aNombresTemp[aNombresTemp.length-2] + aNombresTemp[aNombresTemp.length-1] == totalRecherche)
                    test++;
                
                if(test == 0)
                {
                    aNombresTemp.push(nbreA);
                    aNombresTemp.push(nbreB);
                    //trace("aNombresTemp : "+aNombresTemp);
                    aNombresTemp.sort(function(a,b){return a-b });
                    aNombresTemp.push(totalRecherche);
                    //aNombres[i]=aNombresTemp;
                    aNombres.push(aNombresTemp);
                    break;
                }
            }
            
        }
    }
}

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("les cibles")
    exo.blocConsigneGenerale.html("Cliquer sur la cible pour placer des flèches et obtenir le total demandé.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustration);
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    
    var aValeurZone = aNombres[exo.indiceQuestion];
    soluce = aSoluce[exo.indiceQuestion][0]+aSoluce[exo.indiceQuestion][1];
    aFleche = new Array();
    
    var aBgColor = ["#FFCC00","#99CC00","#6598FF","#CC32FF","#aaa"];
    maxFleche = 2;
    totalFleche = 0;
    sommeTotale = 0;
    
    //le conteneur svg
    svg = disp.createSvgContainer(735,450);
    var centreX = 250;
    var centreY = 225;
    exo.blocAnimation.append(svg);
    svg.position({
        my:"center",
        at:"center",
        of:exo.blocAnimation
    });
    
    //les fleches
    creerFleche = function(e){
        var disque = this;
        if(totalFleche < maxFleche) {
            totalFleche++;
            sommeTotale+=disque.data("valeur");
            if ( !e.pageX   ) {
                var localX = svg.offset().left;
                var localY = svg.offset().top;
            }else{
                var localX = e.pageX - svg.offset().left;
                var localY = e.pageY - svg.offset().top;
            }
            var uri = exo.getURI("flecheBleue");
            console.log(uri)
            var svgFleche = svg.paper.image(uri, localX -5, localY - 14.5, 116, 29);
            var angle = Raphael.angle(localX,localY,centreX,centreY)
            
            svgFleche.transform("r"+angle+" "+localX +" "+localY);
            svgFleche.attr("cursor","pointer");
            svgFleche.data("valeur",disque.data("valeur"));
            svgFleche.data("angle",angle);
            aFleche.push(svgFleche);
            svgFleche.click(supprimerFleche);
            svgFleche.hover(mouseIn,mouseOut);
        }
    }
    
    supprimerFleche = function (e){
        var fleche = this;
        sommeTotale-=fleche.data("valeur");
        totalFleche--;
        aFleche.splice(aFleche.indexOf(fleche),1);
        this.remove();
        console.log(aFleche.length)
    }
    
    function mouseIn(e){
        this.attr("src",exo.getURI("flecheBleueB"));
    }
    
    function mouseOut(e){
        this.attr("src",exo.getURI("flecheBleue"));
    }
    //la cible
    aZone = new Array();
    
    for (var i=0;i<totalZone;i++){
        var rayon = 32*(totalZone - i);
        var couleur = aBgColor[i];
        var disque = svg.paper.circle(centreX,centreY,rayon);
        disque.attr("stroke","none")
        disque.attr("fill",couleur);
        disque.attr("cursor","pointer");
        disque.data("index",i);
        disque.data("valeur",aValeurZone[i]);
        disque.data("rayon",rayon);
        var label = svg.paper.text(centreX,centreY-rayon+20,aValeurZone[i]);
        label.attr({"font-size":18,"font-weight":"bold"});
        disque.click(creerFleche);
        aZone.push(aValeurZone[i]);
    }
    
    
    
    //la consigne
    var cartouche = svg.paper.rect(400,20,280,80);
    cartouche.attr({
        fill:"90-#efefef-#fff",
        stroke:"#ccc",
        "stroke-width":1,
        r:5
    });
    var consigne = svg.paper.text(540,60,"Place 2  flèches pour faire\nun total de "+soluce+".");
    consigne.attr({
        "font-size":18
    })
    
    
    
}

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    
    if( totalFleche < maxFleche) {
        return "rien";
    }
    else if ( totalFleche == maxFleche && sommeTotale == soluce) {
        eve.off("click",creerFleche)
        eve.off("click",supprimerFleche)
        return "juste"
    }
    else if ( totalFleche == maxFleche && sommeTotale != soluce) {
        svg.paper.forEach(function(elt){
            elt.unclick(creerFleche);
            elt.unclick(supprimerFleche);
        })
        return "faux"
    }
    
    
}

// Correction (peut rester vide)

exo.corriger = function() {
    var aCorrection = new Array()
    //reperer les fleches fausses
    for (var i = 0 ; i < totalFleche ; i++) {
        var fleche = aFleche[i];
        if(aSoluce[exo.indiceQuestion].indexOf(fleche.data("valeur")) < 0 ) {
            fleche.attr("opacity","0.8");
            var posX = fleche.getBBox().x+fleche.getBBox().width/2 - 15;
            var posY = fleche.getBBox().y+fleche.getBBox().height/2 -15;
            var rature = svg.paper.image(exo.getURI("rature"), posX, posY, 30 , 30);
            //rature.transform("r"+fleche.data("angle")+" "+posX+" "+posY);
            aCorrection.push(fleche)
        } else
        {
            aSoluce[exo.indiceQuestion].splice(aSoluce[exo.indiceQuestion].indexOf(fleche.data("valeur")),1);
        }
    }
    
    // creer les fleches corrections
    for (var i = 0 ; i < aCorrection.length ; i++) {
        var fleche = aCorrection[i];
        var indexZone = aZone.indexOf(aSoluce[exo.indiceQuestion][i]);
        var rayon = 32*(totalZone - indexZone)-16;
        var angleDeg = aCorrection[i].data("angle");
        var angleRad = aCorrection[i].data("angle")*Math.PI/180 + Math.PI;
        
        var localX = 250 + rayon*Math.cos(angleRad)
        var localY = 225 + rayon*Math.sin(angleRad)-14.5
        var svgFleche = svg.paper.image(exo.getURI("flecheRouge"), localX, localY, 116, 29);
        
        svgFleche.transform("r"+(angleDeg+180)+" "+localX +" "+(225 + rayon*Math.sin(angleRad)));
        
    }
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions (10 maximum) : "
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"typeDonnee",
        texte:"Type d'exercice : ",
        aValeur:[5,6,1,2,3,4],
        aLabel:["Ajouter des entiers - somme < 20 - Cible à 4 zones",
                "Ajouter des entiers < 20 - Cible à 4 zones",
                "Ajouter des dizaines entières - Cible à 4 zones", 
                "Ajouter des centaines entières - Cible à 4 zones",
                "Ajouter des dizaines entières - Cible à 5 zones",
                "Ajouter des centaines entières - Cible à 5 zones"]
        
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))