############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editvoirlinux.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editvoirlinux.tcl,v 1.1.1.1 2004/04/16 11:45:47 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

proc majscales {total0 total1 total2} {
variable prop
set sup 0
set compt 0
set nbitem 1
	foreach item {0 1 2} {
	set compt [expr $compt + $prop($item)]
		if {$prop($item) != 0} {set nbitem [expr \$total$item*$nbitem]
		if {$sup < [expr \$total$item]} {set sup [expr \$total$item]}
		}
	}


	switch $compt {
		1 {
		set min $sup
		set max [expr [.geneframe.visible get]*$nbitem]
		#.leftframe.frame1.listsce insert end "$min $max"
		}
		2 {
		set min [expr $sup + [.geneframe.visible get]]
		set max [expr [.geneframe.visible get]*$nbitem]
		#.leftframe.frame1.listsce insert end "$min $max"
		}
		3 {
		set min [expr $sup + [.geneframe.visible get] + 1]
		set max [expr [.geneframe.visible get]*$nbitem]
		#.leftframe.frame1.listsce insert end "$min $max"
		}

	}
updatescales $min $max 
}


proc prop_event {what} {

global formes couleurs tailles
variable prop
set compt 0
set total0 0
set total1 0
set total2 0
if {[.geneframe.prop[expr $what + 1] cget -state] != "active"} {
#.leftframe.frame1.listsce insert end "[.geneframe.prop[expr $what + 1] cget -state]"
bell
return
}

foreach item $formes {
variable $item
set total0 [expr $total0 + $$item]
}
foreach item $couleurs {
variable $item
set total1 [expr $total1 + $$item]
}
foreach item $tailles {
variable $item
set total2 [expr $total2 + $$item]
}



foreach item {0 1 2} {
set compt [expr $compt + $prop($item)]
}

if {$compt <= 0} {
set prop($what) 1
bell
return
}
majscales $total0 $total1 $total2
}

proc forme_event {what} {
global formes couleurs tailles
variable prop
set total0 0
set total1 0
set total2 0
foreach item $formes {
variable $item
set total0 [expr $total0 + $$item]
}

if {$total0 > 1} {.geneframe.prop1 configure -state normal}

foreach item $couleurs {
variable $item
set total1 [expr $total1 + $$item]
}
foreach item $tailles {
variable $item
set total2 [expr $total2 + $$item]
}
set total [expr $total0 + $total1 + $total2]

if {$total <=3} {
set $what 1
incr total0 -1
bell
return
}
	if {$total0 == 1 } {
		if {[expr $prop(1) + $prop(2)] != 0} {
		set prop(0) 0
		.geneframe.prop1 configure -state disabled
		bell
		} else {
		set $what 1
		incr total0
		}
	}

	if {$total0 == 0} {
	set $what 1
	incr total0
	bell
	}

majscales $total0 $total1 $total2
}

proc coul_event {what} {
global couleurs formes tailles
variable prop
set total0 0
set total1 0
set total2 0

foreach item $couleurs {
variable $item
set total1 [expr $total1 + $$item]
}

if {$total1 > 1} {.geneframe.prop2 configure -state normal}

################################################
foreach item $formes {
variable $item
set total0 [expr $total0 + $$item]
}
foreach item $tailles {
variable $item
set total2 [expr $total2 + $$item]
}
set total [expr $total0 + $total1 + $total2]

if {$total <=3} {
set $what 1
incr total1 -1
bell
return
}
	if {$total1 == 1 } {
		if {[expr $prop(0) + $prop(2)] != 0} {
		set prop(1) 0
		.geneframe.prop2 configure -state disabled
		bell
		} else {
		set $what 1
		incr total1
		}
	}

	if {$total1 == 0} {
	set $what 1
	incr total1
	bell
	}
if {$total1 > 5} {
set $what 0
incr total1 -1
bell
}
majscales $total0 $total1 $total2
}

proc taille_event {what} {
global tailles formes couleurs
variable prop
set total0 0
set total1 0
set total2 0

foreach item $tailles {
variable $item
set total2 [expr $total2 + $$item]
}

if {$total2 > 1} {.geneframe.prop3 configure -state normal}

foreach item $couleurs {
variable $item
set total1 [expr $total1 + $$item]
}
foreach item $formes {
variable $item
set total0 [expr $total0 + $$item]
}
set total [expr $total0 + $total1 + $total2]

if {$total <=3} {
set $what 1
incr total2 -1
bell
return
}

	if {$total2 == 1 } {
		if {[expr $prop(0) + $prop(1)] != 0} {
		set prop(2) 0
		.geneframe.prop3 configure -state disabled
		bell
		} else {
		set $what 1
		incr total2
		}
	}
	if {$total2 == 0} {
	set $what 1
	incr total2
	bell
	}


majscales $total0 $total1 $total2
}
