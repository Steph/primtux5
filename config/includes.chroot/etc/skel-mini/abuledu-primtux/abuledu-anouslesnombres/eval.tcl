#!/bin/sh
#eval.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $Id: eval.tcl,v 1.1.1.1 2004/04/16 11:45:47 erics Exp $
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: eval.tcl,v 1.1.1.1 2004/04/16 11:45:47 erics Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************


proc enregistreeval {titre categorie data user} {

set listeval 0\040\173$titre\175\040\173$categorie\175\040\173$data\175
#exec /usr/bin/leterrier_log --message=$listeval --logfile=$user
set f [open [file join $user] "a+"]
puts $f $listeval
close $f
}


proc appendeval {what user} {
set message 1\040\173$what\175
#exec /usr/bin/leterrier_log --message=$message --logfile=$user
set f [open [file join $user] "a+"]
puts $f 1\040\173$what\175
close $f
}


