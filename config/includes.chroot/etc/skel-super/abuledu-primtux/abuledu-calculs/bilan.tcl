#!/bin/sh
#bilan.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
source calculs.conf
source i18n
global sysFont
source path.tcl
source msg.tcl
source fonts.tcl
##initlog  $tcl_platform(platform) $nom_elev
set  TRACEDIR  [pwd]
set basedir [pwd]
cd $TRACEDIR
set classe ""
# cd $dir_des_bilans (les classes = les groupes ?)
wm geometry . +0+0
wm title . [mc {bilan_elev}]

. configure  -height 480 -width 660 -background #808080
set font {Arial 12}
menu .menu -tearoff 0
# Creation du menu Classes
menu .menu.class -tearoff 0
.menu add cascade -label [mc {Classes}] -menu .menu.class
proc traite1 { ele cla } {
capturebilan $ele $cla
}
proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {,} $var1 "\." var2 ]
return $var2
}
proc traite {classe} {
global liste_noms 
vide_eleve
capturenoms $classe
catch {
##.menu.eleves add radio -label "Aucun" -variable elev -command "capturebilan $elev $classe "
foreach elev $liste_noms  {
.menu.eleves  add radio -label  $elev  -variable elev -command "capturebilan $elev $classe"
}
}
}
catch {
foreach i [lsort [glob -type f -dir [file join $basedir data]  *.bil  ]] {
.menu.class add command -label [file tail $i ] -command "traite [file tail $i]"
}
}
##.menu.class add command -label [mc {edit_scena_ment_appr}
menu .menu.eleves -tearoff 0
.menu add cascade -label "[mc {Eleves}]"  -menu .menu.eleves
menu .menu.quit -tearoff 0 
.menu add cascade -label "[mc {Quitter}]"  -menu .menu.quit
.menu.quit add command -label "quitter" -command "exit"
 

proc vide_eleve {} {
.menu.eleves delete 0 end
##menu .menu.eleves -tearoff 0
##.menu add cascade -label "[mc {Eleves}]"  -menu .menu.eleves
}
proc dans { el liste } {
foreach el_gen $liste {
if { $el_gen == $el } {
return 1
}
}
return 0
}
proc capturenoms { classe}  {
global basedir file liste_noms .text
if { $classe == "" } { return }
set file [open [file join $basedir data $classe ] "r" ]
set liste_noms {}
while {[gets $file ligne] >=0} {
 set liste_noms [linsert $liste_noms 0 [lindex [split $ligne "&"] 1]]

 }
close $file
set liste_noms [lsort $liste_noms ]
set liste_sans_doublons {}
foreach nom $liste_noms {
if { ! [dans $nom $liste_sans_doublons] } {
set liste_sans_doublons [linsert $liste_sans_doublons 0 $nom ]
}
}
set liste_noms $liste_sans_doublons

}


###.menu.eleves add command -label [mc {edit_scena_ment_appr} ]
. configure -menu .menu
text .text -yscrollcommand ".scroll set"  -xscrollcommand ".scroll1 set" \
-setgrid true -width 60 -height 30  -wrap word -background black -font   {Helvetica  12}
##scrollbar .frame2.scroll2  -orient horizontal  -command ".frame2.text xview"
##$sysFont(l)
scrollbar .scroll -command ".text yview"
scrollbar .scroll1 -orient horizontal  -command ".text xview"
pack .scroll -side right -fill y
pack .scroll1 -side bottom -fill x
pack .text -expand yes -fill both


proc capturebilan { eleve classe } {
global file nom_elev f basedir liste_noms
wm title . "[mc {bilan_elev}] : $eleve de [file rootname $classe]"
set f [open [file join $basedir data $classe ] "r" ]
#.text delete 1 end
##set nom_elev eleveb.cma
#set eleve eleveb.cma
#if [catch {set bilan [exec grep $nom_elev $f
#set bilan "pas de bilan"
#}
set bilan {}
##set f [open $file "r" ]
.text tag configure green -foreground green
.text tag configure green1 -foreground green -underline yes
.text tag configure red -foreground red
.text tag configure blue -foreground blue
.text tag configure white -foreground white
.text tag configure white1 -foreground white -underline yes -font {arial 14}
.text tag configure white2 -foreground white -underline yes
.text tag configure yellow -foreground yellow
.text delete 1.0 end
##.text insert end "$eleve $classe $f :" green
##.text insert end \n
set indice 0
while { ! [eof $f] } {
set ligne [gets  $f ]
##.text insert end  "liste  $liste_noms" green
##.text insert end "sligne :[split $ligne : ]\n" red ; .text insert end " nom : [lindex [split $ligne $ ] 1 ] el:  $eleve  \n" blue
if { [lsearch -exact  [split $ligne "&" ]  $eleve  ] >= 0 } {
## lsearch -exact [split $ligne ":" ] {$nom_elev} ] >= 0
##.text insert end "bool :[ lsearch  [split $ligne : ]  $eleve ]\n" yellow
##set bilan [ concat  [lrange [split $ligne ":" ] 1 4] $bilan ]
set ligne [split $ligne & ]
          set  date [lindex $ligne 0 ]  ;    set  nom  [lindex $ligne 1 ]
        set  nom_exo   [lindex $ligne 2 ]  ;     set  oper   [lindex $ligne 3 ]
          set  bonne_liste   [lindex $ligne 4 ]
         set  liste_troptard    [lindex $ligne 5 ]  ;  set  mrep_ou_echecs [lindex $ligne 6 ]
         set logiciel [lindex $ligne 7 ]
         if { $logiciel == "ap" } {
         set approx [lindex  $mrep_ou_echecs 0] ;  set n_mrep [lindex   $mrep_ou_echecs 1]
         set message " [ format [mc {avec une approximation de : %1$s et liste d'erreurs  de %2$s}] $approx ${n_mrep}]"
         }
         if { $logiciel == "pl" } {
         set message "[mc {nbre_max_err}] $mrep_ou_echecs"
        }
       if { $logiciel == "un" } {
         set message "[mc {list_err}]\n  $mrep_ou_echecs"
        }
	if { $logiciel == "eq" } {
         set message "[mc {Mauvais calculs}] :\n  $mrep_ou_echecs"
        }
if { $indice == 0} {
        .text insert end  "[format [mc {Bilans pour %1$s de la classe %2$s}] $eleve $classe]\n"  white1
incr indice
}
	      .text insert end "[format [mc {le %1$s pour %2$s opération : %3$s}] $date $nom_exo $oper ]\n" white
		 if  { $logiciel == "un" }  {
		foreach el [lindex ${bonne_liste} 0 ] {
		 .text insert end  "[format [mc {Calcul de %1$s}] [lindex $el 0]  ]" white2
		.text insert end  " :[lrange $el 1 end]\n" green
		}
		} else {
	     .text insert end  "[mc {bon_calc}] \n " white
	     .text insert end   " ${bonne_liste}\n"    green
             .text insert end " [mc {trop_tard}]\n" white
			   if  { $logiciel == "ap"  } {
				   .text insert end  "  ${liste_troptard}\n $message\n"  yellow
			    } else {  .text insert end  "  ${liste_troptard}\n $message\n"  red }
	    }

	     .text insert end "============================================================\n"  white


#
}
}
}
#
