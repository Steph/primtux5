set etapes 1
set niveaux {0 1}
::1
set niveau 1
set ope {{1 9} {1 9}}
set interope {{1 10 1} {1 10 1}}
set dejap1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set dejap2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope1 [expr int(rand()*(9-$dejap1)) + $dejap1]
set ope2 [expr int(rand()*(9-$dejap2)) + $dejap2]
set volatil 1
set operations {{0+0}}
set enonce "Les billes.\nTim veut [expr $ope1*10 + $ope2] billes.\nRajoute les sacs et les billes qui manquent."
set cible [list [list 4 3 [list sac_billes.gif [expr $dejap1]] source0] [list 4 3 [list bille.gif [expr $dejap2]] source1]]
set intervalcible 60
set taillerect 70
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {sac_billes.gif bille.gif}
set orient 0
set labelcible {Sacs Billes}
set quadri 0
set ensembles [list [expr $ope1] [expr $ope2]]
set reponse [list [list {1 3} [list {Il a rajout�} [expr $ope1 - $dejap1] {sac(s) et} [expr $ope2 - $dejap2] bille(s).]]]
set dessin 0
set canvash 360
set c1height 160
set oprequise {}
::
