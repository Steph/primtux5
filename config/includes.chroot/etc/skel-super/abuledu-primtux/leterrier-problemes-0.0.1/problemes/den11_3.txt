set etapes 1
set niveaux {0 1 3}
::1
set niveau 1
set ope {{10 0}}
set interope {{1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set operations {{0+0}}
set volatil 0
set enonce "Les billes.\nJe veux [expr $ope1*10] billes.\ncombien de sacs dois-je prendre?"
set cible {{4 3 {} source0}}
set intervalcible 60
set taillerect 70
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {sac_billes10.gif}
set orient 0
set labelcible {{Sac de billes}}
set quadri 0
set ensembles [list [expr $ope1]]
set reponse [list [list {1} [list {Il faut} [expr $ope1] {sacs de 10 billes.}]]]
set dessin 0
set canvash 340
set c1height 60
set opnonautorise {}
::

