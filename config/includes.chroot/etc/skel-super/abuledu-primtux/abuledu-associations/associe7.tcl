############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: associe7.tcl,v 1.4 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associe7.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

###########################################################
#alear :tableau d'int pour le m�lange
#etiqarr :tableau pour recevoir les syllabes
#imgarr : tableau pour recevoir les images
#font2 : police de caract�re
#nb : nombre total d'item du jeu en cours
#nbreu : index de l'item du jeu en cours
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
#nbechec variable pour d�terminer � quel moment intervient l'aide et pendant combien de temps
#listeval :liste pour collecter les informations pour les fiches bilan
#categorie : variable pour la categorie
#user : variable pour le nom de l'utilisateur
#flag : d�tection de r�entrance dans le survol des objets, pour la gestion du son 
#niveau : gestion du passage au niveau de difficult� sup�rieur

#variables
source fonts.tcl
source path.tcl
source msg.tcl
source eval.tcl


global alear imgarr etiqarr bgn bgl nb font2 listdata nbechec listeval categorie user niveau repbasecat Home sound baseHome serie

set c .frame.c
set bgn #ffff80
set bgl #ff80c0
set nbechec 0
set niveau 0
set catedefaut ""
set font2 ""
set son 1
set arg [lindex $argv 0]
set serie [lindex $argv 1]

set ident $tcl_platform(user)
set plateforme $tcl_platform(platform)
initlog $plateforme $ident
inithome

#interface
. configure  -height 480 -width 640
. configure -background #ffff80 
frame .frame -width 500 -height 420 -background #ffff80
pack .frame

wm geometry . +0+0
canvas $c -width 480 -height 280 -background $bgn -highlightbackground #ffff80
pack $c
entry .frame.dict -justify center
pack .frame.dict 


frame .bframe -width 370 -height 80 -background #ffff80
pack .bframe -side bottom

for {set i 1} {$i < 9} {incr i 1} {
  label .bframe.lab$i -background #ffff80 -width 4
  grid .bframe.lab$i -column [expr $i -1] -row 1 -sticky e
  }
label .bframe.lab22 -background #ffff80 -text [mc {Ecris et appuie sur la touche entree.}]
grid .bframe.lab22 -column $i -padx 1 -row 1
button .bframe.b1 -image [image create photo imagavant -file [file join sysdata avant.gif] ] -background #ff80c0 -command "setniveauprec $c"
grid .bframe.b1 -column [expr $i + 1] -padx 1 -row 1 -sticky w

button .bframe.b2 -image [image create photo imagbut -file [file join sysdata again2.gif] ] -background #ff80c0
grid .bframe.b2 -column [expr $i + 2] -padx 1  -row 1 -sticky w
button .bframe.b3 -image [image create photo suitebut -file [file join sysdata suite.gif]] -background #ff80c0 -command "quitte"
grid .bframe.b3 -column [expr $i + 3] -padx 1  -row 1 -sticky w

image create photo pbien -file [file join sysdata pbien.gif] 
image create photo ppass -file [file join sysdata ppass.gif]
image create photo pmal -file [file join sysdata pmal.gif]

#R�cup�ration donn�es g�n�rales dans associations.conf
catch {set f [open [file join $baseHome reglages associations.conf] "r"]
set catedefaut [gets $f]
set font2 [gets $f]
set son [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set repbasecat [gets $f]
close $f}

set ext .cat
if {$catedefaut != "none" && $catedefaut != "" && $serie !="0"} {
set catedefaut [string map {.cat ""} $catedefaut]$serie$ext
}


if {$font2 == "none" || $font2 == ""} {
	set font2 {Arial 24 bold}
	} else {
	set font2 \173$font2\175\04024\040bold
	}
.frame.dict configure -font $font2 -width 28

#d�tection possibilit� son (variable sound, et param�tre d'activation du son : son)
#if {[catch {package require snack}] || $son == 0} {
#set sound 0
#} else {
#set sound 1
#snack::sound s
#}

proc setniveauprec {c} {
global listeval categorie serie repbasecat listdata Home
if {$serie > 0} {
incr serie -1
set ext .cat
set file $categorie
for {set i 1} {$i <= [string length $file]} {incr i 1} {
if {[string match {[0-9]} [string index $file end]] == 1} {
set  file [string range $file 0 [expr [string length $file] -2] ]
}
}
if {$serie != 0} {
set file $file$serie$ext
} else {
set file $file$ext
}

set f [open [file join $Home categorie $repbasecat $file] "r"]
set listdata [gets $f]
close $f

set listeval \173[mc {Dictee visuelle - }]\175\040$file
wm title . "[mc {Dictee visuelle - }]$file"

place $c
}
}

proc init {c} {
global catedefaut listdata categorie Home font2 repbasecat plateforme getcat sound listeval iwish

	#if {$catedefaut == "none" || $catedefaut == ""} {
	#set types 	{
	#		{"Cat�gories"		{.cat}	}
      #   		}
	#if {$plateforme == "windows"} {wm iconify .}
	#catch {set file [tk_getOpenFile -initialdir [file join $Home categorie $repbasecat] -filetypes $types]}
	
	#if {$plateforme == "windows"} {wm deiconify .}
 	#} else {
	#set file $catedefaut
	#}

	if {$catedefaut == "none" || $catedefaut == ""} {
	opencat
	catch {destroy .opencate}
	set ext .cat
	set file $getcat$ext
 	} else {
	set file $catedefaut
	}
wm title . "[mc {Dictee visuelle - }]$file" 

	.bframe.b2 configure -command "recommence $c"
	#.bframe.b3 configure -command "suite $c"

	if {[catch { set f [open [file join $Home categorie $repbasecat $file] "r" ] }] } {
      set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info] 
	exec $iwish associations.tcl &
      exit
	}

     if {[catch {set listdata [gets $f]}]} {
     set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info]
     close $f
	exec $iwish associations.tcl &
     exit
     } else {
     close $f
     }



set categorie [lindex [split [lindex [split $file /] end] .] 0]

catch {
set ext ".dat"
set f [open [file join $Home categorie $repbasecat $categorie$ext] "r" ] 
set sson [lindex [lindex [gets $f] 2] 7]
set sound [expr $sound && $sson]
close $f
}
set listeval \173[mc {Dictee visuelle - }]\175\040$categorie

place $c
}


#procedures
##########################################################"""""
proc melange {bas haut} {
global alear nb
set diff [expr $haut - $bas]
for {set i $bas} {$i < $haut} {incr i 1} {
  set alear($i) $i
  }
for {set i 1} {$i < $nb} {incr i 1} {
  set t1 [expr int(rand()*$diff) + 1]
  set t2 [expr int(rand()*$diff) + 1]
  set temp $alear($t1)
  set alear($t1) $alear($t2)
  set alear($t2) $temp
  }
}


##########################################################"""""
proc recommence {c} {
#score : calcul du pourcentage de r�ussite de l'�l�ve
global listeval user score categorie
set score 0
#enregistreval
#lappend listeval "4 \173[mc {Exercice recommence}]\175"
set listeval \173[mc {Dictee visuelle - }]\175\040$categorie
place $c
}

##############################################################"""
proc place {c} {
#nbreu index de l'item en cours
#nbessai nombre de tentatives sur l'item en cours
global alear imgarr etiqarr bgn bgl nbreu font2 nb listdata nbessai listeval categorie score Home
set nbessai 0
set score 0
set nbreu 1
set font1 {Helvetica 12}

#set tmp [mc {Dictee visuelle - }]
#set listeval \173$tmp\175\040$categorie

.frame.dict delete 0 end

#on r�cupere les associaitons et on m�lange
set leng [llength $listdata]
for {set i 1} {$i <= $leng} {incr i 1} {
   set t1 [expr int(rand()*$leng)]
   set t2 [expr int(rand()*$leng)]
   set tmp [lindex $listdata $t1]
   set listdata [lreplace $listdata $t1 $t1 [lindex $listdata $t2]]
   set listdata [lreplace $listdata $t2 $t2 $tmp]
   }

# pas plus de 4 images par jeu
if {$leng > 4 } {
   set nb 4
   } else {
   set nb $leng
   }

# on r�cupere les images et eriquettes dans des tableaux
for {set i 1} {$i <= $nb} {incr i 1} {
    set imgarr($i) [lindex [lindex $listdata [expr $i - 1] ] 0]
    set etiqarr($i) [lindex [lindex $listdata [expr $i - 1] ] 1]
    }

#on efface et on place les images et etiquettes
$c delete all

image create photo pneutre -file [file join sysdata pneutre.gif]
for {set i 1} {$i <= $nb} {incr i 1} {
  .bframe.lab$i configure -image pneutre -width 30
  }


melange 1 [expr $nb +1]
image create photo kimage1 -file [file join $Home images $imgarr($alear(1))] -width 130 -height 130
set xpos 200
set ypos 150
$c create image $xpos $ypos -image kimage1 -tags img
$c create text  230 5 -text [string toupper $etiqarr($alear(1))] -tags cache -anchor n -font $font2

$c create text  230 40 -text $etiqarr($alear(1)) -tags cache -anchor n -font $font2
update

#on cache le mot
after 2000
$c delete withtag cache

image create photo oeil1 -file [file join sysdata oeil.gif]
image create photo oeil2 -file [file join sysdata oeil2.gif]

$c create image 50 160 -image oeil1 -tags voir
}

#appel principal
init $c
focus .frame.dict
#gestion des �v�nements
bind .frame.dict <Return> "verif $c"
#bind . <Destroy> "quitte"
if {$sound == 1} {
$c bind img <Any-Enter> "soundEnter $c"
$c bind img <Any-Leave> "soundLeave $c"
}
$c bind voir <Any-Enter> "voirEnter $c"
$c bind voir <Any-Leave> "voirLeave $c"


#pour revoir le mod�le
proc voirEnter {c} {
global etiqarr alear nbreu font2
$c create text  230 40 -text $etiqarr([expr $alear($nbreu)]) -tags cache -anchor n -font $font2
$c create text  230 5 -text [string toupper $etiqarr($alear($nbreu))] -tags cache -anchor n -font $font2

.frame.dict configure -state disabled
$c itemconf voir -image oeil2
}

proc voirLeave {c} {
catch {$c delete withtag cache}
.frame.dict configure -state normal
$c itemconf voir -image oeil1
}

#gestion du son

proc soundEnter {c} {
global imgarr nbreu etiqarr alear repbasecat Home
    set ext .wav
    set son "s_[lindex [split $imgarr($alear($nbreu)) .] 0]"
    enterstart [file join $Home sons $son$ext]
}

proc soundLeave {c} {
enterstop
}



#Verification de la r�ponse
proc verif {c} {
global etiqarr nbreu alear imgarr etiqarr font2 nb nbechec nbessai listeval niveau categorie score user listdata repbasecat Home serie iwish
variable repert

incr nbessai

set reponse [.frame.dict get]
regsub -all \040+ $reponse \040 reponse
	if {([string trim [string tolower $reponse]] ==$etiqarr([expr $alear($nbreu)]) || [string trim [string toupper $reponse]]==$etiqarr([expr $alear($nbreu)])) ||($nbechec>3)} {
	#si c'est r�ussi
	$c create text  230 40 -text $etiqarr([expr $alear($nbreu)]) -tags cache -anchor n -font $font2
	$c create text  230 5 -text [string toupper $etiqarr($alear($nbreu))] -tags cache -anchor n -font $font2
			switch $nbechec {
			0 {set tete bien.gif}
			1 {set tete bien.gif}
			2 {set tete ppass.gif}
			3 {set tete ppass.gif}
			4 {set tete mal.gif}
			} 

		image create photo figure -file [file join sysdata $tete]
		$c create image 420 160 -image figure -tags figure
		if {$nbechec < 3} {
		$c create text 200 250 -text [mc {Bravo!!}] -font {Arial 24}
		}
		if {$nbechec >3} {
		.frame.dict delete 0 end
		.frame.dict insert end $etiqarr([expr $alear($nbreu)])
		if {[string trim [string tolower $reponse]] !=$etiqarr([expr $alear($nbreu)]) && [string trim [string toupper $reponse]]!=$etiqarr([expr $alear($nbreu)])} {
		image create photo figure -file [file join sysdata $tete]
		} else {
		image create photo figure -file [file join sysdata ppass.gif]
		}
		$c create image 420 160 -image figure -tags figure
		update
		after 2000
		}


#mise � jour du bilan
              switch $nbessai {
                1 {.bframe.lab$nbreu configure -image pbien -width 30
                lappend listeval 1\040\173$etiqarr($alear($nbreu))\175
                incr score 10
                }
                2 {.bframe.lab$nbreu configure -image pbien -width 30
                lappend listeval 1\040\173$etiqarr($alear($nbreu))\175
                incr score 10
                }
			3 {.bframe.lab$nbreu configure -image ppass -width 30
                lappend listeval 2\040\173$etiqarr($alear($nbreu))\175
                incr score 5
                }
                4 {.bframe.lab$nbreu configure -image ppass -width 30
                lappend listeval 2\040\173$etiqarr($alear($nbreu))\175
                incr score 5
                }

                default {.bframe.lab$nbreu configure -image pmal -width 30
                lappend listeval 3\040\173$etiqarr($alear($nbreu))\175

                }
              }

set nbechec 0
update
after 2000
#on efface
.frame.dict delete 0 end
if {[incr nbreu] <= $nb} {
#si l'exercice n'est pas fini on pr�pare le mot suivant
set nbessai 0
.frame.dict delete 0 end
image create photo kimage1 -file [file join $Home images $imgarr([expr $alear($nbreu)])] -width 130 -height 130
set xpos 200
set ypos 150
$c delete all
$c create image $xpos $ypos -image kimage1 -tags img
$c create text  230 40 -text $etiqarr([expr $alear($nbreu)]) -tags cache -anchor n -font $font2
$c create text  230 5 -text [string toupper $etiqarr($alear($nbreu))] -tags cache -anchor n -font $font2

update
after 2000
$c create image 50 160 -image oeil1 -tags voir
#on cache le mot
$c delete withtag cache
} else {
#le niveau est fini, calcul du score
     $c delete all            
      set score [expr $score*10/$nb]
      set pourcent %
      $c create text 200 100 -text "[mc {Score : }] $score\040$pourcent" -font {Arial 24}
	lappend listeval \040bilan\040$serie\04017\040$repbasecat\040$repert\040$score
	enregistreval

      if {$score >= 75} { 
#si le score est suffisant, y a-t-il un niveau plus difficile de dsiponible?
            update
            after 2000
            set file $categorie
            set niveau ""
            for {set i 1} {$i <= [string length $file]} {incr i 1} {
               if {[string match {[0-9]} [string index $file end]] == 1} {
                 set niveau [string index $file end]$niveau
                 set  file [string range $file 0 [expr [string length $file] -2] ]
                }
            }
            if {$niveau == ""} {
            set niveau 0
            }
            incr niveau
            set ext .cat
            set file $file$niveau$ext
            if {[catch {set f [open [file join $Home categorie $repbasecat $file] "r"]} ]} {
#fin du jeu si pas de niveau disponible
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            } else {
#pr�paration du niveau disponible
            set listdata [gets $f]
            close $f
            #enregistreval
            set categorie [lindex [split [lindex [split $file /] end] .] 0]
		wm title . "[mc {Dictee visuelle - }]$file" 

		#lappend listeval "4 \{[mc {Dictee visuelle - }] $categorie\}"
		for {set i 1} {$i < 9} {incr i 1} {
		.bframe.lab$i configure -image "" -width 1
		}
		incr serie
		set listeval \173[mc {Dictee visuelle - }]\175\040$categorie

            place $c
            }
            } else {
#si le score n'est pas suffisant, fin du jeu
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            }
################################################################
     }
} else {
#si la r�ponse n'est pas bonne
incr nbechec
#bell
$c create text  230 40 -text $etiqarr([expr $alear($nbreu)]) -tags cache -anchor n -font $font2
$c create text  230 5 -text [string toupper $etiqarr($alear($nbreu))] -tags cache -anchor n -font $font2
image create photo figure -file [file join sysdata mal.gif]
$c create image 420 160 -image figure -tags figure

update
#affichage de l'aide � dur�e progressive
after [expr 1500 + $nbechec*500]
$c delete withtag cache
$c delete withtag figure
}
}


proc suite {c} {
global catedefaut
enregistreval
set catedefaut ""
init $c
}









