############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: associe17.tcl,v 1.4 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associe11.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

###########################################################
#alear : tableau d'int pour le m�lange
#etiqarr :tableau pour recevoir les syllabes
#imgarr : tableau pour recevoir les images
#font2 : police de caract�re
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
#nbreu index de l'item en cours
#listeval :liste pour collecter les informations pour les fiches bilan
#categorie : variable pour la categorie
#user : variable pour le nom de l'utilisateur
#flag : d�tection de r�entrance dans le survol des objets, pour la gestion du son 
#nb : nombre total d'item du jeu en cours
#niveau : gestion du passage au niveau de difficult� sup�rieur

#variables
source fonts.tcl
source path.tcl
source msg.tcl
source eval.tcl

global alear imgarr etiqarr bgn bgl nb font2 listdata listeval categorie user flag nbreu niveau repbasecat Home sound baseHome serie iwish
set c .frame.c
set bgn #ffff80
set bgl #ff80c0
set flag 0
set niveau 0
set catedefaut ""
set font2 ""
set son 1

set arg [lindex $argv 0]
set serie [lindex $argv 1]
set ident $tcl_platform(user)
set plateforme $tcl_platform(platform)
initlog $plateforme $ident
inithome

#interface
. configure -background #ffff80 
frame .frame -width 640 -height 380 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 380 -background $bgn -highlightbackground #ffff80
pack $c
frame .bframe -background #ffff80
pack .bframe -side bottom
for {set i 1} {$i < 9} {incr i 1} {
label .bframe.lab$i -background #ffff80 -width 4
grid .bframe.lab$i -column [expr $i -1] -row 1 -sticky e
}
label .bframe.lab22 -background #ffff80 -text [mc {Clique sur un bouton.}]
grid .bframe.lab22 -column $i -padx 1 -row 1

button .bframe.b1 -image [image create photo imagavant -file [file join sysdata avant.gif] ] -background #ff80c0 -command "setniveauprec2 $c"
grid .bframe.b1 -column [expr $i + 1] -padx 1 -row 1 -sticky w

button .bframe.b2 -image [image create photo imagbut -file [file join sysdata again2.gif] ] -background #ff80c0 
grid .bframe.b2 -column [expr $i + 2] -padx 1  -row 1 -sticky w
button .bframe.b3 -image [image create photo suitebut -file [file join sysdata suite.gif]] -background #ff80c0 -command "quitte"
grid .bframe.b3 -column [expr $i + 3] -padx 1  -row 1 -sticky w

#R�cup�ration donn�es g�n�rales dans associations.conf
catch {set f [open [file join $baseHome reglages associations.conf] "r"]
set catedefaut [gets $f]
set font2 [gets $f]
set son [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set repbasecat [gets $f]
close $f}

set ext .cat
if {$catedefaut != "none" && $catedefaut != "" && $serie !="0"} {
set catedefaut [string map {.cat ""} $catedefaut]$serie$ext
}

if {$font2 == "none" || $font2 == ""} {
set font2 {Arial 20 bold}

} else {
set font2 \173$font2\175\04020\040bold
}

#d�tection possibilit� son (variable sound, et param�tre d'activation du son : son)
if {$sound == 0 || $son == 0} {
tk_messageBox -message [mc {Ce jeu necessite le support du son. Veuillez installer le package snack.}] -type ok -icon info
exec $iwish associations.tcl &
exit
}

catch {
set f [open [file join $baseHome reglages son.conf] "r"]
set sson [gets $f]
close $f
set sound [expr $sound && [lindex $sson 7]]
}

proc setniveauprec2 {c} {
global listeval categorie serie repbasecat listdata Home catedefaut
if {$serie > 0} {
incr serie -1
set ext .cat
set file $categorie
for {set i 1} {$i <= [string length $file]} {incr i 1} {
if {[string match {[0-9]} [string index $file end]] == 1} {
set  file [string range $file 0 [expr [string length $file] -2] ]
}
}
if {$serie != 0} {
set categorie $file$serie
set file $file$serie$ext
} else {
set categorie $file
set file $file$ext
}
set catedefaut $file

set listeval \173[mc {J'entends (Quelle syllabe?) - }]\175\040$file
wm title . "[mc {J'entends (Quelle syllabe?) - }]$file"

init $c
}
}


proc init {c} {
global catedefaut listdata categorie Home font2 repbasecat Home tabrep sonref paramson getcat listeval iwish
array unset tabrep
set listdata ""
if {$catedefaut == "none" || $catedefaut == ""} {
opencat
catch {destroy .opencate}
set ext .cat
set file $getcat$ext
 } else {
set file $catedefaut
}

wm title . "[mc {J'entends (Quelle syllabe?) - }]$file" 

.bframe.b2 configure -command "recommence $c"
#.bframe.b3 configure -command "suite $c"
if {[catch { set f [open [file join $Home categorie $repbasecat $file] "r" ] }] ==1 } {
      set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info] 
      exec $iwish associations.tcl &
      exit
}

if {[catch {set listdata [gets $f]}]==1} {
     set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info]
     close $f
     exec $iwish associations.tcl &
     exit
     } else {
     close $f
     }

foreach it $listdata {
set indice [lsearch -regexp [lindex $it 3] {\**\*}]
if {$indice != -1} {
set tabrep([lindex $it 0]) $indice
} else {
set listdata [lreplace $listdata [lsearch $listdata $it] [lsearch $listdata $it]]
}
}

if {[llength $listdata] <= 2} {
set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info]
exec $iwish associations.tcl &
exit
}



set categorie [lindex [split [lindex [split $file /] end] .] 0]
set listeval \173[mc {J'entends (Quelle syllabe?) - }]\175\040$categorie

set cate $categorie
set cate [supnum $cate]

catch {
set ext ".dat"
set f [open [file join $Home categorie $repbasecat $cate$ext] "r" ] 
set tmp [gets $f]
set sonref [lindex $tmp 0]
set paramson [lindex $tmp 1]
close $f
}
main $c
}

#proc�dures
##########################################################"""""
proc recommence {c} {
global listeval user catedefaut
#enregistreval
#lappend listeval "4 \173[mc {Exercice recommence}]\175"
if {$catedefaut != ""} {
set listeval \173[mc {J'entends (Quelle syllabe?) - }]\175\040$catedefaut
main $c
}
}

proc melange {bas haut} {
global alear nb
set diff [expr $haut - $bas]
for {set i $bas} {$i < $haut} {incr i 1} {
  set alear($i) $i
  }
for {set i 1} {$i < $nb} {incr i 1} {
  set t1 [expr int(rand()*$diff) + 1]
  set t2 [expr int(rand()*$diff) + 1]
  set temp $alear($t1)
  set alear($t1) $alear($t2)
  set alear($t2) $temp
  }
}


##############################################################"""
proc main {c} {
#nbessai nombre de tentatives sur l'item en cours
#score : calcul du pourcentage de r�ussite de l'�l�ve

global alear imgarr etiqarr bgn bgl nbreu font2 nb listdata nbessai listeval categorie score catedefaut
set nbessai 0
set nbreu 1
set score 0

set font1 {Helvetica 12}
set catedefaut $categorie
#set tmp [mc {J'entends (Quelle syllabe?) - }]
#set listeval \173$tmp\175\040$categorie

#on m�lange les associations
set leng [llength $listdata]
for {set i 1} {$i <= $leng} {incr i 1} {
   set t1 [expr int(rand()*$leng)]
   set t2 [expr int(rand()*$leng)]
   set tmp [lindex $listdata $t1]
   set listdata [lreplace $listdata $t1 $t1 [lindex $listdata $t2]]
   set listdata [lreplace $listdata $t2 $t2 $tmp]
   }
#pas plus de huit associations dans le jeu
if {$leng > 8 } {
   set nb 8
   } else {
   set nb $leng
   }
#cr�ation de la barre de progression
image create photo pbien -file [file join sysdata pbien.gif] 
image create photo ppass -file [file join sysdata ppass.gif]
image create photo pmal -file [file join sysdata pmal.gif]
image create photo pneutre -file [file join sysdata pneutre.gif]

image create photo syl -file [file join sysdata syl.gif] 
image create photo passyl -file [file join sysdata passyl.gif] 

image create photo speaker -file [file join sysdata speaker.gif]

for {set i 1} {$i <= $nb} {incr i 1} {
.bframe.lab$i configure -image pneutre -width 30
}

#stockage des images dans un tableau
for {set i 1} {$i <= $nb} {incr i 1} {
    set imgarr($i) [lindex [lindex $listdata [expr $i - 1] ] 0]
    }
#appel de la proc�dure principale
place $c
}

proc soundEnter {c} {
global imgarr flag nbreu repbasecat Home
if {$flag == 0 } {
    set ext .wav
    set son [lindex [split $imgarr($nbreu) .] 0]
    enterstart [file join $Home sons $son$ext]
   }
}

#######################################"
proc place {c} {
global alear imgarr etiqarr bgn bgl nbreu font2 nb listdata nbessai listeval categorie bonnereponse Home tabrep

$c delete all

set bonnereponse [lindex [lindex $listdata [expr $nbreu - 1] ] 2]

image create photo kimage1 -file [file join $Home images $imgarr($nbreu)] -width 130 -height 130
$c create image 490 250 -image speaker -tags "speaker"
set xpos 200
set ypos 150
$c create image $xpos $ypos -image kimage1 -tags img
for {set i 0} {$i < [llength [lindex [lindex $listdata [expr $nbreu -1] ] 3]]} {incr i 1} {
$c create image [expr $xpos - 80 + [expr 80*$i]] [expr $ypos + 80] -image passyl -tags "entendssyl$i"
$c bind entendssyl$i <Any-Enter> "catch {$c delete withtag figure} \n $c itemconf entendssyl$i -image syl"
$c bind entendssyl$i <Any-Leave> "$c itemconf entendssyl$i -image passyl"
$c bind entendssyl$i <1> "verif $c $i"
}

$c bind speaker <Any-Enter> "Soundspeakerin"
$c bind speaker <Any-Leave> "Stop"

update
soundEnter $c
#$c create text 100 100 -text $tabrep([lindex [lindex $listdata [expr $nbreu - 1] ] 0])
#$c create text [expr $xpos - 20] [expr $ypos + 70] -text "_______" -anchor n -tags cible1 -font $font2 -fill $bgl
}

#appel principal
init $c

#gestion des �v�nements
#bind . <Destroy> "quitte"
if {$sound == 1} {
$c bind img <Any-Enter> "soundEnter $c"
$c bind img <Any-Leave> "soundLeave $c"
}

#gestion du son
proc Stop {} {
enterstop
}

proc Soundspeakerin {} {
global sonref Home categorie repbasecat
    enterstart [file join $Home sons $sonref]
}


proc soundLeave {c} {
enterstop
}


proc majbilan {} {
global bonnereponse nbreu listeval score nbessai
        switch $nbessai {
                1 {.bframe.lab$nbreu configure -image pbien -width 30
                lappend listeval 1\040\173$bonnereponse\175
                incr score 10
                }
                2 {.bframe.lab$nbreu configure -image pmal -width 30
                lappend listeval 3\040\173$bonnereponse\175
                #incr score 5
                }
                default {.bframe.lab$nbreu configure -image pmal -width 30
                lappend listeval 3\040\173$bonnereponse\175

                }
         }

}

#gestion du jeu
proc verif {c num} {
global nbreu nb alear imgarr listdata nbessai listeval etiqarr user niveau categorie score repbasecat Home tabrep catedefaut bonnereponse font2 serie
variable repert
#d�termination du tag de l'�tiquette que l'on a d�plac�, des coordonn�es de l'�tiquette, des coordonn�es de la cible
    incr nbessai
# test pour savoir si c'est ok
	if {$tabrep([lindex [lindex $listdata [expr $nbreu -1] ] 0]) == $num} {
          #bell
          # si c'est ok et que le jeu est fini
          if {$nbreu==$nb} {
		catch {$c delete withtag figure}
             $c create text 470 140 -text [mc {Bravo!!}] -font {Arial 24}
	       $c create text 200 30 -text $bonnereponse -anchor n -tags figure -font $font2
             update
             after 2000
             $c delete all
             #on met � jour la fiche bilan
		 majbilan
##########################################################"""""""
           #calcul du score            
            set score [expr $score*10/$nb]
           set pourcent %
           $c create text 200 100 -text "[mc {Score : }] $score\040$pourcent" -font {Arial 24}
            #test pour savoir si on passe au niveau suivant et d�termination du niveau � faire
		lappend listeval \040bilan\040$serie\0402\040$repbasecat\040$repert\040$score
		enregistreval

            if {$score >= 75} { 
            update
            after 2000
            set file $categorie
            set niveau ""
            for {set i 1} {$i <= [string length $file]} {incr i 1} {
               if {[string match {[0-9]} [string index $file end]] == 1} {
                 set niveau [string index $file end]$niveau
                 set  file [string range $file 0 [expr [string length $file] -2] ]
                }
            }
            if {$niveau == ""} {
            set niveau 0
            }
            incr niveau
            set ext .cat
            set file $file$niveau$ext
            if {[catch {set f [open [file join $Home categorie $repbasecat $file] "r"]} ]} {
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            } else {
            #si un autre niveau est requis
            ##set listdata [gets $f]
            close $f
            #enregistreval
            set categorie [lindex [split [lindex [split $file /] end] .] 0]
		set catedefaut $file
		for {set i 1} {$i < 9} {incr i 1} {
		.bframe.lab$i configure -image "" -width 1
		}
		incr serie
		set listeval \173[mc {J'entends (Quelle syllabe?) - }]\175\040$categorie
            init $c
            }
            } else {
#s'il n'y a pas de niveau sup�rieur disponible
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            }
################################################################

            return
            }
#si on n'est pas � la fin du jeu en cours mais que c'est juste
          majbilan
          set nbessai 0
          image create photo figure -file [file join sysdata bien.gif]
	    $c create image 490 140 -image figure -tags figure
	    $c create text 200 30 -text $bonnereponse -anchor n -tags figure -font $font2
          update
          after 2000         
          catch {$c delete withtag figure}
          incr nbreu
          place $c
          return
	    } else {  
#si c'est faux       
          image create photo figure -file [file join sysdata mal.gif]
	    $c create image 490 140 -image figure -tags figure
          }
     
 }




proc suite {c} {
global catedefaut
enregistreval
$c delete all
set catedefaut ""
init $c
}









