############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: paramcat.tcl,v 1.5 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associations.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global basedir Homeconf plateforme repbasecat Home sysFont progaide plateforme file listdata progaide sound baseHome
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source msg.tcl

init $plateforme
inithome
initlog $plateforme $ident
changehome
variable repert
variable repertcat
set arg $argv

wm title . "[mc {Parametres pour la categorie}] $arg"
catch {destroy .wleft}

set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set repbasecat [gets $f]
close $f

set ext .dat
set ext2 .wav
set file $arg$ext
if {[catch { set f [open [file join $Home categorie $repbasecat $file] "r" ] }] != 1} {
set listdata [gets $f]
close $f
} else {
set listdata \173$arg$ext2\175\040\1731\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\175\040\1731\0401\0401\0401\0401\0401\0401\0401\175
}

set soncat [lindex $listdata 0]
for {set i 1} {$i <= [llength [lindex $listdata 1]]} {incr i 1} {
variable b$i
set b$i [lindex [lindex $listdata 1] [expr $i -1]]
}

for {set i 1} {$i <= [llength [lindex $listdata 2]]} {incr i 1} {
variable s$i
set s$i [lindex [lindex $listdata 2] [expr $i -1]]
}

. configure -background blue
frame .wleft -background blue
pack .wleft -side top -fill both -expand yes


if {$sound != 0} {
label .wleft.a1 -text [mc {Entendre}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a1 -row 0 -column 0 -sticky news
}
label .wleft.a2 -text [mc {Voir}] -background #ffff80 -font $sysFont(s)
grid .wleft.a2 -row 0 -column 1 -sticky news
label .wleft.a3 -text [mc {Reconnaitre}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a3 -row 0 -column 2 -columnspan 4 -sticky news
label .wleft.a4 -text [mc {Combiner}] -background #ffff80 -font $sysFont(s)
grid .wleft.a4 -row 0 -column 6 -columnspan 2 -sticky news
label .wleft.a5 -text [mc {Ecrire}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a5 -row 0 -column 8 -columnspan 2 -sticky news
if {$sound != 0} {
checkbutton .wleft.b11 -image [image create photo -file sysdata/b17.jpg] -indicatoron 0 -background #ffff80 -variable b1 -bd 4 -height 90 -width 70
grid .wleft.b11 -row 1 -column 0 -sticky news -padx 3 -pady 3
checkbutton .wleft.b12 -image [image create photo -file sysdata/b18.jpg] -indicatoron 0 -background #ffff80 -variable b2 -bd 4 -height 90 -width 70
grid .wleft.b12 -row 2 -column 0 -sticky news -padx 3 -pady 3
checkbutton .wleft.b13 -image [image create photo -file sysdata/b19.jpg] -indicatoron 0 -background #ffff80 -variable b3 -bd 4 -height 90 -width 70
grid .wleft.b13 -row 3 -column 0 -sticky news -padx 3 -pady 3
}
checkbutton .wleft.b21 -image [image create photo -file sysdata/b7.jpg] -indicatoron 0 -background #ffff80 -variable b4 -bd 4 -height 90 -width 70
grid .wleft.b21 -row 1 -column 1 -sticky news -padx 3 -pady 3
checkbutton .wleft.b22 -image [image create photo -file sysdata/b8.jpg] -indicatoron 0 -background #ffff80 -variable b5 -bd 4 -height 90 -width 70
grid .wleft.b22 -row 2 -column 1 -sticky news -padx 3 -pady 3
checkbutton .wleft.b23 -image [image create photo -file sysdata/b13.jpg] -indicatoron 0 -background #ffff80 -variable b6 -bd 4 -height 90 -width 70
grid .wleft.b23 -row 3 -column 1 -sticky news -padx 3 -pady 3
checkbutton .wleft.b31 -image [image create photo -file sysdata/b1.jpg] -indicatoron 0 -background #ffff80  -variable b7 -bd 4 -height 90 -width 70
grid .wleft.b31 -row 1 -column 2 -sticky news -padx 3 -pady 3
checkbutton .wleft.b32 -image [image create photo -file sysdata/b2.jpg] -indicatoron 0 -background #ffff80 -variable b8 -bd 4 -height 90 -width 70
grid .wleft.b32 -row 2 -column 2 -sticky news -padx 3 -pady 3
checkbutton .wleft.b33 -image [image create photo -file sysdata/b3.jpg] -background #ffff80 -indicatoron 0 -variable b9 -bd 4 -height 90 -width 70
grid .wleft.b33 -row 3 -column 2 -sticky news -padx 3 -pady 3
checkbutton .wleft.b34 -image [image create photo -file sysdata/b4.jpg] -indicatoron 0 -background #ffff80 -variable b10 -bd 4 -height 90 -width 70
grid .wleft.b34 -row 4 -column 2 -sticky news -padx 3 -pady 3
if {$sound != 0} {
checkbutton .wleft.s11 -text [mc {Son}] -indicatoron 0 -variable s1 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s11 -row 1 -column 3 -rowspan 2 -sticky news -padx 3 -pady 3
checkbutton .wleft.s12 -text [mc {Son}] -indicatoron 0 -variable s2 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s12 -row 3 -column 3 -rowspan 2 -sticky news -padx 3 -pady 3
}

checkbutton .wleft.b41 -image [image create photo -file sysdata/b9.jpg] -indicatoron 0 -background #ffff80 -variable b11 -bd 4 -height 90 -width 70
grid .wleft.b41 -row 1 -column 4 -sticky news -padx 3 -pady 3
checkbutton .wleft.b42 -image [image create photo -file sysdata/b14.jpg] -indicatoron 0 -background #ffff80 -variable b12 -bd 4 -height 90 -width 70
grid .wleft.b42 -row 2 -column 4 -sticky news -padx 3 -pady 3
if {$sound != 0} {
checkbutton .wleft.b43 -image [image create photo -file sysdata/b16.jpg] -indicatoron 0 -background #ffff80 -variable b13 -bd 4 -height 90 -width 70
grid .wleft.b43 -row 3 -column 4 -sticky news -padx 3 -pady 3
}
if {$sound != 0} {
checkbutton .wleft.s13 -text [mc {Son}] -indicatoron 0 -variable s3 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s13 -row 1 -column 5 -sticky news -padx 3 -pady 3
checkbutton .wleft.s14 -text [mc {Son}] -indicatoron 0 -variable s4 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s14 -row 2 -column 5 -sticky news -padx 3 -pady 3
}
checkbutton .wleft.b51 -image [image create photo -file sysdata/b5.jpg] -indicatoron 0 -background #ffff80 -variable b14 -bd 4 -height 90 -width 70
grid .wleft.b51 -row 1 -column 6 -sticky news -padx 3 -pady 3
checkbutton .wleft.b52 -image [image create photo -file sysdata/b6.jpg] -indicatoron 0 -background #ffff80 -variable b15 -bd 4 -height 90 -width 70
grid .wleft.b52 -row 2 -column 6 -sticky news -padx 3 -pady 3
checkbutton .wleft.b53 -image [image create photo -file sysdata/b10.jpg] -indicatoron 0 -background #ffff80 -variable b16 -bd 4 -height 90 -width 70
grid .wleft.b53 -row 3 -column 6 -sticky news -padx 3 -pady 3
checkbutton .wleft.b54 -image [image create photo -file sysdata/b11.jpg] -indicatoron 0 -background #ffff80 -variable b17 -bd 4 -height 90 -width 70
grid .wleft.b54 -row 4 -column 6 -sticky news -padx 3 -pady 3
if {$sound != 0} {
checkbutton .wleft.s15 -text [mc {Son}] -indicatoron 0 -variable s5 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s15 -row 1 -rowspan 2 -column 7 -sticky news -padx 3 -pady 3
checkbutton .wleft.s16 -text [mc {Son}] -indicatoron 0 -variable s6 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s16 -row 3 -column 7 -sticky news -padx 3 -pady 3
checkbutton .wleft.s17 -text [mc {Son}] -indicatoron 0 -variable s7 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s17 -row 4 -column 7 -sticky news -padx 3 -pady 3
}
checkbutton .wleft.b61 -image [image create photo -file sysdata/b12.jpg] -indicatoron 0 -background #ffff80 -variable b18 -bd 4 -height 90 -width 70
grid .wleft.b61 -row 1 -column 8 -sticky news -padx 3 -pady 3
if {$sound != 0} {
checkbutton .wleft.b62 -image [image create photo -file sysdata/b15.jpg] -indicatoron 0 -background #ffff80 -variable b19 -bd 4 -height 90 -width 70
grid .wleft.b62 -row 2 -column 8 -sticky news -padx 3 -pady 3
}
if {$sound != 0} {
checkbutton .wleft.s18 -text [mc {Son}] -indicatoron 0 -variable s8 -relief flat -background #ffff80 -font $sysFont(s)
grid .wleft.s18 -row 1 -column 9 -sticky news -padx 3 -pady 3
}


	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_param.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	} else {
	set fichier [file join [pwd] aide fr_param.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_param.htm]
}


button .wleft.ok -text [mc {  ok  }] -font $sysFont(s) -command {destroy .}
grid .wleft.ok -row 4 -column 4 -padx 3 -pady 3

button .wleft.aide -text [mc {Aide}] -font $sysFont(s) -command "exec $progaide file:$fichier &"
grid .wleft.aide -row 4 -column 5 -padx 3 -pady 3

bind . <Destroy> "sauvecat"

proc sauvecat {} {
global file Home categorie repbasecat soncat listdata

for {set i 1} {$i <= [llength [lindex $listdata 1]]} {incr i 1} {
variable b$i
}
for {set i 1} {$i <= [llength [lindex $listdata 2]]} {incr i 1} {
variable s$i
#set s$i 1
}
set listtmp \173[lindex $listdata 0]\175\040\173$b1\040$b2\040$b3\040$b4\040$b5\040$b6\040$b7\040$b8\040$b9\040$b10\040$b11\040$b12\040$b13\040$b14\040$b15\040$b16\040$b17\040$b18\040$b19\175\040\173$s1\040$s2\040$s3\040$s4\040$s5\040$s6\040$s7\040$s8\175

set f [open [file join $Home categorie $repbasecat $file] "w" ]
puts $f $listtmp
close $f
}

