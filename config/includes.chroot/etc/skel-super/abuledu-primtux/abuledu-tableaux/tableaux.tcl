#!/bin/sh
#tableaux.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Andr� Connes <davidlucardi@aol.com>
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : tableaux.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 15/04/2003 Modifi� le 05/03/2005
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: tableaux.tcl,v 1.11 2006/04/03 14:02:05 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier   
#  @project    Le terrier
#  @copyright  Andr� Connes
# 
#***********************************************************************
global sysFont

source tableaux.conf
source msg.tcl
source aider.tcl
source fonts.tcl

#if { [catch {package require Iwidgets}] == 1} {
#  tk_messageBox -message "Il est n�cessaire d'installer le package Iwidgets"
#}

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.99)]x[expr int([winfo vrootheight .]*0.9)]+0+0
. configure -background blue
wm title . [mc title_m]
frame .frame -background blue -height 300 -width 200
pack .frame -side top -fill both -expand yes

# gestion de l'espace de l'utilisateur

set didacticiel ""
set forms ""

  if {![file exists [file join  $glob(home_tableaux)]]} {
	catch { file mkdir [file join $glob(home_tableaux)]}
  }
  if {![file exists [file join  $glob(home_reglages)]]} {
	catch { file mkdir [file join $glob(home_reglages)] }
  }

  set f [open [file join $glob(home_reglages) trace_user] "w"]
  puts $f $glob(trace_user)
  close $f

  #
  # langue par defaut
  #
  if {![file exists [file join  $glob(home_reglages) lang.conf]]} {
	set f [open [file join $glob(home_reglages) lang.conf] "w"]
	puts $f "fr"
	close $f
  }
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

# if {![file exists [file join  $glob(home_reglages) dir_exos.conf]]} {
#	set f [open [file join $glob(home_reglages) dir_exos.conf] "w"]
#	puts $f "Commun"
#	close $f
# }

  catch { file mkdir [file join $glob(home_msgs)] }
  # astuce suivante : ajoute automatiquement toute nouvelle langue chez l'utilisateur
  foreach f [glob [file join msgs *.msg]] {
	catch { file copy [file join $f] [file join $glob(home_msgs)]}
  }
  if {![file exists [file join  $glob(trace_dir)]]} {
	file mkdir [file join $glob(trace_dir)]
  }

  #
  #si le fichier utilisateur n'existe pas, le cr�er !
  #
  if { ![file exists $glob(trace_user).level] } {
	set f [open [file join $glob(trace_user).level] "w"]
	foreach  local_didacticiel $glob(didacticiels) {
		puts $f "$local_didacticiel -1 -1 -1 -1 -1"
	}
	close $f
  }

proc setlang {lang didacticiel} {
  global env glob
  set env(LANG) $lang
  catch {set f [open [file join $glob(home_reglages) lang.conf] "w"]}
  puts $f [file rootname [file tail $lang]]
  close $f

  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

  main_loop
}

proc setwindowsusername {} {
    global user
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text "[mc {Ton nom}] ?" -background grey
    pack .utilisateur.frame.labobj -side top 
    entry .utilisateur.frame.entobj -font {Helvetica 10} -width 10
    pack .utilisateur.frame.entobj -side top 
    button .utilisateur.frame.ok -background gray75 -text "Ok" -command "verifnom"
    pack .utilisateur.frame.ok -side top -pady 10
}

  proc verifnom {} {
   global glob
   set nom [string tolower [string trim [string map {\040 ""} [.utilisateur.frame.entobj get]]]]
    if {$nom ==""} {
	set nom eleve
    }
    # sauver le r�glage du nom
    catch {set f [open [file join $glob(home_tableaux) reglages trace_user] "w"]}
    set glob(trace_user) "$glob(trace_dir)/$nom"
    puts $f $glob(trace_user)
    close $f
    #
    #si le fichier utilisateur n'existe pas, le cr�er !
    #
    if { ![file exists $glob(trace_user).level] } {
	set f [open [file join $glob(trace_user).level] "w"]
	foreach  local_didacticiel $glob(didacticiels) {
		puts $f "$local_didacticiel -1 -1 -1 -1 -1"
	}
	close $f
    }
    catch {destroy .utilisateur}
}

###########################################################################################""
proc main_loop {} {
  global sysFont glob
  #
  # Creation du menu utilise comme barre de menu:
  #
  catch {destroy .menu}
  catch {destroy .frame}
  #catch {destroy .wcenter}
  #catch {destroy .wright}

  menu .menu -tearoff 0

  # Creation du menu Fichier
  menu .menu.fichier -tearoff 0
  .menu add cascade -label [mc Fichier] -menu .menu.fichier
  .menu.fichier add command -label [mc Bilan] -command "lanceappli bilan.tcl 0"
  .menu.fichier add separator
  .menu.fichier add command -label [mc Quitter] -command exit

  # Cr�ation du menu Activit�s
  menu .menu.action -tearoff 0
  .menu add cascade -label [mc Activites] -menu .menu.action
  foreach didacticiel $glob(didacticiels) {
    .menu.action add command -label $didacticiel -command "lanceappli tableau.tcl $didacticiel"
  }

  # Creation du menu Options
  menu .menu.options -tearoff 0
  .menu add cascade -label [mc Options] -menu .menu.options

  set ext .msg
  menu .menu.options.lang -tearoff 0 
  .menu.options add cascade -label [mc Langue] -menu .menu.options.lang

  foreach i [glob [file join  $glob(home_msgs) *$ext]] {
    set langue [string range [string map {.$ext ""} [file tail $i]] 0 end-4]
    .menu.options.lang add radio -label $langue -variable langue -command "setlang $langue {}"
  }

  # Nom de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    .menu add command -label [mc Utilisateur] -command "setwindowsusername"
  }

  # Cr�ation du menu Aide
  menu .menu.aide -tearoff 0
  .menu add cascade -label [mc Aide] -menu .menu.aide
  .menu.aide add command -label [mc Aide] -command aider
  .menu.aide add command -label [mc {� propos ...}] -command "source apropos.tcl"
  . configure -menu .menu

  #######################################################################"
  . configure -background blue
  frame .frame -background blue -height 300 -width 200
  pack .frame -side top -fill both -expand yes

  ###################On cr�e un canvas####################################

  set c .frame.c
  canvas $c -width $glob(width) -height $glob(height) -bg blue -highlightbackground blue
  pack $c -expand true 

  set nl 0
  set nc 0
  foreach didacticiel $glob(didacticiels) {
    button $c.$didacticiel \
	-image [image create photo -file [file join $didacticiel ${didacticiel}_logo.gif]] \
	-borderwidth 5 -cursor heart \
	-command "lanceappli tableau.tcl $didacticiel"
    grid $c.$didacticiel -column $nc -row $nl -sticky e -padx 5 -pady 5
    # d�terminer ligne/colonne du bouton suivant
    if { $nl > 1 } {
      incr nc
      set nl 0
    } else {
      incr nl
    }

  }

  button $c.quitter \
	-image [image create photo -file [file join sysdata quitter.gif]] \
	-borderwidth 5 -cursor heart \
	-command exit
  grid $c.quitter -column 2 -row 2 -sticky e -padx 5 -pady 5

  set myimage [image create photo -file sysdata/background.gif]
  label $c.imagedisplayer -image $myimage -background blue
  grid $c.imagedisplayer -column 2 -row 1

} ;# fin main_loop

bind . <Control-q> {exit}

proc lanceappli {appli niveau} {
  set id_appli [file join $appli]
  exec wish $id_appli $niveau &
}

########################################################################
#                           programme principal                        #
########################################################################

main_loop

