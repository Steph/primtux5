#!/bin/sh
#closure.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont essais auto couleur categorie indp aide startdirect user Homeconf repertoire tabaide tablistevariable tablongchamp tabstartdirect Home initrep baseHome lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl


proc cancelkey {A} {
set A ""
#focus .
}

#variables
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice

set essais 0
set auto 0
set aide 2
set couleur pink
set categorie ""
set indp 0
#########set juste 0
set startdirect 1
set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right

tux_commence

frame .barre -width 40
pack .barre -side left -fill both

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_phrase [lindex $aller 2]
close $f
set aide [lindex $aller_phrase 0]
#set categorie [lindex $aller_phrase 3]
set startdirect [lindex $aller_phrase 4]
set lecture_mot [lindex $aller_phrase 5]
set lecture_mot_cache [lindex $aller_phrase 6]
}
set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
##############"vvvvvv.text configure -state disabled
bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"
if {$auto == 1} {
set aide $tabaide(phrase)
set longchamp $tablongchamp(phrase)
set listevariable $tablistevariable(phrase)
set startdirect $tabstartdirect(phrase)
set lecture_mot $tablecture_mot(phrase)
set lecture_mot_cache $tablecture_mot_cache(phrase)
}

focus .text
##orig .text configure -state disabled -selectbackground white -selectforeground black
.text configure -selectbackground white -selectforeground black
wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 2] 1]"
label .menu.titre -text "[lindex [lindex $listexo 2] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1

proc main {t} {

global sysFont plist auto listexo iwish aide user startdirect filuser

button .barre.haut -image [image create photo imag1 -file [file join sysdata fhaut.gif]] -command "defileh .text"
place .barre.haut -y 60
button .barre.bas -image [image create photo imag2 -file [file join sysdata fbas.gif]] -command "defileb .text"
place  .barre.bas  -y 110
button .barre.ok -image [image create photo imagbut -file [file join sysdata ok.gif] ] -command "verif .text"
pack .barre.ok -side bottom
#catch {destroy .menu.b1}
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "exec $iwish aller.tcl $filuser & ;exit"
 
#pack .menu.b1 -side right 
    catch {destroy .menu.lab}
    catch {destroy .menu.titre}
    label .menu.lab -text [lindex [lindex $listexo 2] 2]
#[mc {Clique sur une phrase (ou un paragraphe) et d�place-la en appuyant sur les boutons.}]
    pack .menu.lab -side left -fill both -expand 1

set nbmotscaches 0
$t configure -state normal
set what [mc {Clique sur une phrase (ou un paragraphe) et deplace-la en appuyant sur les boutons fleches, puis valide en appuyant sur le bouton pouce.}]
if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what
wm geometry .wtux +200+340

# S�lection du mode auto ou manuel pour la g�n�ration de l'exercice
    if {$auto==0} {
    pauto $t
    } else {
    pmanuel $t
    }
    $t configure -state disabled  
bind .text <ButtonRelease-1> ""
bind . <KeyPress> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""
}

proc pauto {t} {
global sysFont plist indp listerep leng iwish filuser lecture_mot lecture_mot_cache

set cur 1.0
set i 0
set plist {}

    while 1 {
      set cur [$t search -regexp -count len {[A-Z]} $cur end]
      if {$cur == ""} {
	    break
	}
      set tmp $cur
      set cur [$t search -regexp -count length {[.!?\012]} $cur end]
	if {$cur == ""} {
	    break
	}
      lappend plist [$t get $tmp "$cur + 1c"]
   }



#on m�lange les phrases
set leng [llength $plist]

if {$leng > 5} {
  set base  [expr int(rand()*($leng - 5))]
  set plist [lreplace $plist [expr $base + 5] end]
  if {[llength $plist]>5} {
  set plist [lreplace $plist 0 [expr [llength $plist] - 6]]
  }
}

set leng [llength $plist]
set listerep $plist

for {set j 0} {$j <= [expr int(rand()*3)]} {incr j 1} {
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $plist $t1]
  set plist [lreplace $plist $t1 $t1 [lindex $plist $t2]]
  set plist [lreplace $plist $t2 $t2 $tmp]
  }
}


for {set i [expr [llength $plist]-1]} {$i >= 0} {incr i -1} {

set count [llength [regexp -inline -all -- {\S+} [lindex $plist $i]]]
if {$count <=1} { set plist [lreplace $plist $i $i] }
}


   if {[llength $plist] < 2} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }
$t delete 1.0 end
for {set i 0} {$i < [llength $plist]} {incr i 1} {

$t insert current [lindex $plist $i]\012\012
$t tag add d$i "current - [expr [string length [lindex $plist $i]] + 2] char" "current - 2 char"
#$t tag bind d$i <ButtonRelease-1> "highlight $i $t"
########################################vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
 	if {$lecture_mot == "1" } {
	$t tag bind d$i <ButtonRelease-1> "speaktexte [list [lindex $plist $i]]; highlight $i $t"
	$t tag bind d$i <Any-Enter> "$t configure -cursor target"
	$t tag bind d$i <Any-Leave> "$t configure -cursor left_ptr"

	} else {
	$t tag bind d$i <ButtonRelease-1> "highlight $i $t"
	}

    if {$lecture_mot_cache == "0" } {catch {destroy .menu.bb1}}
######################################""vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
}
highlight $indp $t

}

proc defileh {t} {
global sysFont plist indp lecture_mot
catch {destroy .barre.tete}
tux_continue
if {$indp <= 0} {
return}
set plist [linsert $plist [expr $indp -1] [lindex $plist $indp]]
set plist [lreplace $plist [expr $indp + 1] [expr $indp + 1]]
$t configure -state normal
$t delete 1.0 end
for {set i 0} {$i < [llength $plist]} {incr i 1} {

$t insert current [lindex $plist $i]\012\012
$t tag add d$i "current - [expr [string length [lindex $plist $i]] + 2] char" "current - 2 char"
 	if {$lecture_mot == "1" } {
	$t tag bind d$i <ButtonRelease-1> "speaktexte [list [lindex $plist $i]]; highlight $i $t"
	} else {
	$t tag bind d$i <ButtonRelease-1> "highlight $i $t"
	}
}
highlight [incr indp -1] $t

$t see [lindex [$t tag ranges d$indp] 0]
$t configure -state disabled 
}

proc defileb {t} {
global sysFont plist indp lecture_mot
catch {destroy .barre.tete}
tux_continue
if {$indp >= [expr [llength $plist] -1]} {
return}
set plist [linsert $plist [expr $indp + 2] [lindex $plist $indp]]
set plist [lreplace $plist [expr $indp] [expr $indp]]
$t configure -state normal
$t delete 1.0 end
for {set i 0} {$i < [llength $plist]} {incr i 1} {

$t insert current [lindex $plist $i]\012\012
$t tag add d$i "current - [expr [string length [lindex $plist $i]] + 2] char" "current - 2 char"
 	if {$lecture_mot == "1" } {
	$t tag bind d$i <ButtonRelease-1> "speaktexte [list [lindex $plist $i]]; highlight $i $t"
	} else {
	$t tag bind d$i <ButtonRelease-1> "highlight $i $t"
	}
}
highlight [incr indp] $t
$t see [lindex [$t tag ranges d$indp] 0]
$t configure -state disabled 
}

proc highlight {i t} {
global sysFont plist indp
set indp $i
for {set j 0} {$j < [llength $plist]} {incr j 1} {
$t tag configure d$j -background white
}
$t tag configure d$i -background pink
}

proc verif {t} {
global sysFont essais plist listerep aide categorie user juste iwish
set juste 0
incr essais
set compt 0
    for {set i 0} {$i < [llength $plist]} {incr i 1} {
        incr compt
        if {[lindex $plist $i] != [lindex $listerep $i]} {
        bell
        incr compt -1
        }
   }
if {$compt != [llength $plist]} {
       catch {destroy .barre.tete}
       label .barre.tete -image [image create photo imag3 -file [file join sysdata pmal.gif]]
       pack .barre.tete -side top
	if {$essais <= 1} {tux_echoue1} else {tux_echoue2}
         if {$essais >= $aide } {
                afficheaide $listerep
        }
       } else {
       catch {destroy .menu.lab}
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s).}] $essais]
    set str1 [mc {Exercice Phrases melangees}]
    set juste 1
    #enregistreval $str1 $categorie $str2 $user

       label .menu.lab -text $str0$str2
       pack .menu.lab -side left
       bell
       catch {destroy .barre.haut}
       catch {destroy .barre.bas}
       catch {destroy .barre.tete}
       catch {destroy .barre.ok}
       #catch {destroy .menu.b1}
	.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
       #pack .menu.b1 -side right
       #button .menu.suiv -text [mc {Autre texte}] -command "boucle" -height 2
	 #pack .menu.suiv -side right

       #label .menu.tete -image [image create photo imag4 -file [file join sysdata pbien.gif]]
       #pack .menu.tete -side right
set score [expr 100- 20*($essais -1)]
if {$score < 0} {set score 0}
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

       }
    
}

proc pmanuel {t} {
global sysFont plist indp listerep couleur leng lecture_mot lecture_mot_cache

set plist {}

set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    lappend plist $str        
    }

set leng [llength $plist]
    if {$leng <= 1} {
pauto $t
    return
    }

set listerep $plist

for {set j 0} {$j <= [expr int(rand()*3)]} {incr j 1} {
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $plist $t1]
  set plist [lreplace $plist $t1 $t1 [lindex $plist $t2]]
  set plist [lreplace $plist $t2 $t2 $tmp]
  }
}
$t delete 1.0 end
for {set i 0} {$i < [llength $plist]} {incr i 1} {

$t insert current [lindex $plist $i]\012\012
$t tag add d$i "current - [expr [string length [lindex $plist $i]] + 2] char" "current - 2 char"
#$t tag bind d$i <ButtonRelease-1> "highlight $i $t"
###############################"vvvvvvvvvvvvvvvvvvvvvvvvvvvv
 	if {$lecture_mot == "1" } {
	$t tag bind d$i <ButtonRelease-1> "speaktexte [list [lindex $plist $i]]; highlight $i $t"
	$t tag bind d$i <Any-Enter> "$t tag bind d$i configure -cursor target"
	$t tag bind d$i <Any-Leave> "$t tag bind d$i configure -cursor left_ptr"
	} else {
	$t tag bind d$i <ButtonRelease-1> "highlight $i $t"
	}

    if {$lecture_mot_cache == "0" } {catch {destroy .menu.bb1}}
#################################vvvvvvvvvvvvvvvvvvvvvvvvvv
}
highlight $indp $t
}

proc afficheaide {listerep} {
global sysFont
catch {destroy .w1}
toplevel .w1
.w1 configure -width 300 -height 100
wm title .w1 [mc {Aide}]
wm geometry .w1 -0-0
wm transient .w1 .
text .w1.text1 -yscrollcommand ".w1.scrolly set" -width 20 -height 10 -setgrid true -wrap word -background white -font $sysFont(l) -selectbackground white -selectforeground black
scrollbar .w1.scrolly -command ".w1.text1  yview"
pack .w1.scrolly -side right -fill y
pack .w1.text1 -expand yes -fill both

for {set i 0} {$i < [llength $listerep]} {incr i 1} {
 .w1.text1 insert current [lindex $listerep $i]\012\012
}
}

if {$startdirect == 0 } {
main .text
}

proc fin {} {

global sysFont categorie user essais juste leng listexo iwish filuser aide startdirect repertoire lecture_mot lecture_mot_cache
variable repertconf
set score [expr 100- 20*($essais -1)]

if {$score < 0} {set score 0}


set str2 [format [mc {%1$s essai(s) pour %2$s phrase(s).}] $essais $leng]
if {$juste == 1} {
set str2 "[mc {Exercice termine}] : $str2"
}

switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}

switch $lecture_mot_cache {
1 { set lectmotconf [mc {Le texte peut �tre entendu.}]}
0 { set lectmotconf [mc {Le texte ne peut pas �tre entendu.}]}
}

switch $lecture_mot {
1 { set lectmotcacheconf [mc {Les phrases peuvent �tre entendues.}]}
0 { set lectmotcacheconf [mc {Les phrases ne peuvent pas �tre entendues.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Aide : $aideconf"
set exoconf "$exoconf  Son : $lectmotconf $lectmotcacheconf"

    enregistreval [mc {Exercice Phrases melangees}]\040[lindex [lindex $listexo 2] 1] \173$categorie\175 $str2 $score $repertconf 2 $user $exoconf $repertoire

exec $iwish aller.tcl $filuser &
exit
}

proc boucle {} {
global  categorie startdirect essais listexo auto leng juste sysFont user
set str2 [format [mc {%1$s essai(s) pour %2$s phrase(s).}] $essais $leng]
if {$juste == 1} {
set str2 "[mc {Exercice termine}] : $str2"
}
enregistreval [mc {Exercice Phrases melangees}] \173$categorie\175 $str2 $user
set leng 0
set essais 0
set listexo ""
set juste 0
set categorie "Au choix"
catch {destroy .w1}
catch {destroy .menu.tete}
catch {destroy .menu.lab}
catch {destroy .menu.b1}
catch {destroy .menu.suiv}
button .menu.b1 -text [mc {Commencer}] -command "main .text"
pack .menu.b1 -side right




.text configure -state normal
set auto [charge .text $categorie]
.text configure -state disabled
focus .text

if {$startdirect == 0 } {
main .text
}
}
