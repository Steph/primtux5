#!/bin/sh
#espace.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user strbilan police Homeconf repertoire tabaide tablistevariable tablongchamp tabstartdirect Home initrep baseHome essaiseval totaleval lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set nbreu 0
set essais 0
set essaiseval 0
set totaleval 0
set auto 0
set couleur yellow
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set strbilan ""

set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
switch $plateforme {
    unix {
	set police $sysFont(esp) 
    }
    windows {
	set police $sysFont(esp) 
    }
}


initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right

tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_espace [lindex $aller 5]
close $f
set aide [lindex $aller_espace 0]
set longchamp [lindex $aller_espace 1]
set listevariable [lindex $aller_espace 2]
#set categorie [lindex $aller_espace 3]
set startdirect [lindex $aller_espace 4]
set lecture_mot [lindex $aller_espace 5]
set lecture_mot_cache [lindex $aller_espace 6]

}

set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"

if {$auto == 1} {
set aide $tabaide(espace)
set longchamp $tablongchamp(espace)
set listevariable $tablistevariable(espace)
set startdirect $tabstartdirect(espace)
set lecture_mot $tablecture_mot(espace)
set lecture_mot_cache $tablecture_mot_cache(espace)
}
#focus .text
#.text configure -state disabled
wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 5] 1]"
label .menu.titre -text "[lindex [lindex $listexo 5] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1



proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot police aide user startdirect lecture_mot_cache
set nbmotscaches 0
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""

    if {$lecture_mot_cache == "0" } {
catch {destroy .menu.bb1} 
}



$t configure -font $police -width 30 -height 10 -state normal
set what [mc {Clique pour separer les mots de la phrase.}]

if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what

# S�lection du mode auto ou manuel pour la g�n�ration de l'exercice
    if {$auto==0} {
    pauto $t
    } else {
    pmanuel $t
    }
    $t configure -state disabled -selectbackground white -selectforeground black
}


proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide totalp indexp iwish filuser
#########################################################################"
    set plist ""
    set cur 1.0
    while 1 {
      set cur [$t search -regexp -count len {[A-Z]} $cur end]
      if {$cur == ""} {
	    break
	}
      set tmp $cur
      set cur [$t search -regexp -count length {[.!?\012]} $cur end]
	if {$cur == ""} {
	    break
	}
      set wordlist [regexp -inline -all -- {\S+} [$t get $tmp $cur]]
      if {[$t get $tmp "$cur + 1c"]!=" "} {
	set temp [regexp -inline -all -- {\S+} [$t get $tmp "$cur + 1c"]]
      regsub -all {[,;]} $temp "" temp
	regsub -all {[\\\"\(\)\{\}\[\]]} $temp "" temp
    regsub -all \040+ $temp \040 temp

	lappend plist $temp
      }
    }
for {set i [expr [llength $plist]-1]} {$i >= 0} {incr i -1} {
set count [llength [regexp -inline -all -- {\S+} [lindex $plist $i]]]

if {$count <=1} { set plist [lreplace $plist $i $i] }
}
if {[llength $plist] == 0} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }
#############################################################
set totalp [expr [llength $plist] -1]
set indexp 0
for {set i 1} {$i <= $totalp} {incr i 1} {
   set t1 [expr int(rand()*$totalp)]
   set t2 [expr int(rand()*$totalp)]
   set tmp [lindex $plist $t1]
   set plist [lreplace $plist $t1 $t1 [lindex $plist $t2]]
   set plist [lreplace $plist $t2 $t2 $tmp]
   }
if {$totalp > 4} {
set plist [lreplace $plist $totalp $totalp]
set totalp 4
}
$t delete 1.0 end
goon $t
}

proc goon {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide total totalp indexp essais nbreu linepos listexo essaiseval lecture_mot_cache
set phrase [string map {\{ "" \} ""} [lindex $plist $indexp]]
catch {destroy .menu.lab}
catch {destroy .menu.titre}
label .menu.lab -text "[lindex [lindex $listexo 5] 2] - [mc {Numero}] [expr $indexp + 1] [mc {sur}] $totalp" -justify center
pack .menu.lab
$t configure -width 50

#regsub -all \"+ $phrase "" phrase
set tmp ""
set total 0
set nbreu 0
set essais 0
for {set i 0} {$i < [string length $phrase]} {incr i 1} {
set tmp $tmp\040[string index $phrase $i]
}
set phrase $tmp
$t insert end \n\n
set linepos [$t index "[$t index "end -1c"] linestart"]
$t tag remove mauvais 1.0 $linepos
$t insert end $phrase
$t yview -pickplace $linepos
$t tag add mauvais $linepos "end -1c"
set cur $linepos
while 1 {
set cur [$t search \040\040 $cur end]
      if {$cur == ""} {
	    break
	}
$t delete $cur "$cur + 2c"
$t tag add bon $cur
incr total

$t tag remove mauvais $cur
set cur "$cur + 1c"
}
$t tag configure rouge -background red
$t tag configure jaune -background yellow
bind $t <ButtonRelease-1> "verification $t"

bind $t <Any-Enter> "$t configure -cursor hand1"
bind $t <Any-Leave> "$t configure -cursor left_ptr"
    if {$lecture_mot_cache == "1" } {
.menu.bb1 configure -command "speaktexte [list [lindex $plist $indexp]]" 
}

}

###############################################################"""
proc verification {t} {
global sysFont nbreu essais total indexp linepos totalp user categorie strbilan essaiseval totaleval essaiseval
if {[$t get current] != " "} {return}
$t configure -state normal
set ind [lsearch [$t tag names current] "bon"] 
if {$ind != -1} {
$t tag remove bon current
$t insert current "  "
$t tag add jaune "current - 2c" "current + 1c"
incr nbreu
incr essais

    if {$nbreu >= $total} {
    catch {destroy .menu.lab}
    set indc [expr $indexp + 1]
    set str2 [format [mc {N�%1$s : %2$s essai(s) pour %3$s espace(s).}] $indc $essais $total]
    set strbilan $strbilan$str2
    set message [format [mc {%1$s essai(s) pour %2$s espace(s), phrase %3$s sur %4$s.}] $essais $total [expr $indexp + 1] $totalp]
    set totaleval [expr $totaleval + $total]
   set essaiseval [expr $essaiseval + $essais]
 set score [expr int(($total*100)/($essais + $essais - $total))]
if {$score <50} {tux_triste_phrase $score}
if {$score >=50 && $score <75 } {tux_moyen_phrase $score}
if {$score >=75} {tux_content_phrase $score}

    label .menu.lab -text $message
    pack .menu.lab
    bell
    update
    after 3000
    if {[incr indexp]>= $totalp} {
    .menu.lab configure -text [mc {Exercice termine. }]
set score [expr int((($totaleval*100)/($essaiseval))*($indexp*100/$totalp)/100)]
if {$score <50} {tux_triste_final $score}
if {$score >=50 && $score <75 } {tux_moyen_final $score}
if {$score >=75} {tux_content_final $score}

    $t tag remove mauvais 1.0 end
    } else {
    goon $t
	tux_phrasesuivante
    }
    }
$t configure -state disabled

return
} 
set tmp current

set ind [lsearch [$t tag names current] "mauvais"] 
if {$ind != -1} {

set cur current
while 1 {
set ind1 [lsearch [$t tag names $cur] "mauvais"] 
if {$ind1!=-1 && [$t index $cur] !=$linepos } {
set cur "$cur -1c"
} else {
break
}
}

set ind1 "$cur + 1c"
if {[$t index $cur] =="1.0"} {
set ind1 "1.0"
}


set cur $tmp
while 1 {
set ind2 [lsearch [$t tag names $cur] "mauvais"] 
if {$ind2!=-1} {
set cur "$cur + 1c"
} else {
break
}
}
set ind2 $cur

$t tag add rouge $ind1 $ind2
$t tag remove mauvais $ind1 $ind2
incr essais

$t configure -state disabled

bell
return
} 
$t configure -state disabled

}
##################################################################
proc pmanuel {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches couleur aide longchamp longmot listeaide totalp indexp
set totalp 0
set indexp 0
set plist ""
# Construction de la liste des mots � cacher, � partir des tags
set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    incr totalp

    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    regsub -all \040+ $str \040 str
    regsub -all {[,;:]} $str "" str

    lappend plist $str        
    }

for {set i [expr [llength $plist]-1]} {$i >= 0} {incr i -1} {
if {[llength [lindex $plist $i]] <=1} { set plist [lreplace $plist $i $i] }
}
set totalp [llength $plist]
#Si aucun mot n'a �t� masqu�, on repasse en mode auto.
    if {$totalp == 0} {
    pauto $t
    return
    }
$t delete 1.0 end

goon $t

}


if {$startdirect == 0 } {
main .text
}


proc fin {} {
global sysFont categorie strbilan user listexo iwish essaiseval totaleval totalp indexp filuser startdirect repertoire lecture_mot_cache
variable repertconf
#set score [expr ($essaiseval*100)/($totaleval + $fauteseval)]
if {$essaiseval != 0} {
#set score [expr ($totaleval*100)/($essaiseval*($totalp - $indexp + 1))]
set score [expr int((($totaleval*100)/($essaiseval))*($indexp*100/$totalp)/100)]
} else {
set score 0
}

    set str1 [mc {Exercice Phrases sans espaces}]
switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte nest pas visible au debut}]}
}
switch $lecture_mot_cache {
1 { set lectmotcacheconf [mc {Les phrases peuvent �tre entendues.}]}
0 { set lectmotcacheconf [mc {Les phrases ne peuvent pas �tre entendues.}]}
}


set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf"
set exoconf "$exoconf  Son : $lectmotcacheconf"

    enregistreval $str1\040[lindex [lindex $listexo 5] 1] \173$categorie\175 $strbilan $score $repertconf 5 $user $exoconf $repertoire

exec $iwish aller.tcl $filuser &
exit
}



proc boucle {} {
global  categorie startdirect sysFont strbilan user auto
variable repertconf

    set str1 [mc {Exercice Phrases sans espaces}]
    enregistreval $str1 \173$categorie\175 $strbilan $user
set strbilan ""
set categorie "Au choix"
.text configure -state normal -font $sysFont(l)
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
.menu.b1 configure -text [mc {Commencer}] -command "main .text"
catch {destroy .menu.suiv}
catch {destroy .menu.lab}
if {$startdirect == 0 } {
main .text
}
}





















