#!/bin/sh
#closure.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    :
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user Homeconf repertoire listexo tabaide tablistevariable tablongchamp tabstartdirect Home initrep baseHome aller_closure lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set nbreu 0
set essais 0
set auto 0
set couleur green
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1

set filuser [lindex $argv 1]

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}

initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0

frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right

tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_closure [lindex $aller 0]
close $f
set aide [lindex $aller_closure 0]
set longchamp [lindex $aller_closure 1]
set listevariable [lindex $aller_closure 2]
#set categorie [lindex $aller_closure 3]
set startdirect [lindex $aller_closure 4]
set lecture_mot [lindex $aller_closure 5]
set lecture_mot_cache [lindex $aller_closure 6]
}
set initrep [file join $Home textes $repertoire]


#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
#.text configure -state disabled
bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"

if {$auto == 1} {
set aide $tabaide(closure)
set longchamp $tablongchamp(closure)
set listevariable $tablistevariable(closure)
set startdirect $tabstartdirect(closure)
set lecture_mot $tablecture_mot(closure)
set lecture_mot_cache $tablecture_mot_cache(closure)

}
wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 0] 1]"
label .menu.titre -text "[lindex [lindex $listexo 0] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1


proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide iwish filuser lecture_mot lecture_mot_cache
set plist [parse $t]
set compte [comptemots $plist]

#on choisit le premier mot dans un rang al�atoire de 1 � 6
###set indexpremiermot [expr int(rand()*5) + 1]
#on d�termine tout les combien environ on va cacher un mot
####set frequence [expr int($compte/$nbmotscaches)]
#on transforme la liste principale ( de phrases en une liste de mots)
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }

set nbmotscaches [expr int($compte/5)]
    if {$nbmotscaches >= 10} {
    set nbmotscaches 10
    }

if {$nbmotscaches == 0} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }
set frequence [expr int($compte/$nbmotscaches)]
set indexpremiermot [expr int(rand()*5) + 1]


#on r�cup�re le premier mot � masquer
lappend listemotscaches [lindex $tmp $indexpremiermot]
#on r�cup�re les autres mots � masquer, dont la longueur doit �tre sup � 3, sinon on cherche un autre mot en revenant en arri�re
    for {set i 1} {$i < $nbmotscaches} {incr i 1} {
    set mot [lindex $tmp [expr $indexpremiermot + $frequence*$i]]
    set index [expr $indexpremiermot + $frequence*$i]
        while {[string length $mot] <=3 && $index > [expr $indexpremiermot + $frequence*($i -1)]} {
        set index [expr $index -1]
        if {$index <= 0 } {break}
        set mot [lindex $tmp $index]
        }
if {$mot != " " && $mot != [lindex $listemotscaches end]} {
    lappend listemotscaches $mot
    }
}


set nbmotscaches [llength $listemotscaches]

set longmot 0
        foreach mot $listemotscaches {
            if {$longmot < [string length $mot]} {
            set longmot  [string length $mot]
            }
        }
#on recherche les mots cach�s dans le texte, on les supprime et on les remplace par une zone de saisie
    set re1 {\m}
    set re2 {\M} 
    set cur 0.0

    for {set i 0} {$i < $nbmotscaches} {incr i 1} {
    set cur [$t search -regexp $re1[lindex $listemotscaches $i]$re2 $cur end]
    $t delete $cur "$cur + [string length [lindex $listemotscaches $i]] char"
    if {$longchamp == 0} {
    entry $t.ent$i -font $sysFont(l) -width [expr [string length [lindex $listemotscaches $i]] +2] -bg yellow -state disabled
    } else {
    entry $t.ent$i -font $sysFont(l) -width [expr $longmot + 2] -bg yellow -state disabled
    }
    $t window create $cur -window $t.ent$i
    bind $t.ent$i <Return> "verif $i $t"
    bind $t.ent$i <KeyPress> "tux_continue"
    if {$lecture_mot_cache == "1" } {
bind $t.ent$i <1> "speaktexte [lindex $listemotscaches $i]"
bind $t.ent$i <Any-Enter> "$t.ent$i configure -cursor target"
bind $t.ent$i <Any-Leave> "$t.ent$i configure -cursor left_ptr"
}
    if {$lecture_mot == "0" } {
catch {destroy .menu.bb1} 
}


    set listessai($i) 0
    }
$t.ent0 configure -state normal
focus $t.ent0


set listeaide $listemotscaches
    if {$aide== 1} {
    afficheaide 0 $t
     }
}

proc pmanuel {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches couleur aide longchamp longmot listeaide iwish lecture_mot lecture_mot_cache
set nbmotscaches 0
set listemots {}
set listemotscaches {}

# Construction de la liste des mots � cacher, � partir des tags
set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    incr nbmotscaches
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    lappend listemotscaches $str        
    }

 set longmot 0
        foreach mot $listemotscaches {
            if {$longmot < [string length $mot]} {
            set longmot  [string length $mot]
            }
        }

#Si aucun mot n'a �t� masqu�, on repasse en mode auto.
    if {$nbmotscaches == 0} {
    pauto $t
    return
    }
#On marque le texte pour substituer les mots � cacher par des champs de texte
    for {set i 0} {$i < [llength $liste]} {incr i 1} {
    $t mark set cur$i [lindex $liste $i]
    }
# On op�re la substitution
    for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {           
    $t delete cur[expr $i*2] cur[expr ($i*2) +1]
    if {$longchamp == 0} {
    entry $t.ent$i -font $sysFont(l) -width [expr [string length [lindex $listemotscaches $i]] +2] -bg yellow -state disabled
    } else {
    entry $t.ent$i -font $sysFont(l) -width [expr $longmot + 2] -bg yellow -state disabled
    }
    
    $t window create cur[expr $i*2] -window $t.ent$i
    bind $t.ent$i <Return> "verif $i $t"
    bind $t.ent$i <KeyPress> "tux_continue"
    if {$lecture_mot_cache == "1" } {
bind $t.ent$i <1> "speaktexte [lindex $listemotscaches $i]"
bind $t.ent$i <Any-Enter> "$t.ent$i configure -cursor target"
bind $t.ent$i <Any-Leave> "$t.ent$i configure -cursor left_ptr"
}
    if {$lecture_mot == "0" } {
catch {destroy .menu.bb1} 
}

    if {$aide == 1} {
    #bind $t.ent$i <ButtonRelease-1> "afficheaide $i $t"
    } else {
    #bind $t.ent$i <ButtonRelease-1> "catch {destroy .w1}"
    }
    set listessai($i) 0
    }
$t.ent0 configure -state normal
focus $t.ent0
set listeaide $listemotscaches
    if {$aide== 1} {
    afficheaide 0 $t
     }
}

proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot listexo iwish aide user startdirect
set nbmotscaches 0
#########################################################################"
#catch {destroy .menu.b1}
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
 
#pack .menu.b1 -side right 
catch {destroy .menu.titre}
label .menu.titre -text [lindex [lindex $listexo 0] 2] -justify center
pack .menu.titre -side left -fill both -expand 1

if {$aide == 1} {
set what [mc {Ecris ou clique sur le mot convenable pour completer le texte.}]
} else {
set what [mc {Ecris le mot convenable pour completer le texte.}]
}
if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what

#bind .menu  <Destroy> "fin"
$t configure -state normal

# S�lection du mode auto ou manuel pour la g�n�ration de l'exercice
    if {$auto==0} {
    pauto $t
    } else {
    pmanuel $t
    }
    $t configure -state disabled -selectbackground white -selectforeground black
bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""

}

proc verif {i t} {
global sysFont listemotscaches listessai nbreu essais aide listeaide listevariable disabledfore disabledback
incr essais
    if {[lindex $listemotscaches $i] == [$t.ent$i get]} {
    incr nbreu
    if {$listessai($i) == 0} {tux_reussi}
    if {$listessai($i) >= 1}  {tux_continue_bien}
    bind $t.ent$i <Return> {}
    catch {destroy .w1}
    if {$listevariable == 1} {
    set listeaide [lreplace $listeaide [lsearch $listeaide [$t.ent$i get]] [lsearch $listeaide [$t.ent$i get]]]
    }
    $t.ent$i configure -state disabled -$disabledfore blue
        if {[testefin $nbreu [llength $listemotscaches] $essais] == 0} {
        $t.ent[expr $i +1] configure -state normal
        focus $t.ent[expr $i +1]
       if {$aide== 1}  {
           afficheaide [expr $i +1]  $t
           }
        }
    } else {


    incr listessai($i)
	if {$listessai($i) == 1} {tux_echoue1} else {tux_echoue2}
    #affichecouleur $i $t
    $t.ent$i delete 0 end
# Affichage de l'aide
        if {$listessai($i) >= [expr $aide -1] } {

        afficheaide $i $t
        return
        }
    }
affichecouleur $i $t
}

proc testefin {nbreu total essais} {
    global sysFont categorie user nbmotscaches
    if {$nbreu >= $total} {
    catch {destroy .menu.titre}
    catch {destroy .menu.lab}
    set str0 [mc {Exercice termine. }]
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $total]
    set str1 [mc {Exercice de closure}]
    label .menu.lab -text "$str0 - $str2"
    #enregistreval $str1 $categorie $str2 $user
    pack .menu.lab
    bell
set score [expr ($nbreu*100)/($nbmotscaches +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}
    return 1
	
    }
return 0
}


proc afficheaide {ind t} {
global sysFont listeaide listemotscaches listeaide alear xcol ycol
catch {
set xcol [winfo x .w1]
set ycol [winfo y .w1]
destroy .w1
}
toplevel .w1
.w1 configure -width 300 -height 200
#wm geometry .w1 +$xcol+$ycol
wm geometry .w1 -0-0

wm transient .w1 .
text .w1.text1 -yscrollcommand ".w1.scrolly set" -width 20 -height 10 -setgrid true -wrap word -background white -font $sysFont(l) -selectbackground white -selectforeground black
scrollbar .w1.scrolly -command ".w1.text1  yview"
pack .w1.scrolly -side right -fill y
pack .w1.text1 -expand yes -fill both
set tmpliste {}
wm title .w1 [mc {Clique sur une reponse}]
    foreach tmp $listeaide {
        if {[lsearch $tmpliste $tmp]==-1} {
        lappend tmpliste $tmp
        }
    }
set lntmpliste [llength $tmpliste]
    for {set i 0} {$i < $lntmpliste} {incr i 1} {
    set tab($i) [lindex $tmpliste $i]
    set alear($i) $i
    }

  for {set i 0} {$i < [expr $lntmpliste*2]} {incr i 1} {
  set t1 [expr int(rand()*$lntmpliste)]
  set t2 [expr int(rand()*$lntmpliste)]
  set temp $alear($t1)
  set alear($t1) $alear($t2)
  set alear($t2) $temp
  }

    for {set i 0} {$i < $lntmpliste} {incr i 1} {
    label .w1.text1.lab$i -text [lindex $tmpliste $alear($i)] -background yellow -font $sysFont(l)

    .w1.text1 window create current -window .w1.text1.lab$i
    .w1.text1 insert current \040\040\040\040
    bind .w1.text1.lab$i <1> "verifaide \173$tab($alear($i))\175 $i $ind $t \173[lindex $listemotscaches $ind]\175"
    }
    bind .w1 <Destroy> "affichecouleur $ind $t"
    if {[$t.ent$ind cget -state] != "disabled"} {
    $t.ent$ind configure -background white
    }
}

proc affichecouleur {ind t} {
global sysFont listessai xcol ycol disabledfore disabledback
    catch {
    set xcol [winfo x .w1]
    set ycol [winfo y .w1]
    }

switch $listessai($ind) {
    0 { $t.ent$ind configure -$disabledback yellow -bg yellow}
    1 { $t.ent$ind configure -$disabledback green -bg green}
    default { $t.ent$ind configure -$disabledback red -bg red}
    }
}

proc verifaide {rep i ind t mot} {
    global sysFont essais nbreu listemotscaches aide listessai listeaide alear xcol ycol listevariable disabledfore disabledback

    incr essais
    if {$rep == $mot} {
    if {$listessai($ind) == 0} {tux_reussi}
    if {$listessai($ind) >= 1}  {tux_continue_bien}
    

    incr nbreu
    bind $t.ent$ind <Return> {}
    if {$listevariable == 1} {
    set indice [lsearch $listeaide $rep]
    #set listeaide [lreplace $listeaide [expr $alear($i)] [expr $alear($i)]]
    set listeaide [lreplace $listeaide $indice $indice]
    }
    $t.ent$ind delete 0 end
    $t.ent$ind insert end $mot
    $t.ent$ind configure -state disabled -$disabledfore blue
        if {[testefin $nbreu [llength $listemotscaches] $essais] == 0} {
        $t.ent[expr $ind +1] configure -state normal
         focus $t.ent[expr $ind +1] 
            if {$aide== 1}  {
           afficheaide [expr $ind +1]  $t
           return
           }
        }
    catch {destroy .w1}
    } else {
    .w1.text1.lab$i configure -bg red
    incr listessai($ind)
    if {$listessai($ind) == 1} {tux_echoue1} else {tux_echoue2}

    affichecouleur $ind $t
    }
}



proc fin {} {
global sysFont categorie user essais  nbmotscaches nbreu listexo iwish filuser aide startdirect repertoire lecture_mot lecture_mot_cache
variable repertconf
set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu $nbmotscaches]
#set score [expr ($nbreu*100)/$essais]
set score [expr ($nbreu*100)/($nbmotscaches +($essais- $nbreu))]

switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}

switch $lecture_mot {
1 { set lectmotconf [mc {Le texte peut �tre entendu.}]}
0 { set lectmotconf [mc {Le texte ne peut pas �tre entendu.}]}
}

switch $lecture_mot_cache {
1 { set lectmotcacheconf [mc {Les mots peuvent �tre entendus.}]}
0 { set lectmotcacheconf [mc {Les mots ne peuvent pas �tre entendus.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Aide : $aideconf"
set exoconf "$exoconf  Son : $lectmotconf $lectmotcacheconf"


enregistreval [mc {Exercice de closure}]\040[lindex [lindex $listexo 0] 1] \173$categorie\175 $str2 $score $repertconf 0 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}


#focus .text
if {$startdirect == 0 } {
main .text
}

proc boucle {} {
global  categorie startdirect essais nbreu listexo auto listemotscaches
fin
catch {destroy .w1}
set essais 0
set nbreu 0
set listexo ""
set listemotscaches ""
set categorie "Au choix"
.text configure -state normal
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
.menu.c.b1 configure -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
#-text [mc {Commencer}] -command "main .text"
catch {destroy .menu.suiv}
if {$startdirect == 0 } {
main .text
}
}
