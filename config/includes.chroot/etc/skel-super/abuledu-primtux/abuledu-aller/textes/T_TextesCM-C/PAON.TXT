Il va s�rement se marier aujourd'hui.
Ce devait �tre pour hier. En habit de gala, il �tait pr�t. Il n'attendait que sa fianc�e. Elle n'est pas venue. Elle ne peut tarder. Glorieux, il se prom�ne avec une allure de prince indien et porte sur lui les riches pr�sents d'usage. L'amour avive l'�clat de ses couleurs et son aigrette tremble comme une lyre. La fianc�e n'arrive pas. Il monte au haut du toit et regarde du c�t� du soleil. Il jette son cri diabolique : L�on ! L�on !
C'est ainsi qu'il appelle sa fianc�e. Il ne voit rien venir et personne ne r�pond. Les volailles habitu�es ne l�vent m�me point la t�te. Elles sont lasses de l'admirer. Il redescend dans la cour, si s�r d'�tre beau qu'il est incapable de rancune. Son mariage sera pour demain.
Et, ne sachant que faire du reste de la journ�e, il se dirige vers le perron. Il gravit les marches, comme des marches de temple, d'un pas officiel. Il rel�ve sa robe � queue toute lourde des yeux qui n'ont pu se d�tacher d'elle.
Il r�p�te encore une fois la c�r�monie.


						Jules Renard 