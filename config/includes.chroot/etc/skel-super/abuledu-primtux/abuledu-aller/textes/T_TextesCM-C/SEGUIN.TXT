" Ecoutez, monsieur Seguin, je me languis chez vous, laissez-moi aller dans la montagne.
-Ah ! mon Dieu !... Elle aussi ! " cria M. Seguin stup�fait, et du coup il laissa tomber son �cuelle; puis, s'asseyant dans l'herbe � c�t� de sa ch�vre:
-Comment, Blanquette, tu veux me quitter ! "
Et Blanquette r�pondit:
-Oui, monsieur Seguin".
-Est-ce que l'herbe te manque ici ?
-Oh !non ! monsieur Seguin.
-Tu es peut-�tre attach�e de trop court, veux-tu que j'allonge la corde ?
-Ce n'est pas la peine, monsieur Seguin.
-Alors, qu'est-ce qu'il te faut? qu'est-ce que tu veux?
-Je veux aller dans la montagne, monsieur Seguin.
-Mais, malheureuse, tu ne sais pas qu'il y a le loup dans la montagne... Que feras-tu quand il viendra ?...
-Je lui donnerai des coups de corne, monsieur Seguin.
-Le loup se moque bien de tes cornes. Il m'a mang� des biques autrement encorn�es que toi... Tu sais bien, la pauvre vieille Renaude qui �tait ici l'an dernier ? Une ma�tresse ch�vre, forte et m�chante comme un bouc. Elle s'est battue avec le loup toute la nuit... puis, le matin, le loup l'a mang�e.
- P�ca�re ! Pauvre Renaude !... �a ne fait rien, monsieur Seguin, laissez-moi aller dans la montagne.
-Bont� divine !... dit M. Seguin; mais qu'est-ce qu'on leur fait donc � mes ch�vres ? Encore une que le loup va me manger... Eh bien, non... je te sauverai malgr� toi, coquine ! et de peur que tu ne rompes ta corde, je vais t'enfermer dans l'�table, et tu y resteras toujours."


					Alphonse Daudet