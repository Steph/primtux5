Bagheera. 

Bagheera, la panth�re noire, chassait � la tomb�e de la nuit. Soudain, un curieux miaulement l'attira au bord de la rivi�re. Ces cris-l�, Bagheera ne les avait jamais entendus. 
Quelle surprise ! Ils sortaient d'une vieille barque abandonn�e sur le bord de la rivi�re. Doucement, Bagheera s'approcha. 
Dans la barque, il y avait un panier o� un b�b� brun s'agitait en pleurant. Un petit d'homme abandonn� dans la jungle ! D'une patte d�licate, elle ber�a le b�b� qui, aussit�t, cessa de pleurer. 
Bagheera sentit son coeur fondre de tendresse. Elle ne pouvait pas abandonner l'enfant ! Sans elle, il mourrait bient�t de faim ou de froid. Que faire ?. 

