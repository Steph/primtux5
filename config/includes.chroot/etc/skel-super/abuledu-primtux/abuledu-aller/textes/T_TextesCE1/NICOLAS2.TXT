LES CHAUSSURES NEUVES (2).

- Garde les chaussures neuves jusqu'� la maison, dit Maman, qui emporte les vieux "tennis" dans un sac. 
Et les voil� sortis. 
- Tu es content ? �a va ? 
Justement, �a ne va pas du tout. Nicolas boite. Il a un pied tout raide et tout serr� ; celui qu'il a eu tant de peine � chausser tout seul. Il essaie de tirer sur sa chaussette, �a n'arrange rien. 
- Retournons au magasin, dit Maman, on va voir... 
Justement la vendeuse sortait, une chaussure � la main ! 
Nicolas s'�tait tromp�... Il y avait tant de chaussures d�ball�es pr�s de lui... 
La vendeuse enl�ve la chaussure trop �troite. 
- Enlevez les deux, lui dit Nicolas. J'aime mieux mettre mes tennis pour rentrer ! 


