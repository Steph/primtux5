Le grand sac d'or (1).
La poule Gobette arriva tout essouffl�e au jardin.
- Antoine, dit-elle au jardinier, il y a un monsieur tr�s important qui vient au village. Il faut que tout le monde lui fasse un cadeau, et il para�t qu'il donnera un grand sac d'or � celui qui apportera le cadeau le plus beau.
- Vraiment ? r�pondit Antoine distraitement, car il �tait tr�s occup�.
Le vieux noisetier �tait gravement malade et le jardinier passait ses jours et ses nuits � essayer de le sauver.