La vieille maison (6).
Le petit gar�on entendit ses parents se dire : 
� Le vieillard d 'en face a de bien grandes richesses ; mais c'est affreux comme il vit isol� de tout le monde. � Le dimanche d'apr�s, l'enfant enveloppa quelque chose dans un papier, sortit dans la rue et accostant le vieux domestique qui faisait les commissions, il lui dit : 
� Ecoute ! Veux-tu me faire un plaisir et donner cela de ma part � ton ma�tre ? J'ai deux soldats de plomb ; en voil� un ; je le lui envoie pour qu'il ait un peu de soci�t� ; je sais qu'il vit tellement isol� de tout le monde, que c'est lamentable. � Le vieux domestique sourit, prit le papier et porta le soldat de plomb � son ma�tre.
Andersen.