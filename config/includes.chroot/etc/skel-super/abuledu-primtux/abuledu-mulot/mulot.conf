# $id$
# mulot.tcl configuration file

#*************************************************************************
#  Copyright (C) 2002 André Connes <andre.connes@wanadoo.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : mulot.tcl
#  Author  : André Connes <andre.connes@wanadoo.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: mulot.conf,v 1.24 2007/01/18 08:51:11 abuledu_andre Exp $
#  @author     André Connes
#  @modifier
#  @project    Le terrier
#  @copyright  André Connes
#
#***********************************************************************

set glob(version) "8.3"
set glob(tcl_version) $tcl_version

set glob(bgcolor) #cf9e28
set glob(rectcolor) "blue"
set glob(width) 602 ;#800
set glob(height) 602 ;#600
set glob(attente) 5  ;#temps d'attente entre deux sessions en seconde(s)
set glob(nvignettes) 4;# pour glisser/déposer = nb lignes = nb colonnes
set glob(boucle) 0    ;# nb d'images traitées
set glob(bouclemax) 5 ;# nb max d'images à traiter (sentinelle)
set glob(nr_piste) 10 ;# nb de lignes de la piste
set glob(nc_piste) 15 ;# nb de colonnes de la piste
set glob(org) 3       ;#translation de l'origine des coordonnées pour ajustement
set glob(ordre) 1     ;# presentation aleatoire (0) ou ordonnee (1) des images

set glob(passwd) "terrieradmin"

# ##############################
# ne pas modifier ci-dessous svp
# ##############################

# gestion des traces-élèves suivant l'OS
#   user=répertoire+identifiant-élève
if { $tcl_platform(platform) == "unix" } {
  set glob(platform) unix
  set glob(trace_dir) [file join $env(HOME) leterrier mulot log]
  set glob(trace_user) $glob(trace_dir)/$tcl_platform(user)
  set glob(home) [file join $env(HOME)]
  set glob(home_mulot) [file join $env(HOME) leterrier mulot]
  set glob(home_reglages) [file join $glob(home_mulot) reglages]
  set glob(home_msgs) [file join $glob(home_mulot) msgs]
  set glob(progaide) runbrowser
  set glob(wish) wish
  package require Img
} elseif { $tcl_platform(platform) == "windows" } {
  set glob(platform) windows
  set glob(home) [file dir $argv0]
  set glob(home_mulot) [file dir $argv0] ;# cad .
  set glob(trace_dir) [file join [file dir $argv0] mulot log]
  set glob(trace_user) "$glob(trace_dir)/eleve" ;#forcer le nom (cf mulot.tcl)
  set glob(home_reglages) [file join $glob(home_mulot) reglages]
  set glob(home_msgs) [file join $glob(home_mulot) msgs]
  set a ""
  catch {set a $env(USERNAME) }
  if { $a != "" } {
  # répertoire pour tous les utilisateurs
  #     catch { set glob(home_mulot) [file join $env(ALLUSERSPROFILE) leterrier mulot] }
  # ou répertoire pour l'utilisateur loggé
        catch { set glob(home_mulot) [file join $env(USERPROFILE) leterrier mulot] }
        set glob(trace_dir) [file join $glob(home_mulot) log]
        set glob(trace_user) $glob(trace_dir)/$env(USERNAME)
        }
  set glob(home) $glob(home_mulot)
  set glob(home_msgs) [file join $glob(home_mulot) msgs]
  set glob(home_reglages) [file join $glob(home_mulot) reglages]
  set glob(progaide) shellexec.exe
  set glob(wish) wish
  package require Img
} else {
  set glob(platform) undef
  set glob(trace_dir) undef"
  set glob(trace_user) undef
  set glob(home) undef
  set glob(home_mulot) undef
  set glob(progaide) undef
}

set glob(autorise) [file writable [file join [pwd] images]]
