var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.addiclic = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
var aNombres, conteneur, etiquetteCalcul, carreClique, soluce, carreSoluce,intId, totalTentative;

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = {
	carre : "addiclic/images/carre.png",
	illustration : "addiclic/images/illustration.png",
	txt:"addiclic/textes/addiclic_fr.json"
};

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
		totalQuestion:10,
		totalEssai:1,
		temps_question:0,
		temps_exo:0,
		typeAddition:5,
		tempsExpoCorrection:30
	};
	$.extend(exo.options,optionsParDefaut,oOptions);
};


// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
	var i,j,nbreA,nbreB,indiceTemp,alea1,alea2;
	var aTemp;
	if (exo.options.typeAddition == 1) {
		//resultats <= 10
		aNombres = [];
		alea1 = 0 + Math.floor(Math.random()*(1-0+1));
		alea2 = 1 + Math.floor(Math.random()*(9-1+1));
		aNombres.push(new Array(alea1,alea2));
		for(i=2;i<=5;i++) {
			for(j=i;j<=10-i;j++) {
				aNombres.push(new Array(i,j));
			}
		}
	} else if (exo.options.typeAddition == 2) {
		//resultats >= 10
		aNombres = [];
		alea1 = 10 + Math.floor(Math.random()*(19-10+1));
		aNombres.push(new Array(0,alea1));
		alea2 = 10 + Math.floor(Math.random()*(19-10+1));
		aNombres.push(new Array(1,alea2));
		for(i=2;i<=10;i++) {
			for(j=10-i;j<=20-i;j++) {
				aNombres.push(new Array(i,j));
			}
		}	
	} else if (exo.options.typeAddition == 3) {
		//Tous resultats
		aNombres = [];
		alea1 = 0 + Math.floor(Math.random()*(20-0+1));
		aNombres.push(new Array(0,alea1));
		alea2 = 1 + Math.floor(Math.random()*(19-1+1));
		aNombres.push(new Array(1,alea2));
		for(i=2;i<=10;i++) {
			for(j=i;j<=20-i;j++) {
				aNombres.push(new Array(i,j));
			}
		}
	} else if (exo.options.typeAddition == 4) {
		//Chevauchement de dizaine
		aNombres = [];
		aNombres.push(new Array(1,9));
		for(i=2;i<=5;i++) {
			for(j=10-i;j<10;j++) {
				aNombres.push(new Array(i,j));
			}
		}
		for(i=6;i<10;i++) {
			for(j=i;j<10;j++) {
				aNombres.push(new Array(i,j));
			}
		}
	} else if (exo.options.typeAddition == 5) {
		//2 nbres à 1 chiffre
		aNombres = [];
		alea1 = 1 + Math.floor(Math.random()*(9-1+1));
		aNombres.push(new Array(0,alea1));
		alea2 = 1 + Math.floor(Math.random()*(9-1+1));
		aNombres.push(new Array(1,alea2));
		for(i=2;i<10;i++) {
			for(j=i;j<10;j++) {
				aNombres.push(new Array(i,j));
			}
		}
		console.log(aNombres);
	} else if (exo.options.typeAddition == 6) {
		//1 nbre à 1 chiffre + 1 nbre à deux chiffres
		aNombres = [];
		alea1 = 0 + Math.floor(Math.random()*(1-0+1));
		aNombres.push(new Array(10,alea1));
		alea2 = 1 + Math.floor(Math.random()*(9-1+1));
		aNombres.push(new Array(11,alea2));
		for(i=2;i<10;i++) {
			for(j=i;j<10-i;j++) {
				if (j%2==1) {
					aNombres.push(new Array(i+10,j));
				} else {
					aNombres.push(new Array(i,j+10));
				}
			}
		}
	}
	//aNombres.sort(function(){return Math.floor(Math.random()*3)-1;});
	util.shuffleArray(aNombres);
	for(var i=0;i<10;i++){
		console.log(aNombres[i]);
	}
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
	//alert(aNombres[q][0]);
    soluce = aNombres[q][0] + aNombres[q][1];
    exo.btnValider.hide();
    exo.keyboard.config({
		numeric : "disabled",
		arrow : "disabled",
		large : "disabled"
    });
    //
    conteneur = disp.createEmptySprite();
    exo.blocAnimation.append(conteneur);
    conteneur.css({
        left:350,
        top:90
    });
    //
	var texte = "";
	if (exo.indiceQuestion % 2 == 1) {
		texte =aNombres[q][0]+" + "+aNombres[q][1]+" = ? ";
	} else {
		texte =aNombres[q][1]+" + "+aNombres[q][0]+" = ? ";
	}
	//alert(texte);
    etiquetteCalcul = disp.createTextLabel(texte);
    etiquetteCalcul.css({
        fontSize:32,
        fontWeight:'bold',
        border:"1px solid #ccc",
		opacity:0,
        padding:20
    });
    exo.blocAnimation.append(etiquetteCalcul);
    etiquetteCalcul.position({
        my:'left center',
        at:'left+100 center',
        of:exo.blocAnimation
    });
	
	for ( var i = 0 ; i < 20 ; i++) {
		var carre = disp.createImageSprite(exo,"carre");
		conteneur.append(carre);
		var posX = (i % 5) * (carre.width() + 5);
		var posY = Math.floor(i / 5) * (carre.height() + 5);
		carre.css({
			left:posX,
			top:posY
		});
		var etiquette = disp.createTextLabel(i+1);
		etiquette.css({
			fontSize:30,
			fontWeight:'bold',
			color:'#fff'
		});
		carre.append(etiquette);
		etiquette.position({
			my:'center center',
			at:'center center',
			of:carre
		});
		if (soluce == i + 1)
			carreSoluce = carre;
		carre.data("valeur",i+1);
		carre.on("mousedown.clc touchstart.clc",gestionClickCarre);
		carre.hover(
			function(e){
				anim.glow($(e.delegateTarget),"gold",5,5);
				//return false;
			},
			function(e){
				anim.glow($(e.delegateTarget),'none');
				//return false;
			}
		);

	}
	etiquetteCalcul.transition({opacity:1},1000,'linear');
    
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
	
    if(repEleve == soluce) {
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    conteneur.append(carreSoluce);
    anim.glow(carreSoluce,"red",3,3);
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.option1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_exo",
        texte:exo.txt.option2
    });
    exo.blocParametre.append(controle);
    //
    controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_question",
        texte:exo.txt.option3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeAddition",
        texte:exo.txt.option4,
        aValeur:[1,2,3,4,5,6],
        aLabel:exo.txt.valeurs4
    });
    exo.blocParametre.append(controle);
	//
    controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExpoCorrection",
        texte:exo.txt.option5
    });
    exo.blocParametre.append(controle);
};
//
function gestionClickCarre(e) {
	e.preventDefault();//pour éviter la selection sur tablette
	//
	conteneur.find("div").off();
	carreClique = $(e.delegateTarget);
	repEleve = carreClique.data("valeur");
	exo.poursuivreExercice(2000,exo.options.tempsExpoCorrection*100);
	exo.blocInfo.position({
		my:"center top",
		at:"center bottom+20",
		of:etiquetteCalcul
	});
}
/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));