p1=Les allumettes.
p2=Il faut mettre de côté les allumettes qui ne seront pas utilisées pour faire des figures.
p3=Il y a $val allumettes. On veut faire le plus grand nombre possible de figures comportant $val allumettes. Place celles qui ne seront pas utilisées dans la boîte.