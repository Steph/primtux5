var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.bocal = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var poisson, champReponse, k;
var aNombre, aBulle, aEtiquette;

// Référencer les ressources de l'exercice (images, sons)
// {
//      nom-de-la-ressource : chemin-du-fichier
// }

exo.oRessources = {
	txt:"bocal/textes/bocal_fr.json",
	fond:"bocal/images/fond.jpg",
	poissonDroite:"bocal/images/poisson-d.png",
	//poissonGauche:"bocal/images/poisson-g.png",
	bulle:"bocal/images/bulle_02.png",
    illustration:"bocal/images/illustration.jpg",
};

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
		totalQuestion:5,
		totalEssai:1,
		tempsExo:0,
		temps_question:0,
		nBulleMax:3,
		plagePremierNombre:"2-9",
		plageAutreNombre:"2-9",
		choixVitesse:2
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
	
	//les nombres de la premiere bulle
	var premierNombre = [];
	var plagePremierNombre = util.getArrayNombre(exo.options.plagePremierNombre);
	console.log(plagePremierNombre);
	util.shuffleArray(plagePremierNombre);
	var dif=plagePremierNombre.length - exo.options.totalQuestion;
	var i,j;
	if(dif>0) {
		for ( i=0; i<exo.options.totalQuestion; i++ )	{
			premierNombre.push(plagePremierNombre[i]);
		}
	} else {
		for ( i=0; i<exo.options.totalQuestion; i++ )	{
			premierNombre.push(plagePremierNombre[i%plagePremierNombre.length]);
		}
	}
	//Pour le rallye La deuxième bulle est un multiplde de 10
	// var deuxiemeNombre = [];
	// for ( i=0; i<exo.options.totalQuestion; i++){
	// var nb1 = (3 + Math.floor(Math.random()*(9 - 3 + 1)))*10;
	// deuxiemeNombre.push(nb1);
	// }
	
	//les nombres des autres bulles
	var autreNombre = [], aTemp;
	for ( i=0; i<exo.options.totalQuestion; i++){
		var nBulleRestant = exo.options.nBulleMax - 1;
		var plageAutreNombre = util.getArrayNombre(exo.options.plageAutreNombre);
		dif = plageAutreNombre.length - nBulleRestant ;
		util.shuffleArray(plageAutreNombre);
		aTemp = [];
		if ( dif > 0 ) {
			for( j=0; j < nBulleRestant ; j++ ) {
				aTemp.push(plageAutreNombre[j]);
			}
		} else {
			for( j=0; j < nBulleRestant ; j++ ) {
				aTemp.push(plageAutreNombre[j%nBulleRestant]);
			}
		}
		autreNombre.push(aTemp);
	}
	
	aNombre=[];
	//le tableau qui contient tous les nombres (pemieres bulles + autres bulles)
	for( i=0; i<exo.options.totalQuestion; i++ ) {
		aTemp = [];
		aTemp.push(premierNombre[i]);
		//aTemp.push(deuxiemeNombre[i]);
		for( j = 0 ; j < autreNombre[i].length ; j++ ){
			aTemp.push(autreNombre[i][j]);
		}
		var bonneReponse = 0;
		for ( k=0; k<aTemp.length; k++ ) {
			bonneReponse += aTemp[k];
		}
		aTemp.push(bonneReponse);
		aNombre.push(aTemp);
	}
	
	/*
	 * Fin de pour le rallye
	 *
	 * */
};

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigneGenerale);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
	aBulle=[];
	exo.keyboard.config({
		numeric:"disabled",
		arrow:"disabled",
		large:"disabled",
	});
	exo.btnValider.hide();
	switch(exo.options.choixVitesse) {
		case 1:
			vitesseBulle = 2000;
			vitessePoisson = 3000;
			break;
		case 2:
			vitesseBulle = 2000;
			vitessePoisson = 2200;
			break;
		case 3:
			vitesseBulle = 1500;
			vitessePoisson = 1400;
			break;
	}
	/*
	 * pour le rallye
	 *
	 * */
	//var vitessePoisson = 1000;
	//var vitesseBulle = 12000;
	// le fond
	var fond = disp.createImageSprite(exo,"fond");
	exo.blocAnimation.append(fond);
	// le poisson
	poisson = disp.createImageSprite(exo,"poissonDroite");
	poisson.css({left:245,top:270});
	poisson.data("placement",0);
	exo.blocAnimation.append(poisson);
	// on bouge le poisson une première fois vers la droite sans attendre;
	poisson.transition({x:"+=165"},vitessePoisson,'linear',function(e){
		cracherBulle(0);		
	});
	// on lance l'animation
	var animPoisson = anim.creerDemon(poisson,bougerPoisson,vitessePoisson+400,(exo.options.nBulleMax-1));
	animPoisson.start();

	
	function bougerPoisson(etape){
		var rotation,deplacement;
		//Retournement et deplacement vers la gauche
		if (etape%2==1) {
			rotation = '180deg';
			deplacement = "-=165";
		}
		//retournement et deplacement vers la droite
		else if (etape%2 === 0) {
			rotation = '0deg';
			deplacement = "+=165";
		}
		this.find("img").transition({ perspective: '200px', rotateY:rotation });
		this.transition({x:deplacement},vitessePoisson,'linear',function(e){
			cracherBulle(etape);
		});
	}
	
	function cracherBulle(n){
		var bulle,etiquette,valeur,left,deplacement,taille;
		valeur = aNombre[q][n];
		bulle = disp.createImageSprite(exo,"bulle");
		exo.blocAnimation.append(bulle);
		taille = valeur < 100 ? 32 : 26;
		etiquette = disp.createTextLabel(util.numToStr(valeur)).css({fontSize:taille});
		bulle.append(etiquette);
		etiquette.position({my:"center",at:"center",of:bulle});
		if ( n%2 === 0 ) {
			left = 490;
			deplacement = "+=40px";
		} else {
			left = 195;
			deplacement = "-=40px";
		}
		bulle.css({left:left, top:270});
		aBulle[n]=bulle;
		bulle
			.transition({x:deplacement})
			.transition({y:"-=320px"},vitesseBulle,'linear',function(){
				if ( n == exo.options.nBulleMax-1 ) {
					afficherTxtField();
				}
			});
	}
	
	function afficherTxtField(e) {
		champReponse = disp.createTextField(exo,5);
		exo.blocAnimation.append(champReponse);
		champReponse.position({
			my:"center center",
			at:"center center",
			of:exo.blocAnimation
		});
		champReponse.focus();
		if (exo.tabletSupport){
			exo.keyboard.config({numeric : "enabled", large : "enabled"});
		} else{
			exo.btnValider.show();
		}
		
	}
};


// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var q = exo.indiceQuestion;
	var indiceSoluce = exo.options.nBulleMax;
    if (champReponse.val() === "") {
       return "rien" ;
    } else if( Number(champReponse.val()) == aNombre[q][indiceSoluce] ) {
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
	var indiceSoluce = exo.options.nBulleMax;
	var resultat = aNombre[q][indiceSoluce];
    var correction = disp.createCorrectionLabel(resultat);
	var signe=[];
    exo.blocAnimation.append(correction);
	
    correction.position({
        my:"left center",
        at:"right+20 center",
        of:champReponse,
    });
    var barre = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre);
	
	for (i=0;i<exo.options.nBulleMax;i++){
		var x = (i+1) * (735/(exo.options.nBulleMax+2)) - 25;
		aBulle[i].css({x:0,y:0});
		aBulle[i].css({left:x,top:125});
		var position = "left+"+x+" top+125";
		signe[i] = ((i+1)==exo.options.nBulleMax) ?  disp.createTextLabel("=") : disp.createTextLabel("+");
		x = x + (((i+2) * (735/(exo.options.nBulleMax+2)))-x)/2;
		position = "left+"+x+" top+125";
		exo.blocAnimation.append(signe[i]);
		signe[i].position({my:'center top',at:position,of:exo.blocAnimation}).css({"font-size":32});
	}
	
	var reponse = disp.createTextLabel(util.numToStr(resultat));
	exo.blocAnimation.append(reponse);
	var posX = (exo.options.nBulleMax+1)*(735/(exo.options.nBulleMax+2));
	reponse.css({left:posX,top:125,"font-size":32});
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
//    var conteneur = disp.createOptConteneur();
//    exo.blocParametre.append(conteneur);
	var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"totalQuestion",
        texte:exo.txt.option1
    });
    exo.blocParametre.append(controle);
	controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"nBulleMax",
        texte:exo.txt.option3
    });
    exo.blocParametre.append(controle);

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:128,
        taille:10,
        nom:"plagePremierNombre",
        texte:exo.txt.option4
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:128,
        taille:10,
        nom:"plageAutreNombre",
        texte:exo.txt.option5
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"choixVitesse",
        texte:exo.txt.option6,
        aValeur:[1,2,3],
        aLabel:exo.txt.valeur6
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));