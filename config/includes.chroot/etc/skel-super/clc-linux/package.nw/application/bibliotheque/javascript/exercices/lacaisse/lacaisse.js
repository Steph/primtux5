(function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.lacaisse = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var aPrix=[],aSommeDonnee=[],tab_monnaie_donnee = [],aRenduOptimise=[],soluce, caisse, tapis;
//Tableau de valeur de chaque pièce ou billet en centimes
var valeurMonnaie = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000];
//Tableau stockant les nom des fichiers ressources pour l'affichage
var tab_ressources = ["unCent","deuxCent","cinqCent","dixCent","vingtCent","cinquanteCent","unEuro","deuxEuro","cinqEuro","dixEuro","vingtEuro","cinquanteEuro","centEuro","deuxcentsEuro","cinqcentsEuro"]
var aPrenom =["Nordine","Silvère","Sophie","Nadia","Flora"];

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    illustration : "lacaisse/images/illustration.jpg",
	unCent:"lacaisse/images/1.png",
	deuxCent:"lacaisse/images/2.png",
	cinqCent:"lacaisse/images/5.png",
	dixCent:"lacaisse/images/10.png",
	vingtCent:"lacaisse/images/20.png",
	cinquanteCent:"lacaisse/images/50.png",
	unEuro:"lacaisse/images/100.png",
	deuxEuro:"lacaisse/images/200.png",
	cinqEuro:"lacaisse/images/500.png",
	dixEuro:"lacaisse/images/1000.png",
	vingtEuro:"lacaisse/images/2000.png",
	cinquanteEuro:"lacaisse/images/5000.png",
	centEuro:"lacaisse/images/10000.png",
	deuxcentsEuro:"lacaisse/images/20000.png",
	cinqcentsEuro:"lacaisse/images/50000.png"
}

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
		totalEssai:1,
		tempsExo:0,
		temps_question:0,
		rendu_precision:2,
		rendu_opt:1
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    switch (exo.options.rendu_precision) {
		case 0://somme en euros quelconque
			aSommeDonnee = [20,50,100,200,500];
			for (var i=0;i<5;i++) {
				var min=5,max=aSommeDonnee[i]-5;
				aPrix[i] = min + Math.floor(Math.random()*(max-min+1));
			}
			break;
		case 1://somme en euros et centimes (ex 3€50)
			aSommeDonnee = [5,10,20,50,100];
			for (var i=0;i<5;i++) {
				var min=Math.floor(aSommeDonnee[i]/2),max=aSommeDonnee[i]-2;
				var partieEntiere = min + Math.floor(Math.random()*(max-min+1));
				var partieDecimale =  (1 + Math.floor(Math.random()*(9-1+1)))/10;
				aPrix[i] = partieEntiere+partieDecimale;
			}
			break;
		case 2://somme en euros et centimes (ex 3€24)
			aSommeDonnee = [5,10,20,50,100];
			for (var i=0;i<5;i++) {
				var min=Math.floor(aSommeDonnee[i]/2),max=aSommeDonnee[i]-2;
				var partieEntiere = min + Math.floor(Math.random()*(max-min+1));
				var partieDecimale =  (11 + Math.floor(Math.random()*(99-11+1)))/100;
				aPrix[i] = partieEntiere+partieDecimale;
			}
			break;
		case 3:// somme en euros complément à la dizaine sup
			aSommeDonnee = [10,10,20,20,50];
			for (var i=0;i<5;i++) {
				var min=aSommeDonnee[i]-9,max=aSommeDonnee[i]-1;
				aPrix[i] = min + Math.floor(Math.random()*(max-min+1));
			}
			break;
		case 4://somme en euros complément à une dizaine sup
			aSommeDonnee = [20,20,50,50,50];
			for (var i=0;i<5;i++) {
				var min=1,max=aSommeDonnee[i]-11;
				aPrix[i] = min + Math.floor(Math.random()*(max-min+1));
			}
			break;
		case 5://somme en euros complément à une centaine sup
			aSommeDonnee = [100,100,200,200,500];
			for (var i=0;i<5;i++) {
				var min=50,max=aSommeDonnee[i]-11;
				aPrix[i] = min + Math.floor(Math.random()*(max-min+1));
			}
			break;
	}
}

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("La caisse");
	exo.blocConsigneGenerale.html("Rendre la monnaie en cliquant sur les pièces ou les billets de la caisse.");
	exo.blocIllustration.append(disp.createImageSprite(exo,"illustration"));
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
	exo.keyboard.config({
		numeric : "disabled",
		float : "disabled",
		delete : "disabled",
		arrow : "disabled",
		large:"enabled"
	});
	//
	tab_monnaie_donnee = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	exo.btnValider.css({left:50,top:180});
	exo.blocScore.css({left:20,top:280})
    
	var q = exo.indiceQuestion;
	var prix = formaterPrix(aPrix[q]);
	var sommeDonnee = aSommeDonnee[q];
	soluce = (sommeDonnee*100) - (aPrix[q]*100);
	aRenduOptimise = calculerReponse(aPrix[exo.indiceQuestion],aSommeDonnee[exo.indiceQuestion]);
    
	//la consigne
	var texte = aPrenom[q]+" achète \nun article à "+prix+".\nIl paie avec \nun billet de "+sommeDonnee+" €.\nRends-lui la monnaie";
	var svgConsigne = disp.createSvgContainer(180,160);
	var blocConsigne = svgConsigne.paper.rect(0,0,180,150,5);
	if (exo.options.rendu_opt==0) {
		texte+="\navec le moins de billets\nou pièces possible."
	}
	blocConsigne.attr("fill","90-#ddd-#eee");
	blocConsigne.attr("stroke","#ccc");
    exo.blocAnimation.append(svgConsigne);
    svgConsigne.css({left:20,top:20});
	var etiquette = svgConsigne.paper.text(blocConsigne.getBBox().width/2,blocConsigne.getBBox().height/2,texte);
	etiquette.attr("font-size","16px");
	
    //la caisse
    tabEncaisse=[5,5,5,5,5,5,5,5,5,5,5,5,5,5,5];
    var caisse = disp.createEmptySprite();
    caisse.css({width:"100%",height:130,left:0,top:320,borderTop:"3px solid #ccc",background:"#450062"});
    exo.blocAnimation.append(caisse);
    
    var reduc = 0.7;
	var i, j, k, espece, offsetLeft, offsetTop;
    for( i = 0 ; i< 6 ; i++ ) {
        var j = Math.floor(i/3);
		for (k=0;k<tabEncaisse[i];k++) {
			espece = disp.createImageSprite(exo,tab_ressources[i]);
			offsetLeft = 20+ (i%3*reduc*70)+(reduc*70-espece.width())/2;
			offsetTop = 10 + (j*reduc*80) + k*2;
			espece.css({width:espece.width()*reduc,height:espece.height()*reduc,left:offsetLeft,top:offsetTop,cursor:"pointer"});
			espece.data("position","basse");
			espece.data("indice",i)
			espece.on("mousedown.clc touchstart.clc",gestionClickEspece);
			espece.data("caisseX",offsetLeft);
			espece.data("caisseY",offsetTop);
			caisse.append(espece);	
		}
    }
	
	for( i = 0 ; i< 2 ; i++ ) {
		for (k=0;k<tabEncaisse[i+6];k++) {
			espece = disp.createImageSprite(exo,tab_ressources[i+6]);
			offsetLeft = 180
			offsetTop = 10 + (i%2*reduc*80) + k*2;
			espece.css({width:espece.width()*reduc,height:espece.height()*reduc,left:offsetLeft,top:offsetTop,cursor:"pointer"});
			espece.data("position","basse");
			espece.data("indice",i+6)
			espece.on("mousedown.clc touchstart.clc",gestionClickEspece);
			espece.data("caisseX",offsetLeft);
			espece.data("caisseY",offsetTop);
			caisse.append(espece);	
		}
    }
	
    for( i = 0 ; i< 6 ; i++ ) {
		for (k=0;k<tabEncaisse[i+8];k++) {
			espece = disp.createImageSprite(exo,tab_ressources[i+8]);
			offsetLeft = 250+(i*75)+(75-espece.width())/2;
			offsetTop = 10+k*3;
			espece.css({width:espece.width()*reduc,height:espece.height()*reduc,left:offsetLeft,top:offsetTop,cursor:"pointer"});
			espece.on("mousedown.clc touchstart.clc",gestionClickEspece);
			espece.data("position","basse");
			espece.data("indice",i+8)
			espece.data("caisseX",offsetLeft);
			espece.data("caisseY",offsetTop);
			caisse.append(espece);
		}
    }
	
    //le tapis
	tapis = disp.createEmptySprite();
	tapis.css({width:1,height:131})
	exo.blocAnimation.append(tapis);
	tapis.position({
		my:"center top",
		at:"center+100 top+150",
		of:exo.blocAnimation
	})
    
	//
	var largeurTapis=0;
	var hauteurTapis = 0;
	function gestionClickEspece(e){
		e.preventDefault();
		exo.blocInfo.hide();
		especeClique = $(this);//On récupère l'espèce sur laquelle on a cliqué 
		if (especeClique.data("position") == "basse") {//Si c'est en position basse, on la monte
			especeClique.data("caisseW",especeClique.width());
			especeClique.data("caisseH",especeClique.height());
			var posx=caisse.position().left+especeClique.position().left;
			var posy=caisse.position().top+especeClique.position().top;
			tapis.append(especeClique);//Remet au premier plan la pièce ou le billet
			especeClique.css({left:largeurTapis,top:(131-especeClique.height())/2})
			largeurTapis+=especeClique.width();
			tapis.css({width:largeurTapis});
			/*
			tapis.position({
				my:"center top",
				at:"center+100 top+150",
				of:exo.blocAnimation
			});
			*/
			tapis.css({
				left: 467 - tapis.width()/2,
				top : 150
			})
			especeClique.data("position","haute");
			tab_monnaie_donnee[especeClique.data("indice")]++;//On incrémente le tableau tab_monnaie_donnee (grâce à l'indice stocké);	
		}
		else if (especeClique.data("position") == "haute") {//Si c'est en position haute, on la rabaisse
			caisse.append(especeClique);
			especeClique.css({left:especeClique.data("caisseX"),top:especeClique.data("caisseY")});
			especeClique.data("position","basse");
			tab_monnaie_donnee[especeClique.data("indice")]--;
			largeurTapis=0;
			tapis.children().each(function(index,value){
				$(value).css({left:largeurTapis});
				largeurTapis+=$(value).width();
			})
			tapis.css({width:largeurTapis})
			tapis.position({
				my:"center top",
				at:"center+100 top+150",
				of:exo.blocAnimation
			})
		}
	}
	
	function formaterPrix(nPrix) {
		if (nPrix%1===0) {
			return String(nPrix)+" €";
		} else {
			return nPrix.toFixed(2).split(".").join(" € ");
		}
	}
	
	function calculerReponse (prix,sommeDonnee) {
		var sommeARendre = (sommeDonnee*100) - (prix*100);
		var tabRendu = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		for (var i = 14; i >= 0 ;i-- ) {
			var val = valeurMonnaie[i];
			tabRendu[i] = Math.floor(sommeARendre/val);
			sommeARendre-=tabRendu[i]*val;
		}
		return tabRendu;
	}
}

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
	var repEleve=0;
	var repEleveOptimale=true;
	for (var i = 0 ; i < 15 ; i++ ) {
		repEleve+=tab_monnaie_donnee[i]*valeurMonnaie[i];
	}
	for (var i = 0 ; i < 15 ; i++ ) {
		if (tab_monnaie_donnee[i] != aRenduOptimise[i]) {
			repEleveOptimale=false;
			break;
		}
	}
	console.log("soluce",soluce,"repeleve",repEleve);
	if (repEleve == 0) {
		return "rien";
	}
	else if (soluce == repEleve) {
		//l'option "rendu optimal" n'est pas activée
		if (exo.options.rendu_opt==1) {
			return "juste";
		}
		//l'option "rendu optimal" est activée
		else if ( exo.options.rendu_opt==0 ) {
			if ( repEleveOptimale ) {
				return "juste";
			}else{
				exo.msgFauxQuestionSuivante="Il existe une meilleure solution. Regarde la correction."
				return "faux";
			}
		}
	}
	else{
		return "faux";
	}
    
    
}

// Correction (peut rester vide)

exo.corriger = function() {
	//
	var tapisCorrection = disp.createEmptySprite();
	tapisCorrection.css({
		left:400,
		top:180
	});
	var reduc=0.7;
	var offsetLeft=0;
	for (var i=0;i<aRenduOptimise.length;i++) {
		if (aRenduOptimise[i]>0) {
			for (var j=0;j<aRenduOptimise[i];j++) {
				var espece = disp.createImageSprite(exo,tab_ressources[i]);
				tapisCorrection.append(espece);
				espece.css({width:espece.width()*reduc,height:espece.height()*reduc,left:offsetLeft,top:(131-espece.height()*reduc)/2})
				//espece.css({position:"static",width:espece.width()*0.7,display:"inline-block",verticalAlign:"middle",margin:0});
				offsetLeft += (espece.width()+10);
				//k++;	
			}
			
		}
	}
	tapisCorrection.css({width:offsetLeft});
	
	tapis.transition({y:-140},500,"linear",function(){
		tapis.css({opacity:0.4,height:131});
		exo.blocAnimation.append(tapisCorrection);
		tapisCorrection.position({
			my:"center top",
			at:"center+100 top+180",
			of:exo.blocAnimation
		})
	})
			
	
	
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_question",
        texte:"Temps maximum par question (en secondes)  : "
    });
    exo.blocParametre.append(controle);
	var controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"rendu_opt",
        texte:"Utiliser le moins de billets ou de pièces possible  : ",
		aLabel:["oui","non"],
		aValeur:[0,1]
    });
    exo.blocParametre.append(controle);
	
	var controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"rendu_precision",
        texte:"La somme rendue est : ",
		aLabel:[
			"une somme en Euros quelconque &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
			"une somme en Euros et centimes (ex. 3€50)",
			"une somme en Euros et centimes (ex. 3€24)",
			"une somme en Euros complément à la dizaine supérieure",
			"une somme en Euros complément à une dizaine",
			"une somme en Euros complément à une centaine"
		],
		aValeur:[0,1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))