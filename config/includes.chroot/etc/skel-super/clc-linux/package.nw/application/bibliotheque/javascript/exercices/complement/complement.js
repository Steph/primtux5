var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.complement = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var i,reponse_juste,complementA,alea;
var aNombreDonne = [], aNombre = [];
var cartouche, champReponse;


exo.oRessources = { 
    illustration:"complement/images/illustration.png",
    cartouche:"complement/images/cartouche.png"
};

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
    	temps_question:0,
    	temps_exo:0, 
    	totalEssai:1,
    	totalQuestion:10,
    	nombreDonne:"50-59",
    	typeComplement:0,
    	typeEnonce : [1,2]
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
//Creation des donnees numeriques de l'exo
//Tirage des nombres en fonction des paramètres nombre_a et nombre_b
//
    aNombreDonne = util.getArrayNombre(exo.options.nombreDonne);
    aNombre = [];//On réinitialise aNombre
    //on 
    var dif = exo.options.totalQuestion - aNombreDonne.length;
    //
    if(dif>0) {
	//plus de questions que nombres
        for(i=0;i<dif;i++) {
            aNombreDonne.push(aNombreDonne[i]);
	   }
    } 
    else if(dif < 0) {
	// aNombreDonne.sort(function(){return Math.floor(Math.random()*3)-1});
        util.shuffleArray(aNombreDonne);
        aNombreDonne.splice(0,Math.abs(dif));
    } 
    else if(dif===0) {
    	//nombres donnes = nombre de questions
    	// aNombreDonne.sort(function(){return Math.floor(Math.random()*3)-1});
        util.suffleArray(aNombreDonne);
    }
//
    if(exo.options.typeComplement===0) {
    	//complement a la dizaine superieure
    	for(i=0;i<aNombreDonne.length;i++) {
            if (aNombreDonne[i]%10===0) {
    		  complementA=aNombreDonne[i]+10;
            } else {
    		  complementA=Math.ceil(aNombreDonne[i]/10) * 10;
            }
            reponse_juste = complementA - aNombreDonne[i];
            aNombre.push(new Array(aNombreDonne[i],reponse_juste,complementA));
    	}
    } 
    else if (exo.options.typeComplement==1) {
	//complement a une dizaine superieure
    	for(i=0;i<aNombreDonne.length;i++) {
            alea = 0 + Math.floor(Math.random()*(4-0+1));
            complementA = (Math.ceil(aNombreDonne[i]/10)*10)+(10*alea);
            reponse_juste = complementA - aNombreDonne[i];
            aNombre.push(new Array(aNombreDonne[i],reponse_juste,complementA));
    	}
    } 
    else if (exo.options.typeComplement==2) {
    	//complement a la centaine supérieure
    	for(i=0;i<aNombreDonne.length;i++){
            if (aNombreDonne[i]%100===0) {
    		  complementA=aNombreDonne[i]+100;
            } else {
    		  complementA=Math.ceil(aNombreDonne[i]/100) * 100;
            }
            reponse_juste = complementA - aNombreDonne[i];
            aNombre.push(new Array(aNombreDonne[i],reponse_juste,complementA));
    	}
    } 
    else if (exo.options.typeComplement==3) {
    	//le complement a une centaine superieure
    	for(i=0;i<aNombreDonne.length;i++) {
                alea = 0 + Math.floor(Math.random()*(4-0+1));
                complementA = (Math.ceil(aNombreDonne[i]/100)*100)+(100*alea);
                reponse_juste = complementA - aNombreDonne[i];
                aNombre.push(new Array(aNombreDonne[i],reponse_juste,complementA));
    	}
    } 
    else if(exo.options.typeComplement==4) {
	// le complement est un multiple de 10
    	for(i=0;i<aNombreDonne.length;i++) {
            alea = 2 + Math.floor(Math.random()*(8-2+1));
            complementA = aNombreDonne[i] + alea *10;
            reponse_juste = alea * 10;
            aNombre.push(new Array(aNombreDonne[i],reponse_juste,complementA));
    	}
    }
//
    //aNombre.sort(function(){return Math.floor(Math.random()*3)-1});
    util.shuffleArray(aNombre);
    // for(i=0;i<aNombre.length;i++) {
	   // console.log(aNombre[i]);
    // }

};
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Le complément");
    exo.blocConsigneGenerale.html("Trouve le nombre qui manque.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    var texte;
    var enonce = 0;
    for(i=0;i<exo.options.typeEnonce.length;i++) {
	   enonce+=exo.options.typeEnonce[i];
    }
    if(enonce==1) {
	   texte = aNombre[q][0]+" + ? = "+aNombre[q][2];
    } else if(enonce==2) {
	   texte = aNombre[q][0]+" pour aller à "+aNombre[q][2];
    } else if(enonce==3) {
    	if(q%2===0) {
                texte = aNombre[q][0]+" + ? = "+aNombre[q][2];
    	} else if(q%2==1) {
                texte = aNombre[q][0]+" pour aller à "+aNombre[q][2];
    	}
    } else if(enonce==4) {
	   texte = aNombre[q][2]+" - "+aNombre[q][0]+" = ?";
    } else if(enonce==5) {
    	if(q%2===0) {
                texte = aNombre[q][2]+" - "+aNombre[q][0]+" = ?";
    	} else if(q%2==1) {
                texte = aNombre[q][0]+" + ? = "+aNombre[q][2];
    	}
    } else if(enonce==6) {
    	if(q%2===0) {
            texte = aNombre[q][2]+" - "+aNombre[q][0]+" = ?";
    	} else if(q%2==1) {
            texte = aNombre[q][0]+" pour aller à "+aNombre[q][2];
    	}
    } else if(enonce==7) {
    	if(q%3===0) {
            texte = aNombre[q][0]+" + ? = "+aNombre[q][2];
    	} else if(q%3==1) {
            texte = aNombre[q][2]+" - "+aNombre[q][0]+" = ?";
    	} else if(q%3==2) {
            texte = aNombre[q][0]+" pour aller à "+aNombre[q][2];
    	}
    }

    cartouche = disp.createImageSprite(exo,"cartouche");
    exo.blocAnimation.append(cartouche);
    if(q%3===0) {
	cartouche.css({
	    left:735+cartouche.width(),
	    top:67
	});
/*        cartouche.position({
            my:'left center',
            at:'right center-100',
            of:exo.blocAnimation
        });
*/  } else if (q%3==1){
	cartouche.css({
	    left:-cartouche.width(),
	    top:67
	});
/*	
        cartouche.position({
            my:'right center',
            at:'left center',
            of:exo.blocAnimation
        });
        
*/  } else if (q%3==2){
	cartouche.css({
	    left:367.5-(cartouche.width()/2),
	    top:450
	});
/*        cartouche.position({
            my:'center bottom',
            at:'center top-100',
            of:exo.blocAnimation
        });        
*/  }
    //
    var etiquette = disp.createTextLabel(texte);
    etiquette.css({
        fontSize:32,
        fontWeight:'bold',
        color:'#fff'
    });
    cartouche.append(etiquette);
    etiquette.position({
        my:'center center',
        at:'center center',
        of:cartouche
    });
    if(q%3===0) {
        cartouche.animate({left:67},800,'easeInCubic');
    } else if(q%3==1) {
        cartouche.animate({left:67},800,'easeInCubic');
    } else {
        cartouche.animate({top:67},800,'easeInCubic');
    }
    //
    champReponse = disp.createTextField(exo,6);
    //
    exo.blocAnimation.append(champReponse);
    champReponse.position({
        my:'center center',
        at:'center center+50',
        of:exo.blocAnimation
    });
    champReponse.focus();
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var q = exo.indiceQuestion;
    if (champReponse.val() === "") {
       return "rien" ;
    } else if( Number(champReponse.val()) == aNombre[q][1] ) {
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
    var correction = disp.createCorrectionLabel(aNombre[q][1]);
    exo.blocAnimation.append(correction);
    correction.position({
        my:"left center",
        at:"right+20 center",
        of:champReponse,
    });
    var barre = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre);
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"temps_question",
        texte:"Temps par question (en s) : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:144,
        taille:12,
        nom:"nombreDonne",
        texte:"Nombres de départ : "
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeComplement",
        texte:"Type de complément : ",
        aValeur:[0,1,2,3,4],
        aLabel:["à la dizaine supérieure ","à une dizaine supérieure","à la centaine supérieure","à une centaine supérieure","le complément est multiple de 10"]
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"checkbox",
        largeur:24,
        taille:2,
        nom:"typeEnonce",
        texte:"Type d'énoncé : ",
        aValeur:[1,2,4],
        aLabel:["A + ? = C ", "A pour aller à C", "A - B = ?"]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));