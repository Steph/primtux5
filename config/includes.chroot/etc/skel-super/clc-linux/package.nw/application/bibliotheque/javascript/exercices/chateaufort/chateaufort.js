var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.chateaufort = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var stage;
var Herbe_Tab;  // tableau contenant les herbes
var M;		// La muraille
var Larg //La largeur de l'ouverture;
var LBloc // La largeur du bloc;
var Pu,Bo // shape bloc unités et diviseur
var Input1,Input2 // Les champs de texte réponse
var Quotient,Reste // La solution
var Controle // container contenant les champs de texte
var L // La fleche donnant la largeur de l'ouverture
var B // Le barbare...grrrrr
var Bouton_Valider
var Essai // Le nombre de tentatives: j'ai essayé avec exo.indiceTentative, mais j'obtiens toujours 0 lorsque je valide la réponse... Je m'en passerai donc.
var S // Le soldat
var Q //le tableaux contenant les 10 questions
var Tab_Mur=[] // Tableau contenant les murs en sous nombres. Rempli lorsque le nombre est insuffisant, dans uncorrect2.
var  toucheValider;
var Ready=false;
var T0,T1; //date de début et de fin de question
var TTotal=0;  //temps total

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:10,
        totalEssai:2,		
        tempsExo:300,
        largeur_bloc:"2;9", //largeur min et max des blocs
        largeur_porte:"20;100", //largeur min et max de l'ouverture à combler
        nombre_bloc:"4;9", //nombre de blocs min et max de largeur largeur_bloc pour combler l'ouverture
        reste:"0;100" // "pourcentage approximatif" que représente le reste par rapport au diviseur: 
        //0% implique que largeur_porte est un multiple de largeur_bloc
        // si le min est supérieur à 0, alors le reste ne sera jamais nul
        //100% implique que le reste vaut largeur_bloc-1 (et non pas largeur_bloc comme le pourcentage pourrait le faire croire).
        
        
		
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "chateaufort/textes/chateaufort_fr.json",
	illustration:"chateaufort/images/illustration.jpg",
	herbe: "chateaufort/images/herbe.png",
	tour: "chateaufort/images/Tour.png",
	mur: "chateaufort/images/Mur.png",
	tete: "chateaufort/images/Tete.png",
	tete2: "chateaufort/images/Tete2.png",
	corps: "chateaufort/images/Corps.png",
	bras1: "chateaufort/images/Bras1.png",
	bras2: "chateaufort/images/Bras2.png",
	bras3: "chateaufort/images/Bras3.png",
	pied1: "chateaufort/images/Pied1.png",
	pied2: "chateaufort/images/Pied2.png",
	soldat: "chateaufort/images/Soldat.png",
	bulle: "chateaufort/images/Bulle.png",
	
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    var a,b,q,r,p
    
    function Parse_Tab(t){
        for(var i=0;i<t.length;i++){
            t[i]=parseInt(t[i]);
        }
    }
    
	Q=[];
    var lb=exo.options.largeur_bloc.split(";");
    var lp=exo.options.largeur_porte.split(";");
    var re=exo.options.reste.split(";");
    var nb=exo.options.nombre_bloc.split(";");
    
    Parse_Tab(lb);
    Parse_Tab(lp)
    Parse_Tab(re)
    Parse_Tab(nb)
    
    
    if(nb[0]<=0){nb[0]=1}
    if(lp[0]<=0){lp[0]=1}
    if(lb[0]<=0){lb[0]=1}
    
    for(var i=0;i<exo.options.totalQuestion;i++){
        b=Math.floor(lb[0]+Math.random()*(lb[1]-lb[0]));
        q=Math.floor(nb[0]+Math.random()*(nb[1]-nb[0]));
        if(re[0]==0 && re[1]==0){r=0}
            else{              
                r=Math.floor(re[0]+Math.random()*(re[1]-re[0]));             
                r=Math.floor(q*r/100);              
                if(r==q){r--}
                while(re[0]>0 && r==0){
                      r=Math.floor(re[0]+Math.random()*(re[1]-re[0]));             
                      r=Math.floor(q*r/100);                    
                }
            }
        a=b*q+r;
        while(a>lp[1] || a<lp[0]){
             b=Math.floor(lb[0]+Math.random()*(lb[1]-lb[0]));
            q=Math.floor(nb[0]+Math.random()*(nb[1]-nb[0]));
            if(re[0]==0 && re[1]==0){r=0}
                else{
                r=Math.floor(re[0]+Math.random()*(re[1]-re[0]));
                r=Math.floor(q*r/100);
                if(r==q){r--}
                 while(re[0]>0 && r==0){
                      r=Math.floor(re[0]+Math.random()*(re[1]-re[0]));             
                      r=Math.floor(q*r/100);                    
                }
                
            }
        a=b*q+r;        
            
        }   
      console.log(a+"="+b+"x"+q+"+"+r)
	Q.push(a,q);
        
    }
    console.log(Q);
	
	
	
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
	var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // on ajoute un element canvas au bloc animation 
    // Attention ses dimensions doivent être fixées avec .attr(width:xx,height:xx)
    // et pas avec .css({width:xx,height:yy})
	T0= new Date().getTime();   
	T1=0;         
    var canvas = $("<canvas id='cnv'></canvas>");
    canvas.attr({width:735,height:400});
    canvas.css({position:'absolute',top:0,background:"#000"});
    exo.blocAnimation.append(canvas);
    //on cree la scene avec createjs
    stage = new createjs.Stage("cnv");

    /**************************************/
    exo.aStage.push(stage); // pour qu'on puise appeler Touch.disable() au déchargement de l'exercice
	createjs.Ticker.setFPS(60);
    createjs.Ticker.timingMode = createjs.Ticker.RAF; // pour une animation Tween plus fluide
    createjs.Ticker.addEventListener("tick", stage); // pour que la scene soit redessinée à chaque "tick"
    createjs.Ticker.addEventListener("tick", createjs.Tween); // pour que Tween fonctionne après un Ticker.reset() au déchargement de l'exercice;
    createjs.Touch.enable(stage);// pour compatibilité avec les tablettes tactiles
    /*************************************/
	exo.keyboard.config({
		
		arrow : "disabled"
		
    });
	//Larg=Math.floor(20+80*Math.random());
	//LBloc=2+Math.floor(Math.random()*(Larg/2.5));
	Larg=Q[2*exo.indiceQuestion];
	LBloc=Q[2*exo.indiceQuestion+1];
	console.log(Q);
	Essai=0
	Ready=false;
	
	// Faux bouton valider pour mieux gérer les transitions entre chaque question
   Bouton_Valider = disp.createTextLabel("Valider");
   Bouton_Valider.css({
        fontSize:16,
		"position":"center",
       	opacity:1,       
	    "padding-left":20,
		"padding-right":20,
		"padding-top":5,
		"padding-bottom":5,
	    border:"1px solid #aaa",
	    "border-radius":5,
	    "background-color":"#fff",
	    cursor:"pointer",	
	    left:510,
	    top:370,
		margin:"auto"
    });
	
	 toucheValider = exo.keyboard.baseContainer.find('.key.large');
     toucheValider.data("valeur","exo");// permet de redefinir le comportement de la touche "Valider"
    
  
	
	Quotient=Math.floor(Larg/LBloc);
	Reste=Larg-Quotient*LBloc
	
	var Terre=new createjs.Container()
	var Sol=new createjs.Shape();
	Terre.addChild(Sol);
	Sol.graphics.beginLinearGradientFill(["#336600","#66FF00"], [0, 1], 0, 0, 0, 200);
	Sol.graphics.drawRect(0,0,735,200);
	Sol.graphics.endFill();
	Terre.y=200
	Terre.addChild(Sol);
	stage.addChild(Terre);
	
	var Ciel=new createjs.Shape()
	Ciel.graphics.beginLinearGradientFill(["#0066FF","#0099FF"], [0.1, 0.5], 0, 0, 0, 200);
	Ciel.graphics.drawRect(0,0,735,200);
	Ciel.graphics.endFill();
	Ciel.y=0
	stage.addChild(Ciel);
	
	function Barbare(){
		this.C=new createjs.Bitmap(exo.getURI("corps"))
		this.T=new createjs.Bitmap(exo.getURI("tete"))
		this.T2=new createjs.Bitmap(exo.getURI("tete2"))
		this.B1=new createjs.Bitmap(exo.getURI("bras1"))
		this.B2=new createjs.Bitmap(exo.getURI("bras2"))
		this.B3=new createjs.Bitmap(exo.getURI("bras3"))
		this.P1=new createjs.Bitmap(exo.getURI("pied1"))
		this.P2=new createjs.Bitmap(exo.getURI("pied2"))
		this.P3=new createjs.Bitmap(exo.getURI("pied1"))
		this.Bar=new createjs.Container()
		
		Barbare.prototype.Dessine=function(){
			
			this.Bar.removeAllChildren();
			this.Bar.addChild(this.P1);
			this.P1.x=55;
			this.P1.y=95;
		    this.Bar.addChild(this.P2)
			this.P2.x=10;
			this.P2.y=95;
			
			this.B2.regY=8;
			this.B2.x=60
			this.B2.y=25
			this.Bar.addChild(this.B2)
			
			
			this.Bar.addChild(this.C);
			
			this.B1.regY=25;
			this.B1.regX=80;
			this.B1.x=55
			this.B1.y=25
			this.Bar.addChild(this.B1)
			
			this.T.x=-15
			this.T.y=-85
			this.Bar.addChild(this.T)				
			
		}
		
		Barbare.prototype.Dessine2=function(){
			
			this.Bar.removeAllChildren();
			this.Bar.addChild(this.P1);
			this.P1.x=55;
			this.P1.y=80;
		    this.Bar.addChild(this.P3)
			this.P3.x=10;
			this.P3.y=95;
			
			this.B3.regY=8;
			this.B3.regX=60
			this.B3.x=30
			this.B3.y=20
			this.B3.rotation=40;
			this.Bar.addChild(this.B3)		
			
			this.B1.regY=25;
			this.B1.regX=80;
			this.B1.x=70
			this.B1.y=20
			this.Bar.addChild(this.B1)	
			this.B1.rotation=140;
			
			this.Bar.addChild(this.C);
			
			this.T2.x=0
			this.T2.y=-60
			this.Bar.addChild(this.T2)				
			
		}
		
		Barbare.prototype.Marche=function(){
			this.p1=createjs.Tween.get(this.P1,{loop:true}).to({x:10},200).to({x:55},200)
			this.p2=createjs.Tween.get(this.P2,{loop:true}).to({x:55},200).to({x:10},200)
			this.b2=createjs.Tween.get(this.B2,{loop:true}).to({rotation:180},500).to({rotation:0},500)
			this.b1=createjs.Tween.get(this.B1,{loop:true}).to({rotation:-180},500).to({rotation:0},500)
			this.t=createjs.Tween.get(this.T,{loop:true}).to({y:-75},200).to({y:-85},200)
		}
		
		Barbare.prototype.Marche2=function(){
			this.p1=createjs.Tween.get(this.P1,{loop:true}).to({y:95},200).to({y:80},200)
			this.p2=createjs.Tween.get(this.P3,{loop:true}).to({y:80},200).to({y:95},200)
			this.b3=createjs.Tween.get(this.B3,{loop:true}).to({rotation:-40},500).to({rotation:40},500)
			this.b1=createjs.Tween.get(this.B1,{loop:true}).to({rotation:220},500).to({rotation:140},500)
			this.t=createjs.Tween.get(this.T2,{loop:true}).to({y:-75},200).to({y:-60},200)
		}
		
		Barbare.prototype.Stop=function(){
		createjs.Tween.removeTweens(this.T2);
		createjs.Tween.removeTweens(this.T);
		createjs.Tween.removeTweens(this.B2);
		createjs.Tween.removeTweens(this.B1);
		createjs.Tween.removeTweens(this.B3);
		createjs.Tween.removeTweens(this.P2);
		createjs.Tween.removeTweens(this.P1);
		createjs.Tween.removeTweens(this.P3);
		}
	}
	
	function Herbe(xd,yd,xa,ya){
		this.Xd=xd;
		this.Yd=yd;
		this.Xa=xa;
		this.Ya=ya;
		this.Fond=new createjs.Bitmap(exo.getURI("herbe"));
				
		Herbe.prototype.Dessine=function(){	
			var T=Math.sqrt(this.Ya*this.Ya+(368-this.Xa)*(368-this.Xa))*25
			this.Fond.regX=13;
			this.Fond.regY=9;
			this.Fond.x=this.Xd;
			this.Fond.y=this.Yd;
			this.Fond.scaleX=0.1;
			this.Fond.scaleY=0.1;
			Terre.addChild(this.Fond);	
			this.Tw=createjs.Tween.get(this.Fond,{loop:true}).to({x:this.Xa,y:this.Ya,scaleX:1,scaleY:1},T,createjs.Ease.linear());
			this.Tw.setPosition(5000*Math.random());
			this.Tw.setPaused(true) 		
		}		
				
	}
	
	function Muraille(xd,yd,xa,ya,L,T){
		this.Xd=xd;
		this.Yd=yd;
		this.Xa=xa;
		this.Ya=ya;
		this.Largeur=L
		this.Temps=T
		this.Fond=new createjs.Container();
		
				
		Muraille.prototype.Dessine=function(){	
			var Droite=new createjs.Container();
			var T=new createjs.Bitmap(exo.getURI("tour"));
			T.x=-20;
			for(var i=0;i<20;i++){
				var M=new createjs.Bitmap(exo.getURI("mur"));
				M.x=i*100;
				M.y=50;
				Droite.addChild(M);
			}
			Droite.addChild(T);
			Droite.x=this.Largeur/2+10
			this.Fond.addChild(Droite)
			
			var Gauche=Droite.clone(true);
			Gauche.x=-this.Largeur/2-10
			Gauche.scaleX=-1;
			this.Fond.addChild(Gauche)
					
			this.Fond.regY=150; 
			this.Fond.scaleX=this.Fond.scaleY=0.2
			this.Fond.x=this.Xd;
			this.Fond.y=this.Yd;
			stage.addChild(this.Fond);	
			this.Tw=createjs.Tween.get(this.Fond).to({x:this.Xa,y:this.Ya,scaleX:1,scaleY:1},this.Temps,createjs.Ease.linear());
			this.Tw.setPaused(true)	
		}		
				
	}
	
	Herbe_Tab=[]
	
	for(var i=-16;i<16;i++){
		var H=new Herbe(368,0,368+60*i,200);
		H.Dessine();
		Herbe_Tab.push(H);
		
		
	}
		
	M=new Muraille(368,200,368,300,Larg*5,2000);
	M.Dessine();
	
	B=new Barbare();
	B.Dessine2()
	B.Bar.x=318;
	B.Bar.y=350;
    stage.addChild(B.Bar);
	B.Marche2();
	
	for(i=0;i<Herbe_Tab.length;i++){
		Herbe_Tab[i].Tw.setPaused(false);
	}
	
	M.Tw.setPaused(false);
	exo.setTimeout(Stop,2000);	
	
	Controle=new createjs.Container;
	
	Pu=new createjs.Shape();
	Pu.graphics.beginStroke("0x000000");
	Pu.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, 5,0);
	Pu.graphics.drawRect(0,0,5,75);
	Pu.graphics.endFill();
	Pu.x=120+LBloc*5;
	Pu.y=0;
	Pu.graphics.beginStroke("0x000000");
	Pu.graphics.moveTo(10,30)
	Pu.graphics.lineTo(40,60)
	Pu.graphics.moveTo(10,60)
	Pu.graphics.lineTo(40,30)
	var textu = new createjs.Text("1 m", "20px Arial", "#000000")	
	textu.x=120+LBloc*5
	textu.y=85
	Controle.addChild(textu);
	Controle.addChild(Pu);
	
	Bo=new createjs.Shape();
	Bo.graphics.beginStroke("0x000000");
	Bo.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, LBloc*5,0);
	Bo.graphics.drawRect(0,0,LBloc*5,75);
	Bo.graphics.endFill();
	Bo.x=-10
	Bo.y=0
	Bo.graphics.moveTo(5+LBloc*5,30)
	Bo.graphics.lineTo(35+LBloc*5,60)
	Bo.graphics.moveTo(5+LBloc*5,60)
	Bo.graphics.lineTo(35+LBloc*5,30)
	var textbo = new createjs.Text(LBloc+" m", "20px Arial", "#000000")	
	textbo.x=-10
	textbo.y=85
	Controle.addChild(textbo);	
	Controle.addChild(Bo);
	
	
	Controle.x=368-Controle.getBounds().width/2;
	Controle.y=-200;
	stage.addChild(Controle)
	exo.btnValider.hide();
	
	function Stop(){
		Ready=true;
		if(exo.tabletSupport ==false){
		exo.blocAnimation.append(Bouton_Valider);
		Bouton_Valider.on("mousedown",Tester);}
				
		for(i=0;i<Herbe_Tab.length;i++){
		Herbe_Tab[i].Tw.setPaused(true);
		}
		B.Stop();
		Input1 = disp.createTextField(exo,2);
		exo.blocAnimation.append(Input1);
		Input1.css({
                   
					x:Controle.x+30+LBloc*5,
                    y:-200,                    
                });
		
		Input2 = disp.createTextField(exo,2);
		exo.blocAnimation.append(Input2);
		Input2.css({
                   
					x:Controle.x+165+LBloc*5,
                    y:-200,                    
                });
		
		createjs.Tween.get(Controle).to({y:20},1000);
		Input1.animate({y:45},1000);
		Input2.animate({y:45},1000);		
		Input1.focus();
		
		Input1.on("keydown",gestionClavier);
		Input2.on("keydown",gestionClavier);
		
		//Input1.on("click",Input1.focus);
	  //  Input2.on("click",Input2.focus);
		
				
		 // comportement de la touche "Valider" du clavier virtuel.
       
        toucheValider.on("touchstart", Tester);
		
		L=new createjs.Container()
		var Fleche=new createjs.Shape();
		Fleche.graphics.setStrokeStyle(12,"round").beginStroke("#FFFFFF");
		Fleche.graphics.moveTo(-Larg*5/2+20,0);
		Fleche.graphics.lineTo(Larg*5/2-20,0);
		Fleche.graphics.moveTo(-Larg*5/2+40,-20);
		Fleche.graphics.lineTo(-Larg*5/2+20,0);
		Fleche.graphics.lineTo(-Larg*5/2+40,20);
		Fleche.graphics.moveTo(Larg*5/2-40,-20);
		Fleche.graphics.lineTo(Larg*5/2-20,0);
		Fleche.graphics.lineTo(Larg*5/2-40,20);
		var textL = new createjs.Text(Larg+" m", "30px Arial", "#FFFFFF");
		textL.x=-textL.getBounds().width/2;
		textL.y=-60;		
		L.addChild(textL);
		L.addChild(Fleche);		
		L.x=368;
		L.y=250;
		L.scaleX=0;
		L.cache(-Larg*5/2,-70,Larg*5,100);
		stage.addChild(L);
		createjs.Tween.get(L).to({scaleX:1},1000)	
		S=new createjs.Bitmap(exo.getURI("soldat"))
		M.Fond.addChild(S);
		S.x=-M.Largeur/2-120
		var Rappel = disp.createTextLabel(exo.txt.rappel);
    	Rappel.css({
        fontSize:12,
        fontWeight:'bold',
        border:"1px solid #FFCCCC",
		"background-color":"#fff",
		padding:5,
		top:30,
		left:5
    });
	 exo.blocAnimation.append(Rappel);
	
		
		
	}
	
	
}

function gestionClavier(e) {
        //console.log(e)
        if(e.which == 13) {
            Input1.off("keydown",gestionClavier);
		    Input2.off("keydown",gestionClavier);
            Tester();
         
           
        }
    }





function Correct(p){
	//exo.btnSuite.hide();
	
	
	createjs.Tween.get(Controle).to({y:-200},500);
	Input1.animate({y:-200},500);
	Input2.animate({y:-200},500);	
	var i;
	var p1=new createjs.Shape();
	p1.graphics.beginStroke("0x000000");
	p1.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, LBloc*5,0);
	p1.graphics.drawRect(0,0,LBloc*5,75);
	p1.graphics.endFill();
	p1.cache(0,0,LBloc*5,75);
	var p2=new createjs.Shape();
	p2.graphics.beginStroke("0x000000");
	p2.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, 5,0);
	p2.graphics.drawRect(0,0,5,75);
	p2.graphics.endFill();
	p2.cache(0,0,5,75);
	
	for(i=0;i<Tab_Mur.length;i++){
		Tab_Mur[i].alpha=0.5;
	}
	
	for(i=0;i<Quotient;i++){
		var b=p1.clone(true);
		b.cache(0,0,LBloc*5,75);
		b.x=368-Larg/2*5+LBloc*5*i
		b.y=-225;
		if(p=="correction"){stage.addChild(b)} else {
		stage.addChildAt(b,2);}
		createjs.Tween.get(b).wait(500+i*250).to({y:225},500);
	}
	for(i=0;i<Reste;i++){
		var b=p2.clone(true);
		b.cache(0,0,5,75);
		b.x=368-Larg/2*5+LBloc*5*(Quotient)+5*i
		b.y=-225;
		if(p=="correction"){stage.addChild(b)} else {
		stage.addChildAt(b,2);}
		createjs.Tween.get(b).wait(500+i*250+Quotient*250).to({y:225},500);
	}
	if(p!="correction"){
	createjs.Tween.get(L).to({scaleX:0,alpha:0},500)
	var delay=500+Reste*250+Quotient*250	
	exo.setTimeout(Fonce,delay);	} 
	  else {
		stage.setChildIndex(B.Bar,2);
		var delay=500+Reste*250+Quotient*250	
	    exo.setTimeout(exo.avancer,delay);
		
		createjs.Tween.get(Controle).to({y:20},1000);
		var q= new createjs.Text(Quotient, "50px Arial", "#FF0000");
		q.x=Controle.x+30+LBloc*5;
		q.y=45;
		q.alpha=0;
		stage.addChild(q);
		var r= new createjs.Text(Reste, "50px Arial", "#FF0000");
		r.x=Controle.x+165+LBloc*5
		r.y=45;
		r.alpha=0;
		stage.addChild(r);
		createjs.Tween.get(q).wait(1000).to({alpha:1},500);
		createjs.Tween.get(r).wait(1000).to({alpha:1},500);
		createjs.Tween.get(L).wait(1000).to({scaleX:1,alpha:1,y:190},500)
		
	}

function Fonce(){
	B.Bar.regX=70;
	B.Bar.x+=70;
	B.Marche2();
	createjs.Tween.get(B.Bar).to({y:280,scaleX:0.2,scaleY:0.2},2000).call(Mort);

	function Mort(){
		B.Bar.regY=150;
		B.Bar.y+=150*0.2;
		B.Stop();
		exo.setTimeout(tombe,500);	
		function tombe(){
		createjs.Tween.get(B.Bar).to({rotation:90},1000).call(Suite);}
	}
}
}

function Suite(){
	exo.avancer();
	
	
}


function UnCorrect2(){
	console.log("trop petit");
	createjs.Tween.get(Controle).to({y:-200},500);
	Input1.animate({y:-200},500);
	Input2.animate({y:-200},500);
	createjs.Tween.get(L).to({scaleX:0,alpha:0},500)
	var i;
	var p1=new createjs.Shape();
	p1.graphics.beginStroke("0x000000");
	p1.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, LBloc*5,0);
	p1.graphics.drawRect(0,0,LBloc*5,75);
	p1.graphics.endFill();
	p1.cache(0,0,LBloc*5,75);
	var p2=new createjs.Shape();
	p2.graphics.beginStroke("0x000000");
	p2.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, 5,0);
	p2.graphics.drawRect(0,0,5,75);
	p2.graphics.endFill();
	p2.cache(0,0,5,75);
	Tab_Mur=[];
	for(i=0;i<Input1.val();i++){
		var b=p1.clone(true);
		b.cache(0,0,LBloc*5,75);
		b.x=368-Larg/2*5+LBloc*5*i
		b.y=-225;
		
		stage.addChildAt(b,2);
		createjs.Tween.get(b).wait(500+i*250).to({y:225},500);
		Tab_Mur.push(b);
	}
	for(i=0;i<Input2.val();i++){
		var b=p2.clone(true);
		b.cache(0,0,5,75);
		b.x=368-Larg/2*5+LBloc*5*(Input1.val())+5*i
		b.y=-225;
		stage.addChildAt(b,2);
		createjs.Tween.get(b).wait(500+i*250+Input1.val()*250).to({y:225},500);
		Tab_Mur.push(b);
	}
	var delay=100 // +Number(Input2.val())*250+Number(Input1.val())*250
	exo.setTimeout(Endouce,delay);	

function Endouce(){
	B.Dessine()
	B.Marche();
	createjs.Tween.get(B.Bar).to({x:900},1500).call(Entrer);

	function Entrer(){
		B.Bar.scaleX=-0.2
		B.Bar.scaleY=0.2
		B.Bar.x=800
		B.Bar.y=280
		createjs.Tween.get(B.Bar).to({x:368+Larg*5/2},1500).call(In);
		}
	function In(){
		console.log("in");
		B.Stop();
		createjs.Tween.get(B.Bar).to({alpha:0},500);
		exo.setTimeout(Correct("correction"),1000);
	}
}
}


function UnCorrect(){
	createjs.Tween.get(Controle).to({y:-200},500);
	Input1.animate({y:-200},500);
	Input2.animate({y:-200},500);
	createjs.Tween.get(L).to({scaleX:0,alpha:0},500)
	var i;
	var p1=new createjs.Shape();
	p1.graphics.beginStroke("0x000000");
	p1.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, LBloc*5,0);
	p1.graphics.drawRect(0,0,LBloc*5,75);
	p1.graphics.endFill();
	p1.cache(0,0,LBloc*5,75);
	var p2=new createjs.Shape();
	p2.graphics.beginStroke("0x000000");
	p2.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, 5,0);
	p2.graphics.drawRect(0,0,5,75);
	p2.graphics.endFill();
	p2.cache(0,0,5,75);
	
	var pivot=368-5*Larg/2;
	var Bloc_Tab=[]
	
	for(i=0;i<Math.min(10,Input1.val());i++){
		var b=p1.clone(true);
		b.cache(0,0,LBloc*5,75);
		b.regY=40;
		b.rotation=30-Math.random()*60		
		b.x=pivot
		b.y=-225;
		stage.addChildAt(b,2);
		pivot=pivot+LBloc*5
		if(pivot>=368+Larg*5/2){
			pivot=368-5*Larg/2;
		}
		createjs.Tween.get(b).wait(500+i*50).to({y:265},200);
		Bloc_Tab.push(b);
		
	}
	for(i=0;i<Math.min(10,Input2.val());i++){
		var b=p2.clone(true);
		b.cache(0,0,5,75);
		b.regY=40;
		b.rotation=30-Math.random()*60	
		b.x=368+Larg*5/2-Math.random()*Larg*5
		b.y=-225;
		stage.addChildAt(b,2);
		createjs.Tween.get(b).wait(500+i*50).to({y:265},200);
		Bloc_Tab.push(b);
	}
	
	var delay=500+Math.max(Math.min(10,Input2.val()),Math.min(10,Input2.val()))*50	
	
	exo.setTimeout(Fonce2,delay);
	
	function Fonce2(){
	console.log("go");
	B.Bar.regX=70;
	B.Bar.x+=70;
	B.Marche2();
	createjs.Tween.get(B.Bar).to({y:280,scaleX:0.2,scaleY:0.2},1000).call(Boom);

	function Boom(){
		for(var i=0;i<Bloc_Tab.length;i++){
			var a=-Math.PI*Math.random();
			var s=Math.random();
			Bloc_Tab[i].regY=80;
			createjs.Tween.get(Bloc_Tab[i]).to({x:368+700*Math.cos(a),y:368+700*Math.sin(a),scaleX:0.1+0.3*s,scaleY:0.1+0.3*s,rotation:180*360*Math.random()},1000+2000*Math.random())
			
		}
		createjs.Tween.get(B.Bar).to({y:200,scaleX:0.02,scaleY:0.02,alpha:0},2000);
		exo.setTimeout(Correct("correction"),4000);
	}
}
	
}

function Tester(e){
	var Bu,Aide	
	Input2.focus();
	if(isNaN(parseInt(Input1.val()))|| (isNaN(parseInt(Input2.val())) && Input2.val()!="")){
		console.log("des lettres ?");
		return false;
	}
	
	
			if((Input1.val())=="" && (Input2.val())==""){
				console.log("rien")
				exo.avancer();				
				exo.btnValider.hide();
			} 
			else {
			if(Number(Input1.val())==Quotient && Number(Input2.val())==Reste){	
			    
			    toucheValider.off("touchstart", Tester);
			    exo.chrono.pause();
		   		console.log("bon");	
				Bouton_Valider.hide();
				T1=new Date().getTime();
				 TTotal=TTotal+(T1-T0);
		    	Correct("joueur");				
	        }
			if(Number(Input1.val())*LBloc+Number(Input2.val())<Larg){
				console.log("pas assez");
				Essai++;		
		        if(Essai>1 || exo.options.totalEssai==1){
					toucheValider.off("touchstart", Tester);
					T1=new Date().getTime();
					 TTotal=TTotal+(T1-T0);
					exo.chrono.pause();UnCorrect2();Bouton_Valider.hide();
				
				} else {
				
					Bu=new createjs.Bitmap(exo.getURI("bulle"))
					stage.addChild(Bu);
					Bu.x=S.x+20+M.Fond.x
					Bu.y=90
					Aide = new createjs.Text(exo.txt.inferieur, "15px Arial", "#000000");
					Aide.lineWidth=160;
					Aide.x=Bu.x+15;
					Aide.y=Bu.y+10;											
					stage.addChild(Aide);
					exo.setTimeout(Vanish,5000);
					exo.avancer();
				    exo.btnValider.hide();
					exo.chrono.start();
				}		   
	        }
			if(Number(Input1.val())*LBloc+Number(Input2.val())>Larg){	
				console.log("Trop");
				Essai++;		
				if(Essai>1 || exo.options.totalEssai==1){
				    toucheValider.off("touchstart", Tester);
					 
					T1=new Date().getTime();
					 TTotal=TTotal+(T1-T0);
					exo.chrono.pause();UnCorrect();Bouton_Valider.hide();
					} else {
					Bu=new createjs.Bitmap(exo.getURI("bulle"))
					stage.addChild(Bu);
					Bu.x=S.x+20+M.Fond.x
					Bu.y=90
					Aide = new createjs.Text(exo.txt.superieur, "15px Arial", "#000000");
					Aide.lineWidth=160;
					Aide.x=Bu.x+15;
					Aide.y=Bu.y+10;											
					stage.addChild(Aide);
					exo.setTimeout(Vanish,5000);
					exo.avancer();
				    exo.btnValider.hide();
					exo.chrono.start();
				}
			
			}
			
			if(Number(Input1.val())*LBloc+Number(Input2.val())==Larg && Number(Input1.val())!=Quotient){	
				console.log("trop de blocs pour un resultat juste");
				Essai++;		
				if(Essai>1|| exo.options.totalEssai==1){
					toucheValider.data("valeur","suite");// permet de redefinir le comportement de la touche "Valider"
                    toucheValider.off("touchstart", Tester);
					T1=new Date().getTime();
					 TTotal=TTotal+(T1-T0);
					UnCorrect();Bouton_Valider.hide();
					} else {
					Bu=new createjs.Bitmap(exo.getURI("bulle"))
					stage.addChild(Bu);
					Bu.x=S.x+20+M.Fond.x
					Bu.y=90
					Aide = new createjs.Text(exo.txt.egal, "15px Arial", "#000000");
					Aide.lineWidth=160;
					Aide.x=Bu.x+15;
					Aide.y=Bu.y+10;											
					stage.addChild(Aide);
					exo.setTimeout(Vanish,5000);
					exo.avancer();
				    exo.btnValider.hide();
					exo.chrono.start();
				}
			
			}
			}
			
		function Vanish(){
			createjs.Tween.get(Aide).to({alpha:0},500);
			createjs.Tween.get(Bu).to({alpha:0},500);
		}
		}

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {	
  // console.log(TTotal);
  
	if(exo.indiceQuestion==exo.options.totalQuestion-1){
		
		exo.tempsDebutExo=new Date().getTime()-TTotal;
	   		
		
	}
	
	if(Ready==false){
		exo.str.info.msgTempsDepasseJuste=exo.txt.fini;
		exo.str.info.msgTempsDepasseFaux=exo.txt.fini;
		return("rien");
	}
	if(Input1.val()==""){
		return("rien")
	} 
	
	if(Number(Input1.val())==Quotient && Number(Input2.val())==Reste){		
		return "juste";		
	}
	if(Number(Input1.val())*LBloc+Number(Input2.val())<Larg){		
		
		return "faux";		
	}
	if(Number(Input1.val())*LBloc+Number(Input2.val())>Larg){		
	
		return "faux";		
	}
	if(Number(Input1.val())*LBloc+Number(Input2.val())==Larg && Number(Input1.val())!=Quotient){
		return "faux";	
	}

    
};

// Correction (peut rester vide)
exo.corriger = function() {
	Bouton_Valider.hide();
	if(exo.indiceQuestion==exo.options.totalQuestion-1){
		
		exo.tempsDebutExo=0;
	    exo.tempsFinExo=TTotal;
		console.log(exo.dureeExo);
		
	}
	
	    if((exo.tempsFinExo-exo.tempsDebutExo)>exo.options.tempsExo*1000){
	T1=new Date().getTime();
	TTotal=TTotal+(T1-T0);
	exo.tempsDebutExo=0;
	exo.tempsFinExo=TTotal;
	Controle.y=-200;
	Input1.animate({y:-200},500);
	Input2.animate({y:-200},500);	
	var i;
	var p1=new createjs.Shape();
	p1.graphics.beginStroke("0x000000");
	p1.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, LBloc*5,0);
	p1.graphics.drawRect(0,0,LBloc*5,75);
	p1.graphics.endFill();
	p1.cache(0,0,LBloc*5,75);
	var p2=new createjs.Shape();
	p2.graphics.beginStroke("0x000000");
	p2.graphics.beginLinearGradientFill(["#666666","#CCCCCC"], [0, 1], 0, 0, 5,0);
	p2.graphics.drawRect(0,0,5,75);
	p2.graphics.endFill();
	p2.cache(0,0,5,75);
	
	for(i=0;i<Tab_Mur.length;i++){
		Tab_Mur[i].alpha=0.5;
	}
	
	for(i=0;i<Quotient;i++){
		var b=p1.clone(true);
		b.cache(0,0,LBloc*5,75);
		b.x=368-Larg/2*5+LBloc*5*i
		b.y=225;
		stage.addChildAt(b,2)
	}
	for(i=0;i<Reste;i++){
		var b=p2.clone(true);
		b.cache(0,0,5,75);
		b.x=368-Larg/2*5+LBloc*5*(Quotient)+5*i
		b.y=225;
		stage.addChildAt(b,2)
	}	
			
		Controle.y=20
		var q= new createjs.Text(Quotient, "50px Arial", "#FF0000");
		q.x=Controle.x+30+LBloc*5;
		q.y=45;
		
		stage.addChild(q);
		var r= new createjs.Text(Reste, "50px Arial", "#FF0000");
		r.x=Controle.x+165+LBloc*5
		r.y=45;
		
		stage.addChild(r);
		
		L.scaleX=1;
		L.alpha=1;
		L.y=190;
		
		
		stage.update();
	}
	
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:60,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:80,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
     controle = disp.createOptControl(exo,{
        type:"text",
        largeur:80,
        taille:8,
        nom:"largeur_bloc",
        texte:exo.txt.opt4
    });
     exo.blocParametre.append(controle);
        //
     controle = disp.createOptControl(exo,{
        type:"text",
        largeur:80,
        taille:8,
        nom:"nombre_bloc",
        texte:exo.txt.opt7
    });
    exo.blocParametre.append(controle);
   
     //
     controle = disp.createOptControl(exo,{
        type:"text",
        largeur:80,
        taille:8,
        nom:"largeur_porte",
        texte:exo.txt.opt5
    });
    exo.blocParametre.append(controle);
     //
     controle = disp.createOptControl(exo,{
        type:"text",
        largeur:80,
        taille:8,
        nom:"reste",
        texte:exo.txt.opt6
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));