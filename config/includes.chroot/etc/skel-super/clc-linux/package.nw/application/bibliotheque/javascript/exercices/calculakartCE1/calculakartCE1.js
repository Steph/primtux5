var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.calculakartCE1 = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var stage
var R // l'objet Route
var C // l'objet Compteur
var P  // l'objet "5 panneaux"
var Voiture  // le clip de la voiture
var Truck   // l'objet Camion
var Vitesse  // la vitesse courante
var Vitesse_O // la vitesse originelle
var Acc // l'accélaration à chaque bonne réponse;
var NAcc=0 // le nombre d'accélérations appliquées
var Produit,M1,M2;// le produit et sa décomposition en facteurs
var Table=[6,8,9,10,12,14,15,16,18,20,21,24,25,27,28,30,32,35,36,40,42,45,48,49,54,56,63,64,72,81]  // les produits testés
var Voie=230; // la voie haute (130+5) ou basse (230+5)
var WIN // indicateur d'état de la réponse vide-bon-faux-deja
var Fini=false // indique si le dernier camion a été doublé.
var Tab_Produit; // Les tableaux 6x7 contenant les probabilités de sorties des produits
var lastAccident=false

//var FPS=new createjs.Text("fps", "20px Calibri", "#000000");




// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:1,
        totalEssai:1,
		
        //tabletSupport:true,
		tempsQuestion:180,
		acceleration:0.6,
		vitesse:5,
		propositions:3,
		totalTentative:40,
		ponderation0:1,
		ponderation1:1,
		ponderation2:1,
		ponderation3:1,
		ponderation4:0,
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "calculakartCE1/textes/calculakartCE1_fr.json",
	illustration:"calculakartCE1/images/calculakart.jpg",
	troncon:"calculakartCE1/images/Troncon.png",
	bitume:"calculakartCE1/images/Bitume.png",
	compteur:"calculakartCE1/images/Compteur.png",
	aiguille:"calculakartCE1/images/Aiguille.png",
	camion:"calculakartCE1/images/Camion.png",
	panneau:"calculakartCE1/images/Panneau.png",
	voiture:"calculakartCE1/images/Voiture.png",
	traine:"calculakartCE1/images/blurvoiture.png",
	smoke:"calculakartCE1/images/Fumee.png",
	
	
};
//fonction qui génére les tirages possibles
function Generer_Tab(){
	var i;
	if((exo.options.poderation0+exo.options.poderation1+exo.options.poderation2+exo.options.poderation3+exo.options.poderation4)==0){
		exo.options.poderation0=1
		exo.options.poderation1=1
		exo.options.poderation2=1
		exo.options.poderation3=1
		exo.options.poderation4=1
	}
	Tab_Produit=[[1,1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1],[1,1,1,1,1,1,1]]
	
	
	
	var t4=[exo.options.ponderation4,exo.options.ponderation4,exo.options.ponderation4,exo.options.ponderation4,exo.options.ponderation4,exo.options.ponderation4,exo.options.ponderation4];
	Tab_Produit[4]=t4;
	Tab_Produit[5]=t4;
	Tab_Produit[6]=t4;
	Tab_Produit[7]=t4;
	for(i=0;i<Tab_Produit.length;i++){
		Tab_Produit[i][3]=exo.options.ponderation4;
		Tab_Produit[i][4]=exo.options.ponderation4;
		Tab_Produit[i][5]=exo.options.ponderation4;
		Tab_Produit[i][5]=exo.options.ponderation4;
	}
	var t3=[exo.options.ponderation3,exo.options.ponderation3,exo.options.ponderation3,exo.options.ponderation3,exo.options.ponderation3,exo.options.ponderation3,exo.options.ponderation3];
	Tab_Produit[3]=t3;
	for(i=0;i<Tab_Produit.length;i++){
		Tab_Produit[i][2]=exo.options.ponderation3;
	}
	var t2=[exo.options.ponderation2,exo.options.ponderation2,exo.options.ponderation2,exo.options.ponderation2,exo.options.ponderation2,exo.options.ponderation2,exo.options.ponderation2];
	Tab_Produit[2]=t2;
	for(i=0;i<Tab_Produit.length;i++){
		Tab_Produit[i][1]=exo.options.ponderation2;
		
	}
	var t1=[exo.options.ponderation1,exo.options.ponderation1,exo.options.ponderation1,exo.options.ponderation1,exo.options.ponderation1,exo.options.ponderation1,exo.options.ponderation1];
	Tab_Produit[1]=t1;
	for(i=0;i<Tab_Produit.length;i++){
		Tab_Produit[i][0]=exo.options.ponderation1;
	}
	var t0=[exo.options.ponderation0,exo.options.ponderation0,exo.options.ponderation0,exo.options.ponderation0,exo.options.ponderation0,exo.options.ponderation0,exo.options.ponderation0];
	Tab_Produit[0]=t0;
	
		
}


// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
   Voie=230; // la voie haute (130+5) ou basse (230+5)
   Fini=false;
   lastAccident=false
   exo.options.totalTentative=Math.floor((exo.options.vitesse+11*exo.options.acceleration)*40/19);
   
 	
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
	var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // on ajoute un element canvas au bloc animation 
    // Attention ses dimensions doivent être fixées avec .attr(width:xx,height:xx)
    // et pas avec .css({width:xx,height:yy})
	var canvas = $("<canvas id='cnv'></canvas>");
    canvas.attr({width:735,height:400});
    canvas.css({position:'absolute',top:0,background:"#66CC00"});
    exo.blocAnimation.append(canvas);
    //on cree la scene avec createjs
    stage = new createjs.Stage("cnv");
	stage.enableMouseOver()

    /**************************************/
    exo.aStage.push(stage); // pour qu'on puise appeler Touch.disable() au déchargement de l'exercice
   createjs.Ticker.timingMode = createjs.Ticker.RAF; // pour une animation Tween plus fluide
   
  //  createjs.Ticker.setFPS(25);
    createjs.Ticker.addEventListener("tick", stage); // pour que la scene soit redessinée à chaque "tick"
    createjs.Ticker.addEventListener("tick", createjs.Tween); // pour que Tween fonctionne après un Ticker.reset() au déchargement de l'exercice;
    createjs.Touch.enable(stage);// pour compatibilité avec les tablettes tactiles
    /*************************************/
	exo.keyboard.config({
		numeric : "disabled",
		arrow : "disabled",
		large : "disabled"
    });
	
	Generer_Tab()
	exo.str.info.msgTempsDepasseJuste=exo.str.info.msgTempsDepasseFaux="";
	Vitesse_O=Vitesse=exo.options.vitesse;
	Acc=exo.options.acceleration;
	var B=new createjs.Bitmap(exo.getURI("bitume"))	
	B.y=111;
	stage.addChild(B);	
	R=new Route(Vitesse/2);
	C=new Compteur();
	C.Bouge(Vitesse);
	P=new Panneau(10,20,30,40,50);
	exo.btnValider.hide()	
	R.Roule();
	Truck=new Camion(800,230,Vitesse/4);
	Truck.Roule();
	P.Change(Question());
	Truck.ChangeColor(M1+" x "+M2);
	
	Voiture=new Car();
	Voiture.Fond.x=100;
	Voiture.Fond.y=235;
	stage.addChild(Voiture.Fond)
	//stage.addChild(FPS);
			
		
}
function speedup(v){
	    NAcc++;	
		if(NAcc>11){
		NAcc=11}
	 	R.Up_Vitesse(v);		
		Voiture.Vite(3)				
	}

function Question(){
	var alea=[]
	var i,j,k;
	for(i=0;i<Tab_Produit.length;i++){
		for(j=0;j<Tab_Produit[i].length;j++){
			for(k=0;k<Tab_Produit[i][j];k++){
				alea.push([i,j,(i+2)*(j+3)]);
			}
		
		}
	}
	
	var t=Math.floor(Math.random()*alea.length);
	
	M1=alea[t][0]+2;
	M2=alea[t][1]+3;
	console.log(M1+"/"+M2);
	
	Produit=M1*M2
	 i=Table.indexOf(Produit);
	var result=[];
	if(i<exo.options.propositions){
		//console.log("cas1")
		for(j=0;j<exo.options.propositions;j++){
		result.push([Table[j]]);
		}
	}
	if(i>29-exo.options.propositions){
	//	console.log("cas2")
		for(j=0;j<exo.options.propositions;j++){
		result.push(Table[30-exo.options.propositions+j]);
		}
		
	}
	if(i>exo.options.propositions-1 && i<30-exo.options.propositions){
		
		var d=Math.floor(Math.random()*exo.options.propositions);
	//	console.log("cas3:"+d);
		for(j=0;j<exo.options.propositions;j++){		
		result.push(Table[i-d+j]);
	
		}
		
	}
	
	return(result)	
	
}



function Panneau(a,b,c,d,e){
	var Clip=new createjs.Container();
	var Re=[a,b,c,d,e]
	var Pa=new Array()
	var Te=new Array()
	
	var i;
	for(i=0;i<exo.options.propositions;i++){
		Pa.push(new createjs.Bitmap(exo.getURI("panneau")))
		Pa[i].x=10+110*i;
		Clip.addChild(Pa[i]);	
		Te.push(new createjs.Text(Re[i], "40px Calibri", "#000000"));
		Te[i].textBaseline = "alphabetic";
		Te[i].textAlign = "center";
		Te[i].mouseEnabled=false;
		Te[i].y=56;
		Te[i].x=56+110*i;
		Pa[i].resultat=Re[i];
		Pa[i].addEventListener("click",Reponse)
		Clip.addChild(Te[i]);		
	}	
	Clip.y=305;
	stage.addChild(Clip);
	
	this.Change=function(t){
		Re=new Array()
		for(i=0;i<t.length;i++){
			Re.push(t[i]);
			Te[i].text=t[i];
			Pa[i].resultat=t[i];
			Pa[i].y=0;
			Te[i].y=56;
			Pa[i].addEventListener("click",Reponse);
			WIN="vide"
		}		
	}
	
	this.Soluce=function(){
		for(i=0;i<exo.options.propositions;i++){
		Pa[i].removeEventListener("click",Reponse);
		if(Pa[i].resultat!=Produit){
		createjs.Tween.get(Pa[i]).to({y:100},200,createjs.Ease.elasticOut());
		createjs.Tween.get(Te[i]).to({y:156},200,createjs.Ease.elasticOut());}		
		}	
	}
	
	
}


function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function finish(){
		exo.setTimeout(exo.poursuivreQuestion,10);  // un délai car si appel direct par un call en fin de tween, le chrono bug.
	}


function Camion(x,y,V){
	
	var Clip=new createjs.Container();
	Clip.Vitesse=V
	var C=new createjs.Bitmap(exo.getURI("camion"))
	var M=new createjs.Shape();	
	M.graphics.f("#000000").s().p("AqTDfQgWgFgDgIIg6gBIAAAQIgVAAIAAnBIAVAAIAAAQIA6gBQADgIAWgFQAagHAkAAQAkAAAZAHQAXAFADAIIAMAAQADgHATgFQAYgGAiAAQAiAAAYAGQATAFAEAHIGcAAQABgHAWgEQAYgFAhgBQAhABAXAFQAWAEACAHIA0AAIAAgOIASAAIAAAtIALAEIA1AAIABAQIAVABIgBgTIBaAAIAFgjQARAJADAhQAagZBFAIQBCgEANAlIAyAAIAAAEQAnA6gEBbIAAAgQgEBHgfAvIAAAEIgyAAQgNAlhCgEQhFAIgagZQgDAhgRAJIgFgjIhaAAIABgTIgVABIgBAQIgyAAIgOAEIAAAtIgSAAIAAgOIg0AAQgCAHgWAEQgXAGghAAQghAAgYgGQgWgEgBgHImcAAQgEAHgTAFQgYAGgiAAQgiAAgYgGQgTgFgDgHIgMAAQgDAIgXAFQgZAHgkgBQgkABgagHg");
	M.setTransform(78.1,23);	
	var Fond=new createjs.Shape();
	Fond.graphics.beginFill(getRandomColor()).drawRect(0, 0, 200, 60)		
	Fond.mask=M	
	Clip.addChild(Fond);
	Clip.addChild(C);	
	Clip.y=y;
	Clip.x=x;	
	var Text=new createjs.Text("7 x 8", "40px Calibri", "#000000")	
	Text.x=55;
	Text.y=35;
	Text.textBaseline = "alphabetic";
	Text.textAlign = "center";		
	Clip.addChild(Text);
	Clip.cache(0,0,160,50);	
	stage.addChild(Clip);
	
	this.ChangeColor=function(t){
		Fond.graphics.clear();
		Fond.graphics.beginFill(getRandomColor()).drawRect(0, 0, 200, 60)
		Text.text=t;
		Clip.updateCache();
	}	
		
	
	
	this.Avance=function(e){	
		Clip.Vitesse=Vitesse_O/4+NAcc*Acc/4;
		if(Clip.Vitesse>Vitesse_O/4+Acc*11/4){
			Clip.Vitesse=Vitesse_O/4+Acc*11/4
		}
		if(Clip.Vitesse<0){
			Clip.Vitesse=Vitesse_O/4
		}
		Clip.x=Clip.x-Clip.Vitesse;
		if(Clip.x<=-200 ){		
			if(!Fini || lastAccident){
				if(WIN=="bon"){		
					exo.poursuivreQuestion();
				
					WIN="faux";
					}
				P.Change(Question());					 
			} else {
					e.remove();
					if(WIN=="bon"){
					createjs.Tween.get(Voiture.Fond).to({x:800},1000,createjs.Ease.linear()).call(finish);}
				}
					 
				Fond.graphics.clear();
				Fond.graphics.beginFill(getRandomColor()).drawRect(0, 0, 200, 60)
				Text.text=M1+" x "+M2;
				Clip.updateCache();
				Clip.y=Voie;
				Clip.x=800;
				} else {
					if(Clip.x<=Voiture.Fond.x+45){
					
					if(WIN!="bon" && WIN!="deja"){
						 if(!Fini){
						 WIN="deja",Accident();}
						 else {
						createjs.Tween.get(Voiture.Fond).to({y:Voie+5,rotation:1000,x:250},1000,createjs.Ease.linear()).call(finish);
					     }
					}			
			
					}
				}
		
	}
	this.Roule=function(){				
		Clip.on("tick",this.Avance);		
	}
	
}

function Car(){
	this.Fond=new createjs.Container()
	var Clip=new createjs.Bitmap(exo.getURI("voiture"))
	var Blur=new createjs.Bitmap(exo.getURI("traine"))
	
	
	var masque=new createjs.Shape();
	masque.graphics.beginFill(getRandomColor()).drawRect(-100, 0, 120, 40)		
	Blur.mask=masque	
	
	
	this.Vite=function(d){
		
		Blur.visible=true
		Blur.x-=d;
		Blur.x=Math.max(-70,Blur.x)
	}
	this.Lent=function(){
		Blur.x=-10
		Blur.visible=false;
	}
	
	this.Zip=function(){
		Blur.x=-10
		Blur.visible=false;
		for(i=0;i<20;i++){
		var Smoke1=new createjs.Bitmap(exo.getURI("smoke"))
		Smoke1.alpha=0
		Smoke1.y=10
		this.Fond.addChild(Smoke1);
		var r=2*Math.PI*Math.random();
		var X1=50*Math.cos(r);
		var Y1=10+50*Math.sin(r);
		createjs.Tween.get(Smoke1).wait(100*i).to({rotation:360,x:X1,y:Y1,alpha:1},500,createjs.Ease.elasticOut()).to({alpha:0},100);
		}
	}
	Blur.x=-20
	this.Fond.addChild(Blur,Clip)
	
	
}

function Compteur(){
	var Clip=new createjs.Container();
	var c=new createjs.Bitmap(exo.getURI("compteur"))
	var a=new createjs.Bitmap(exo.getURI("aiguille"))	
	c.regX=98
	c.regY=98
	a.regX=72;
	a.regY=6;
	a.rotation=0;
	Clip.addChild(c);
	Clip.addChild(a);
	Clip.x=735
	Clip.y=400
	stage.addChild(Clip);
	
	this.Bouge =function(v){
		createjs.Tween.get(a).to({rotation:10+NAcc*90/15},500,createjs.Ease.elasticOut());	
	}
}

function Route(V){
	var Clip=new createjs.Container();	
	Clip.Vitesse=V
	var i;
	for(i=0;i<5;i++){
		var r=new createjs.Bitmap(exo.getURI("troncon"))	
		r.y=100;
		r.x=i*200;
		Clip.addChild(r)
	}
	Clip.cache(0,0,1000,300);	
	stage.addChild(Clip);
	this.Up_Vitesse=function(v){
		
		C.Bouge(Clip.Vitesse);
	}
	this.Avance=function(e){
		Clip.Vitesse=Vitesse_O+NAcc*Acc;
		if(Clip.Vitesse>Vitesse_O+Acc*10){
			Clip.Vitesse=Vitesse_O+Acc*10
		}
		if(Clip.Vitesse<0){
			Clip.Vitesse=Vitesse_O
		}
		Clip.x=Clip.x-Clip.Vitesse;
		if(Clip.x<=-200){
	  var f=createjs.Ticker.getMeasuredFPS();
	 
	  Vitesse_O=exo.options.vitesse*60/createjs.Ticker.getMeasuredFPS();
	  Acc=exo.options.acceleration*60/createjs.Ticker.getMeasuredFPS();
	//  FPS.text=Math.round(createjs.Ticker.getMeasuredFPS())+"/"+Math.round(Vitesse_O)+"/"+Math.round(Acc);
		Clip.x=0;}
		//if(Fini)
//		{ e.remove();
//		}
	}
	this.Roule=function(){				
		Clip.on("tick",this.Avance);
		
	}
	
}
function Redresse(){
	
			createjs.Tween.get(Voiture.Fond).to({x:100},1000)
			Voiture.Fond.rotation=0;		
	
}
function Reponse(e){
	var i,j;
	P.Soluce();
	if(e.target.resultat==Produit){
		Tab_Produit[M1-2][M2-3]=Tab_Produit[M1-2][M2-3]-1;
		var c=0;
		for(i=0;i<Tab_Produit.length;i++){
		for(j=0;j<Tab_Produit[i].length;j++){
				c=c+Tab_Produit[i][j];	
		}
	}
	if(c==0){
	Generer_Tab();
	}
		WIN="bon";
		//Vitesse+=Acc;
		speedup(Acc);
		if(Voie==130){
			Voie=230
			Voiture.Fond.rotation=10
		} else  {
			Voie=130
			Voiture.Fond.rotation=-10
		}
		
		createjs.Tween.get(Voiture.Fond).to({y:Voie+5},300,createjs.Ease.linear()).call(Redresse);
		
		
	} else {
		
		WIN="faux";
		
	}
}

function Accident(){	
	Vitesse=Vitesse_O;
	NAcc=0;
	Voiture.Zip();
	speedup(-100);
	P.Soluce();
	Tab_Produit[M1-2][M2-3]=Tab_Produit[M1-2][M2-3]+2;		
	
	if(Voie==130){
			Voie=230			
		} else  {
			Voie=130			
		}
	if(!Fini){
	createjs.Tween.get(Voiture.Fond).to({y:Voie+5,rotation:1440,x:250},1000,createjs.Ease.linear()).call(Redresse);
	exo.poursuivreQuestion();
	if( exo.indiceTentative==exo.options.totalTentative-1){
		lastAccident=true;
	}
	
	}
	
		
		
	
}

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {	
if( exo.indiceTentative==exo.options.totalTentative-1){
	     	Fini=true;
	        	
			
		}
	
	if(WIN=="bon"){
	return "juste"}
	{return("faux")};
	
	    
};

// Correction (peut rester vide)
exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"vitesse",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:50,
        taille:4,
        nom:"acceleration",
        texte:exo.txt.opt2
    });
    exo.blocParametre.append(controle);
	 controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:1,
        nom:"propositions",
        texte:exo.txt.opt3,
        aLabel:exo.txt.label3,
        aValeur:[3,4,5]
    });
	exo.blocParametre.append(controle);
	  controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"ponderation0",
        texte:exo.txt.opt8,
        aLabel:exo.txt.label8,
        aValeur:[0,1,2,3,4,5]
    });	
    exo.blocParametre.append(controle);
	  controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"ponderation1",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[0,1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
	exo.blocParametre.append(controle);
	  controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"ponderation2",
        texte:exo.txt.opt5,
        aLabel:exo.txt.label5,
        aValeur:[0,1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
	exo.blocParametre.append(controle);
	  controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"ponderation3",
        texte:exo.txt.opt6,
        aLabel:exo.txt.label6,
        aValeur:[0,1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
	exo.blocParametre.append(controle);
	  controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"ponderation4",
        texte:exo.txt.opt7,
        aLabel:exo.txt.label7,
        aValeur:[0,1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));