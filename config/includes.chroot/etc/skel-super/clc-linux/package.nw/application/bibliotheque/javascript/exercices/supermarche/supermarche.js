var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.supermarche = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    

var repEleve, soluce, etiquetteQuestion, champReponse1, champReponse2, symboleEuro, arNombre, aNombre, paperQuestion;


exo.oRessources = { 
    //illustration : "monexo/images/illustration.png"
};

// Options par dÃ©faut de l'exercice (dÃ©finir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
    	temps_question:0,
    	temps_exo:0,
    	niveau:1,
    	vitesse:2
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// CrÃ©ation des donnÃ©es de l'exercice (peut rester vide), exÃ©cutÃ©e a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
        aNombre = [];
        var min,max,nArticle;
	var prixA,centimeA,prixB, centimeB, prixC,centimeC, prixD,prixE,soluceTemp;
	var aTemp = [];
	if( exo.options.niveau ==  1 ) {
		// 2 articles en euros, un article en euros centimes 
		// resultat < 20
		max=9;
		min=2;
		for ( i = 0; i < exo.options.totalQuestion ; i++ ) {
			centimeA = (1 + Math.floor(Math.random()*(9-1+1)))*10;
			prixA = (0 + Math.floor(Math.random()*(9-0+1))) + centimeA/100;
			prixB = 2 + Math.floor(Math.random()*(6-2+1));
			prixC = 2 + Math.floor(Math.random()*(6-2+1));
			soluceTemp = ( prixA * 100 + prixB * 100 + prixC * 100 ) / 100;
			aTemp = [ prixA, prixB, prixC ];
			// aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aTemp);
			aTemp.push(soluceTemp);
			if ( testePresence( aTemp, aNombre) === false ) {
				aNombre.push(aTemp);
			} else {
				i--;
			}
		}
	}
	else if( exo.options.niveau ==  2 ) {
		// 2 articles en euros centimes, un article en euros 
		// resultat < 20 pas de conversion
		max=9;
		min=2;
		for ( i = 0; i < exo.options.totalQuestion ; i++ ) {
			centimeA = (1 + Math.floor(Math.random()*(5-1+1)))*10;
			prixA = (0 + Math.floor(Math.random()*(6-0+1))) + centimeA/100;
			centimeB = (1 + Math.floor(Math.random()*(4-1+1)))*10;
			prixB = (1 + Math.floor(Math.random()*(6-1+1))) + centimeB/100;
			prixC = 2 + Math.floor(Math.random()*(6-2+1));
			soluceTemp = ( prixA * 100 + prixB * 100 + prixC * 100 ) / 100;
			aTemp = [ prixA, prixB, prixC ];
			// aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aTemp);
			aTemp.push(soluceTemp);
			if ( testePresence( aTemp, aNombre) === false ) {
				aNombre.push(aTemp);
			} else {
				i--;
			}
		}
	} else if( exo.options.niveau ==  3 ) {
		// 2 articles en euros 50 centimes, un article en euros 
		// resultat < 20
		max=9;
		min=2;
		for ( i = 0; i < exo.options.totalQuestion ; i++ ) {
			prixA = (0 + Math.floor(Math.random()*(9-0+1))) + 0.5;
			prixB = (2 + Math.floor(Math.random()*(8-2+1))) + 0.5;
			prixC = min + Math.floor(Math.random()*(max-min+1));
			soluceTemp = ( prixA * 100 + prixB * 100 + prixC * 100 ) / 100;
			aTemp = [ prixA, prixB, prixC ];
			// aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aTemp);
			aTemp.push(soluceTemp);
			if ( testePresence( aTemp, aNombre) === false ) {
				aNombre.push(aTemp);
			} else {
				i--;
			}
		}
	} else if( exo.options.niveau ==  4 ) {
		// 3 articles, deux dont le prix est en euros et centimes et sont "sympathiques"
		max=9;
		min=2;
		for ( i = 0; i < exo.options.totalQuestion ; i++ ) {
			centimeA = (1 + Math.floor(Math.random()*(9-1+1)))*10;
			centimeB = 100-centimeA;
			prixA = (0 + Math.floor(Math.random()*(9-0+1))) + centimeA/100;
			prixB = (0 + Math.floor(Math.random()*(9-0+1))) + centimeB/100;
			prixC = (1 + Math.floor(Math.random()*(9-1+1)));
			soluceTemp = ( prixA * 100 + prixB * 100 + prixC * 100 ) / 100;
			aTemp = [ prixA, prixB, prixC ];
			// aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aTemp);
			aTemp.push(soluceTemp);
			if ( testePresence( aTemp, aNombre) === false ) {
				aNombre.push(aTemp);
			} else {
				i--;
			}
		}
	} else if( exo.options.niveau ==  5 ) {
		// 3 articles, deux dont le prix est en euros et centimes conversion
		max=9;
		min=2;
		for ( i = 0; i < exo.options.totalQuestion ; i++ ) {
			centimeA = (3 + Math.floor(Math.random()*(9-3+1)))*10;
			centimeB = (3 + Math.floor(Math.random()*(9-3+1)))*10;
			prixA = (0 + Math.floor(Math.random()*(9-0+1))) + centimeA/100;
			prixB = (0 + Math.floor(Math.random()*(9-0+1))) + centimeB/100;
			prixC = (1 + Math.floor(Math.random()*(9-1+1)));
			soluceTemp = ( prixA * 100 + prixB * 100 + prixC * 100 ) / 100;
			aTemp = [ prixA, prixB, prixC ];
			//aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            util.shuffleArray(aTemp);
			aTemp.push(soluceTemp);
			if ( testePresence( aTemp, aNombre) === false ) {
				aNombre.push(aTemp);
			} else {
				i--;
			}
		}
	}
};
function testePresence(arrayNeedle,arrayStack) {
	return arrayStack.some(function(element,index,arr){
		return element.every(function(e,n,a){
			return e==arrayNeedle[n];
		});
	});
}

exo.creerPageTitre = function() {
    exo.blocTitre.html("Le Supermarché");
    exo.blocConsigneGenerale.html("Observer le prix des articles puis écrire la somme à payer.");
    //var illustration = disp.createImageSprite(exo,"illustration");
    //exo.blocIllustration.html(illustration)
};

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    soluce = aNombre[q][aNombre[q].length-1];
    repEleve = "";
    var vitesse = exo.options.vitesse;
    exo.btnValider.hide();
    var svgCnt = disp.createSvgContainer(735,430).css({top:20});
    paperQuestion = svgCnt.paper;
    exo.blocAnimation.append(svgCnt);
    //les paquets
    var svgPaths = ['M0,200L50,200L50,150L0,150Z','M0,200L45,200L45,155L0,155Z','M0,200L50,200L25,150Z','M0,200L40,200L30,160L10,160Z','M 0 200 L 50 200 L 50 170 L 40 170 L 40 180 L 10 180 L 10 170 L 0 170 Z','M 0 200 L10 200L25 180L40 200L50 200L50 140L0 140 Z'];
    var Colors = [];
    Colors[1] = "0";
    Colors[2] = "3";
    Colors[3] = "6";
    Colors[4] = "9";
    Colors[5] = "c";
    Colors[6] = "f";   
    var aPaquet=[];
    for ( i = 0 ; i < aNombre[q].length - 1 ; i++ ) {
        var aleatF = Math.floor(6*Math.random());
        var paquet = paperQuestion.path(svgPaths[aleatF]).attr({'stroke-width':'3','fill':'#'+Colors[Math.floor(1+6*Math.random())]+Colors[Math.floor(1+6*Math.random())]+Colors[Math.floor(1+6*Math.random())]});
        var translatex = 250-200*i;
        paquet.transform("T"+translatex+","+-50);
        paquet.data={pospX:translatex,rangA:i,valeur:aNombre[q][i],enMouv:true};
        aPaquet.push(paquet);
    }
    var aMove=setInterval(function() {
        for ( i = 0 ; i < aNombre[q].length - 1 ; i++ ) {
            if (aPaquet[i].data.pospX<550) {
                aPaquet[i].data.pospX+=vitesse*2;
                aPaquet[i].transform("T"+aPaquet[i].data.pospX+","+"-50");
                if (aPaquet[i].data.pospX>400 && aPaquet[i].data.pospX<480) {
                    caisse.prixtext.attr('text',convertirNombre(aPaquet[i].data.valeur));
                }
            } else {
                if (aPaquet[i].data.enMouv===true) {
                    caisse.prixtext.attr('text','-------');
                    var angleR=30+Math.floor(15*Math.random());
                    //aPaquet[i].transform("r"+angleR);
                    aPaquet[i].data.pospX+=66+Math.floor(40*Math.random());
                    aPaquet[i].transform("T"+aPaquet[i].data.pospX+","+20+"r"+angleR);
                    aPaquet[i].data.enMouv=false;
                }
                if (i==aNombre[q].length - 2) {
                    clearInterval(aMove);
                    suiteDeLExo();
                }
            }
        }
    }, 100);
    function convertirNombre(nombre) {
	nombre = nombre.toFixed(2);
	if ( nombre.indexOf(".") > -1 ) {
		return nombre.split(".").join("€");
	} else {
		return nombre + " €";
	}
    }
    function convertirChaine(st) {
	var chaineToNB = st;
	if (st.indexOf(",")>-1) {
		return Number(st.split(",")[0]+"."+st.split(",")[1]);
	} else {
		return Number(st);
	}
    }
    etiquetteQuestion = disp.createTextLabel("Observe les prix de la caisse");
    exo.blocAnimation.append(etiquetteQuestion);
    etiquetteQuestion.position({
        my:'center center',
        at:'center-155 center+150',
        of:exo.blocAnimation
    });
    function suiteDeLExo() {
        caisse.prixtext.attr('text','-------');
		exo.btnValider.show();
        etiquetteQuestion.remove();
        champReponse1 = "";
        champReponse2 = "";
        etiquetteQuestion = disp.createTextLabel("Somme totale :");
        exo.blocAnimation.append(etiquetteQuestion);
        etiquetteQuestion.position({
            my:'center center',
            at:'center-155 center+150',
            of:exo.blocAnimation
        });
        champReponse1 = disp.createTextField(exo,3);
        champReponse2 = disp.createTextField(exo,2);
        exo.blocAnimation.append(champReponse1);
        exo.blocAnimation.append(champReponse2);
        champReponse1.position({
            my:'center center',
            at:'center-40 center+150',
            of:exo.blocAnimation
        });
		champReponse1.focus();
		champReponse1.on("touchstart",function(e){
			champReponse1.focus();
		});
        symboleEuro = disp.createTextLabel("€");
        exo.blocAnimation.append(symboleEuro);
        symboleEuro.position({
            my:'center center',
            at:'center+3 center+150',
            of:exo.blocAnimation
        });
        champReponse2.position({
            my:'center center',
            at:'center+40 center+150',
            of:exo.blocAnimation
        });
		champReponse2.on("touchstart",function(e){
			champReponse2.focus();
		});
		
    }
    // la caisse
    var caisse = {};
    caisse.base=paperQuestion.rect(0,150,600,100).attr({'fill':'#ddd'});
	caisse.base1=paperQuestion.rect(0,150,600,12).attr({'fill':'#aaa'});
    caisse.base2=paperQuestion.path('M450,150L450,40L580,40L580,150').attr({'fill':'#ddd'});
    caisse.affichBase=paperQuestion.rect(460,50,110,30,4).attr({'fill':'#df5'});
    caisse.prixtext = paperQuestion.text(515,67,'-------');
    caisse.prixtext.attr({"font-size":"18px","font-weight":"bold"});
	caisse.panier1 = paperQuestion.rect(605,160,120,10).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.panier2 = paperQuestion.rect(605,180,120,10).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.panier3 = paperQuestion.rect(605,200,120,10).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.panier4 = paperQuestion.rect(605,220,120,10).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.panier5 = paperQuestion.rect(625,230,80,10).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.panier6 = paperQuestion.rect(605,140,10,100).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.panier7 = paperQuestion.rect(715,140,10,100).attr({'fill':'#6cf','stroke-width':'0'});
	caisse.roue1 = paperQuestion.circle(620,245,10).attr({'fill':'#ccc','stroke-width':'4'});
	caisse.roue2 = paperQuestion.circle(710,245,10).attr({'fill':'#ccc','stroke-width':'4'});
	//le tapis
    for ( i = 0 ; i < 46 ; i++ ) {
        var tapis = paperQuestion.circle(8+13*i,156,5).attr({'fill':'#888','stroke-width':'0'});
    }
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var repEuros = Number(champReponse1.val());
    console.log(repEuros);
    var repCents = Number(champReponse2.val())/100;
    console.log(repCents);
    repEleve = repEuros + repCents;
    console.log(repEleve);
    if (champReponse1.val()==="" && champReponse2.val()==="") {
	    return "rien";
    }
    if (repEleve == soluce){
	return "juste";
    } else {
	return "faux";
    }
    
};

// Correction (peut rester vide)

exo.corriger = function() {
    var correction1 = disp.createCorrectionLabel(soluce.toFixed(2).split(".")[0]);
    var correction2 = disp.createCorrectionLabel(soluce.toFixed(2).split(".")[1]);
    exo.blocAnimation.append(correction1);
    correction1.position({
        my:"center center",
        at:"center center-30",
        of:champReponse1,
    });
    var barre1 = disp.drawBar(champReponse1);
    exo.blocAnimation.append(barre1);
    exo.blocAnimation.append(correction2);
    correction2.position({
        my:"center center",
        at:"center center-30",
        of:champReponse2,
    });
    var barre2 = disp.drawBar(champReponse2);
    exo.blocAnimation.append(barre2);
};

// CrÃ©ation des contrÃ´les permettant au prof de paramÃ©ter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"vitesse",
        texte:"Vitesse : ",
        aLabel:[
            "lente",
	    "moyenne",
	    "rapide"
        ],
        largeur:500,
        aValeur:[1,2,3]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"niveau",
        texte:"Type de calcul : ",
        aLabel:[
            "2 articles en euros, 1 article en euros et centimes",
            "2 articles en euros et centimes, 1 article en euros, sans conversion",
            "2 articles en euros 50 centimes, 1 article en euros ",
            "2 articles en euros et centimes (somme \"sympathique\"), 1 en euros",
            "2 articles en euros et centimes, 1 en euros, avec conversion"
        ],
        largeur:500,
        aValeur:[1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
    
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));