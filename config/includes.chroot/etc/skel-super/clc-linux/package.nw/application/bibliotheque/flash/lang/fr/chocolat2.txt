//Français
// Les questions
p0_1=Cette tablette de chocolat pèse 200 g. Mangez 100 g de cette tablette.
p0_2=Cette tablette de chocolat pèse 200 g. Mangez 50 g de cette tablette.
p0_3=Cette tablette de chocolat pèse 200 g. Mangez 150 g de cette tablette.
p0_4=Cette tablette de chocolat pèse 200 g. Mangez 40 g de cette tablette.
p0_5=Cette tablette de chocolat pèse 200 g. Mangez 120 g de cette tablette.
//
p1_1=Cette tablette de chocolat pèse 240 g. Mangez 120 g de cette tablette
p1_2=Cette tablette de chocolat pèse 240 g. Mangez 60 g de cette tablette.
p1_3=Cette tablette de chocolat pèse 240 g. Mangez 180 g de cette tablette.
p1_4=Cette tablette de chocolat pèse 240 g. Mangez 80 g de cette tablette.
p1_5=Cette tablette de chocolat pèse 240 g. Mangez 160 g de cette tablette.
//
p2_1=Cette tablette de chocolat pèse 300 g. Mangez 150 g de cette tablette.
p2_2=Cette tablette de chocolat pèse 300 g. Mangez 75 g de cette tablette.
p2_3=Cette tablette de chocolat pèse 300 g. Mangez 225 g de cette tablette.
p2_4=Cette tablette de chocolat pèse 300 g. Mangez 100 g de cette tablette.
p2_5=Cette tablette de chocolat pèse 300 g. Mangez 200 g de cette tablette.
//
p3_1=Cette tablette de chocolat pèse 600 g. Mangez 300 g de cette tablette.
p3_2=Cette tablette de chocolat pèse 600 g. Mangez 150 g de cette tablette.
p3_3=Cette tablette de chocolat pèse 600 g. Mangez 450 g de cette tablette.
p3_4=Cette tablette de chocolat pèse 600 g. Mangez 200 g de cette tablette.
p3_5=Cette tablette de chocolat pèse 600 g. Mangez 400 g de cette tablette.
//Le titre et la consiqne générale
p4_1 =La tablette de chocolat
p4_2 =Il faut "manger" une partie de la tablette de chocolat. Exerce-toi sur la tablette ci-dessous avant de commencer.
//Les boutons propres à l'exo
p5_1 =Annuler\rtout
