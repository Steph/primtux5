var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.viaduc = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
var q,repEleve,mavoiture, soluce, champReponse, tabP, tabPSol, aConteneurP,aPosX,aPosY,aPoutrelles, bonneReponse, longueurTotale, longueurPoutrelle, nbPoutrelle;
// Référencer les ressources de l'exercice (textes, image, son)
exo.oRessources = { 
    txt : "viaduc/textes/viaduc_fr.json",
    voiture : "viaduc/images/maFerari.png",
    pilasse : "viaduc/images/pilasse.png",
    poutrelle : "viaduc/images/poutrelle.png",
    fondP : "viaduc/images/fondP.png",
    illustration : "viaduc/images/illustration.jpg"
};
// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        typeDifficulte:1
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};
// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    var a0,a1,a2,a3,a4,a5,a6,a7,tempTab,e;
    if (exo.options.typeDifficulte==1) {
        tabP = [];
        tabP[0]="";
        tabPSol=[];
        tabPSol[0]=[0];
        //q1
        tabP[1]= [];
        a0 = 20;
        a1 = 6+Math.floor(2*Math.random());
        a2 = 6+Math.floor(2*Math.random());
        a3 = a0-a1-a2;
        a4 = 5+Math.floor(2*Math.random());
        a5 = 4+Math.floor(2*Math.random());
        a6 = 8+Math.floor(2*Math.random());
        a7 = 4+Math.floor(2*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[1] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[1]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<4) {
                tabPSol[1].push(i);
            }
	}
        //q2
        tabP[2]= [];
        a0 = 20;
        a1 = 4+Math.floor(2*Math.random());
        a2 = 5+Math.floor(2*Math.random());
        a3 = 6+Math.floor(2*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 4+Math.floor(2*Math.random());
        a6 = 7+Math.floor(2*Math.random());
        a7 = 3+Math.floor(2*Math.random());
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[2] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[2]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[2].push(i);
            }
	}
        //q3
        tabP[3]= [];
        a0 = 30;
        a1 = 7+Math.floor(2*Math.random());
        a2 = 7+Math.floor(2*Math.random());
        a3 = 9+Math.floor(2*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 10+Math.floor(2*Math.random());
        a6 = 9+Math.floor(2*Math.random());
        a7 = 9+Math.floor(2*Math.random());
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[3] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[3]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[3].push(i);
            }
	}
        //q4
        tabP[4]= [];
        a0 = 40;
        a1 = 11+Math.floor(2*Math.random());
        a2 = 20+Math.floor(0*Math.random());
        a3 = a0-a1-a2;
        a4 = 15+Math.floor(2*Math.random());
        a5 = 10+Math.floor(2*Math.random());
        a6 = 15+Math.floor(2*Math.random());
        a7 = 10+Math.floor(2*Math.random());
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[4] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[4]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<4) {
                tabPSol[4].push(i);
            }
	}
        //q5
        tabP[5]= [];
        a0 = 50;
        a1 = 13+Math.floor(2*Math.random());
        a2 = 15+Math.floor(2*Math.random());
        a3 = 11+Math.floor(2*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 10+Math.floor(2*Math.random());
        a6 = 15+Math.floor(2*Math.random());
        a7 = 10+Math.floor(2*Math.random());
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[5] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[5]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[5].push(i);
            }
	}
	//tabP = [[0],[20,6,10,5,4,3,7,5],[20,4,5,6,6,4,5,4],[30,7,8,9,9,12,8,7],[40,8,17,20,15,12,13],[50,17,12,8,11,10,13,9]]
	//tabPSol = [[0],[1,2,4],[1,3,2,6],[1,2,6,7],[1,5,3],[2,3,1,6]];
    }
    if (exo.options.typeDifficulte==2) {
        tabP = [];
        tabP[0]="";
        tabPSol=[];
        tabPSol[0]=[0];
        //q1
        tabP[1]= [];
        a0 = 100;
        a1 = 30;
        a2 = 20;
        a3 = 25;
        a4 = 25;
        a5 = 15;
        a6 = 35;
        a7 = 20;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[1] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[1]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[1].push(i);
            }
	}
        //q2
        tabP[2]= [];
        a0 = 200;
        a1 = 60+10*Math.floor(2*Math.random());
        a2 = 50+10*Math.floor(3*Math.random());
        a3 = 40+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 60+10*Math.floor(3*Math.random());
        a6 = 70;
        a7 = 70;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[2] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[2]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[2].push(i);
            }
	}
        //q3
        tabP[3]= [];
        a0 = 300;
        a1 = 100+10*Math.floor(2*Math.random());
        a2 = 80+10*Math.floor(3*Math.random());
        a3 = 70+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 80+10*Math.floor(3*Math.random());
        a6 = 70+10*Math.floor(3*Math.random());
        a7 = 70+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[3] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[3]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[3].push(i);
            }
	}
        //q4
        tabP[4]= [];
        a0 = 400;
        a1 = 100+10*Math.floor(2*Math.random());
        a2 = 80+10*Math.floor(3*Math.random());
        a3 = 80+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 90+10*Math.floor(3*Math.random());
        a6 = 90+10*Math.floor(3*Math.random());
        a7 = 90+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[4] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[4]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[4].push(i);
            }
	}
        //q5
        tabP[5]= [];
        a0 = 500;
        a1 = 130+10*Math.floor(2*Math.random());
        a2 = 110+10*Math.floor(3*Math.random());
        a3 = 100+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 60+10*Math.floor(3*Math.random());
        a6 = 110+10*Math.floor(3*Math.random());
        a7 = 100+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[5] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[5]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[5].push(i);
            }
	}
	//tabP = [[0],[100,30,20,35,20,25,30,25],[200,50,40,70,50,60,40,70],[400,100,110,120,120,90,90,80],[300,110,80,70,130,110,40,150],[500,110,80,70,190,110,40,130]]
	//tabPSol = [[0],[1,2,5,7],[1,2,4,5],[2,5,3,7],[2,3,1,6],[1,4,7,3]];
    }
    if (exo.options.typeDifficulte==3) {
        tabP = [];
        tabP[0]="";
        tabPSol=[];
        tabPSol[0]=[0];
        //q1
        tabP[1]= [];
        a0 = 500;
        a1 = 130+10*Math.floor(2*Math.random());
        a2 = 130+10*Math.floor(3*Math.random());
        a3 = 130+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 60+10*Math.floor(3*Math.random());
        a6 = 110+10*Math.floor(3*Math.random());
        a7 = 100+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[1] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[1]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[1].push(i);
            }
	}
        //q2
        tabP[2]= [];
        a0 = 700;
        a1 = 150+10*Math.floor(2*Math.random());
        a2 = 160+10*Math.floor(3*Math.random());
        a3 = 130+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 160+10*Math.floor(3*Math.random());
        a6 = 110+10*Math.floor(3*Math.random());
        a7 = 170+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[2] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[2]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[2].push(i);
            }
	}
        //q3
        tabP[3]= [];
        a0 = 800;
        a1 = 200+10*Math.floor(2*Math.random());
        a2 = 150+10*Math.floor(3*Math.random());
        a3 = 230+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 160+10*Math.floor(3*Math.random());
        a6 = 210+10*Math.floor(3*Math.random());
        a7 = 190+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[3] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[3]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[3].push(i);
            }
	}
        //q4
        tabP[4]= [];
        a0 = 1000;
        a1 = 250+10*Math.floor(2*Math.random());
        a2 = 250+10*Math.floor(3*Math.random());
        a3 = 200+10*Math.floor(3*Math.random());
        a4 = a0-a1-a2-a3;
        a5 = 270+10*Math.floor(3*Math.random());
        a6 = 200+10*Math.floor(3*Math.random());
        a7 = 270+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[4] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[4]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[4].push(i);
            }
	}
        //q5
        tabP[5]= [];
        a0 = 1000;
        a1 = 190;
        a2 = 310;
        a3 = 170;
        a4 = a0-a1-a2-a3;
        a5 = 260+10*Math.floor(3*Math.random());
        a6 = 260+10*Math.floor(3*Math.random());
        a7 = 270+10*Math.floor(3*Math.random());
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[5] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[5]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[5].push(i);
            }
	}
	//tabP = [[0],[500,110,120,110,130,140,150,120,130],[700,180,150,165,130,185,150,170],[800,160,250,170,200,160,150,140,240],[1000,210,250,280,330,350,180,250,300],[1000,250,210,220,280,390,320,190,290]]
	//tabPSol = [[0],[1,2,5,8],[1,7,3,5],[1,8,2,6],[1,3,4,6],[3,4,2,8]];
    }
    if (exo.options.typeDifficulte==4) {
        tabP = [];
        tabP[0]="";
        tabPSol=[];
        tabPSol[0]=[0];
        //q1
        tabP[1]= [];
        a0 = 2;
        a1 = (4+Math.floor(2*Math.random()))/10;
        a2 = Math.round(10*(1-a1))/10;
        a3 = (3+Math.floor(2*Math.random()))/10;
        a4 = Math.round(10*(1-a3))/10;
        a5 = (5+Math.floor(3*Math.random()))/10;
        a6 = (5+Math.floor(3*Math.random()))/10;
        a7 = (5+Math.floor(3*Math.random()))/10;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[1] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[1]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[1].push(i);
            }
	}
        //q2
        tabP[2]= [];
        a0 = 2;
        a1 = (6+Math.floor(2*Math.random()))/10;
        a2 = (6+Math.floor(2*Math.random()))/10;
        a3 = Math.round(10*(a0-a1-a2))/10;
        a4 = (6+Math.floor(2*Math.random()))/10;
        a5 = (5+Math.floor(3*Math.random()))/10;
        a6 = (5+Math.floor(3*Math.random()))/10;
        a7 = (5+Math.floor(3*Math.random()))/10;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[2] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[2]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<4) {
                tabPSol[2].push(i);
            }
	}
        //q3
        tabP[3]= [];
        a0 = 3;
        a1 = (7+Math.floor(2*Math.random()))/10;
        a2 = (6+Math.floor(2*Math.random()))/10;
        a3 = (6+Math.floor(2*Math.random()))/10;
        a4 = Math.round(10*(a0-a1-a2-a3))/10;
        a5 = (5+Math.floor(3*Math.random()))/10;
        a6 = (5+Math.floor(3*Math.random()))/10;
        a7 = (5+Math.floor(3*Math.random()))/10;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[3] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[3]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[3].push(i);
            }
	}
        //q4
        tabP[4]= [];
        a0 = 3;
        a1 = (10+Math.floor(2*Math.random()))/10;
        a2 = (6+Math.floor(2*Math.random()))/10;
        a3 = Math.round(10*(a0-a1-a2))/10;
        a4 = (8+Math.floor(3*Math.random()))/10;
        a5 = (8+Math.floor(1*Math.random()))/10;
        a6 = (8+Math.floor(3*Math.random()))/10;
        a7 = (8+Math.floor(3*Math.random()))/10;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[4] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[4]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<4) {
                tabPSol[4].push(i);
            }
	}
        //q5
        tabP[5]= [];
        a0 = 4;
        a1 = (10+Math.floor(2*Math.random()))/10;
        a2 = (9+Math.floor(0*Math.random()))/10;
        a3 = (11+Math.floor(2*Math.random()))/10;
        a4 = Math.round(10*(a0-a1-a2-a3))/10;
        a5 = (12+Math.floor(3*Math.random()))/10;
        a6 = (13+Math.floor(3*Math.random()))/10;
        a7 = (13+Math.floor(3*Math.random()))/10;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[5] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[5]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[5].push(i);
            }
	}
	//tabP = [[0],[2,0.6,1,0.5,0.4,0.3,0.7,0.5],[2,0.4,0.5,0.6,0.6,0.4,0.5,0.4],[3,0.7,0.8,0.9,0.9,1.2,0.8,0.7],[4,0.8,1.7,2,1.5,1.2,1.3],[5,1.7,1.2,0.8,1.1,1,1.3,0.9]]
	//tabPSol = [[0],[1,2,4],[1,3,2,6],[1,2,6,7],[1,5,3],[2,3,1,6]];
    }
    if (exo.options.typeDifficulte==5) {
        tabP = [];
        tabP[0]="";
        tabPSol=[];
        tabPSol[0]=[0];
        //q1
        tabP[1]= [];
        a0 = 1;
        a1 = (24+Math.floor(2*Math.random()))/100;
        a2 = Math.round(100*(0.5-a1))/100;
        a3 = (24+Math.floor(2*Math.random()))/100;
        a4 = Math.round(100*(0.5-a3))/100;
        a5 = (24+Math.floor(3*Math.random()))/100;
        a6 = (24+Math.floor(3*Math.random()))/100;
        a7 = (26+Math.floor(3*Math.random()))/100;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        var e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[1] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[1]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[1].push(i);
            }
	}
        //q2
        tabP[2]= [];
        a0 = 1;
        a1 = (40+Math.floor(0*Math.random()))/100;
        a2 = (25+Math.floor(2*Math.random()))/100;
        a3 = Math.round(100*(1-a1-a2))/100;
        a4 = (30+Math.floor(3*Math.random()))/100;
        a5 = (40+Math.floor(3*Math.random()))/100;
        a6 = (35+Math.floor(3*Math.random()))/100;
        a7 = (30+Math.floor(3*Math.random()))/100;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[2] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[2]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<4) {
                tabPSol[2].push(i);
            }
	}
        //q3
        tabP[3]= [];
        a0 = 1;
        a1 = (25+Math.floor(0*Math.random()))/100;
        a2 = (20+Math.floor(0*Math.random()))/100;
        a3 = (25+Math.floor(3*Math.random()))/100;
        a4 = Math.round(100*(1-a1-a2-a3))/100;
        a5 = (15+Math.floor(3*Math.random()))/100;
        a6 = (35+Math.floor(0*Math.random()))/100;
        a7 = (30+Math.floor(3*Math.random()))/100;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[3] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[3]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[3].push(i);
            }
	}
        //q4
        tabP[4]= [];
        a0 = 1;
        a1 = (21+Math.floor(2*Math.random()))/100;
        a2 = (23+Math.floor(2*Math.random()))/100;
        a3 = (28+Math.floor(2*Math.random()))/100;
        a4 = Math.round(100*(1-a1-a2-a3))/100;
        a5 = (36+Math.floor(3*Math.random()))/100;
        a6 = (31+Math.floor(2*Math.random()))/100;
        a7 = (22+Math.floor(3*Math.random()))/100;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[4] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[4]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[4].push(i);
            }
	}
        //q5
        tabP[5]= [];
        a0 = 1.5;
        a1 = (41+Math.floor(2*Math.random()))/100;
        a2 = (30+Math.floor(2*Math.random()))/100;
        a3 = (40+Math.floor(2*Math.random()))/100;
        a4 = Math.round(100*(1-a1-a2-a3))/100;
        a5 = (36+Math.floor(3*Math.random()))/100;
        a6 = (31+Math.floor(2*Math.random()))/100;
        a7 = (43+Math.floor(3*Math.random()))/100;
        tempTab = [];
        tempTab = [a0,a1,a2,a3,a4,a5,a6,a7];
        e=[];
	for (i=1;i<8;i++) {
            e[i]=Math.floor(Math.random()*7)+1;
	        for (j=1;j<i;j++) {
		    if (e[i]==e[j]) {
		    i--;
		}
	    }
	}
        tabP[5] = [a0,tempTab[e[1]],tempTab[e[2]],tempTab[e[3]],tempTab[e[4]],tempTab[e[5]],tempTab[e[6]],tempTab[e[7]]];
        tabPSol[5]=[];
	for (i=1;i<e.length;i++) {
	    if (e[i]<5) {
                tabPSol[5].push(i);
            }
	}
	//tabP=[[0],[1,0.25,0.24,0.25,0.25,0.26,0.24,0.25,0.24],[1,0.40,0.24,0.45,0.30,0.25,0.36],[1,0.2,0.25,0.3,0.35,0.45,0.15],[1,0.21,0.23,0.29,0.39,0.32,0.19,0.29],[1.5,0.42,0.25,0.4,0.39,0.31,0.43,0.26,0.36]];
	//tabPSol=[[0],[1,3,5,6],[3,5,4],[1,3,4,6],[2,3,1,7],[1,3,2,6]];
    }
};
//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};
//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    
    exo.keyboard.config({
        numeric : "disabled",
        arrow : "disabled"
    });

    q = exo.indiceQuestion+1;
    repEleve=0;
    //consigne
    var uniteL = " m";
    if (exo.options.typeDifficulte==4||exo.options.typeDifficulte==5) {
        uniteL = " km";
    }
    etiquetteQuestion = disp.createTextLabel(exo.txt.cons1+" "+tabP[q][0]+uniteL+".");
    exo.blocAnimation.append(etiquetteQuestion);
    etiquetteQuestion.css({fontSize:20,fontWeight:200,left:(735-etiquetteQuestion.width())/2,top:370,
                            backgroundColor:"#357",color:"#fff",padding:10});
    //decor global
    fondP = disp.createImageSprite(exo,"fondP");
    exo.blocAnimation.append(fondP);
    fondP.css({left:0,top:150});
    mavoiture = disp.createImageSprite(exo,"voiture");
    exo.blocAnimation.append(mavoiture);
    mavoiture.css({left:10,top:128});
    aPoutrelles = [""];
    aConteneurP = [""];
    aPosX = [""];
    aPosY = [""];
    var poutre;
    for (i=1; i<9; i++) {
        poutre = disp.createImageSprite(exo,"poutrelle");
        poutre.css({left:8+74*i,top:173});
        exo.blocAnimation.append(poutre);
    }
    for (i=1; i<8; i++) {
        poutre = disp.createImageSprite(exo,"pilasse");
        poutre.css({left:73+74*i,top:191});
        exo.blocAnimation.append(poutre);
    }
    //le bloc ou sont placées les bouts de route
    var fond2=disp.createEmptySprite();
    fond2.css({left:80,top:151,width:600,height:24,backgroundColor:"#111",opacity:0.1});
    exo.blocAnimation.append(fond2);
    //les morceaux de la route
    var templ = 0;
    for (i=1; i<tabP[q].length; i++) {
        aConteneurP[i] = disp.createEmptySprite();
        templ = tabP[q][i]*600/tabP[q][0];
        aConteneurP[i].css({width:templ,height:20,backgroundColor:"#357",borderBottom:'2px solid #000',borderTop:'2px solid #AAA',borderLeft:'2px solid #AAA',borderRight:'2px solid #AAA'});
        var j=i-1;
        var p=0;
        if (i == 1) {
            aConteneurP[i].css({left:12,top:22});
            aPosX[i] = 12;
            aPosY[i] = 22;
	} else if (i<4) {
            p=20+aConteneurP[j].position().left+aConteneurP[j].width();
            aConteneurP[i].css({left:p,top:22});
            aPosX[i] = p;
            aPosY[i] = 22;
	} else if (i==4) {
	    aConteneurP[i].css({left:12,top:52});
            aPosX[i] = 12;
            aPosY[i] = 52;
	} else if (i<7) {
            p=20+aConteneurP[j].position().left+aConteneurP[j].width();
	    aConteneurP[i].css({left:p,top:52});
            aPosX[i] = p;
            aPosY[i] = 52;
	} else if (i==7) {
	    aConteneurP[i].css({left:12,top:82});
            aPosX[i] = 12;
            aPosY[i] = 82;
	} else if (i<10) {
            p=20+aConteneurP[j].position().left+aConteneurP[j].width();
	    aConteneurP[i].css({left:p,top:82});
            aPosX[i] = p;
            aPosY[i] = 82;
	}
        aConteneurP[i].data({index:i,pose:0,val:tabP[q][i],dernier:0});
        var etiquette = disp.createTextLabel(util.numToStr(tabP[q][i])+uniteL);
        etiquette.css({fontSize:14,color:"#DFD", fontWeight:"bold",textAlign:"center",width:"100%"});
        aConteneurP[i].append(etiquette);
        exo.blocAnimation.append(aConteneurP[i]);
        aConteneurP[i].on("mousedown.clc touchstart.clc",gererClick);
    }
    var dernierPlace = 0;
    var totalX = 78;
    longueurTotale=0;

    function gererClick(e){
        e.preventDefault();
        var eDeplace=$(this);
        if (longueurTotale<tabP[q][0] && eDeplace.data("pose")===0) {
            longueurTotale+=eDeplace.data("val");
            eDeplace.css({left:totalX,top:151});
            totalX+=eDeplace.width();
	    console.log(totalX);
            eDeplace.data("pose",1);
            dernierPlace++;
            eDeplace.data("dernier",dernierPlace);
        } else if (eDeplace.data("pose")==1 && eDeplace.data("dernier")==dernierPlace) {
            longueurTotale-=eDeplace.data("val");
            totalX-=eDeplace.width();
            eDeplace.css({left:aPosX[eDeplace.data("index")],top:aPosY[eDeplace.data("index")]});
            eDeplace.data("pose",0);
            eDeplace.data("dernier",0);
            dernierPlace--;
        }
    }
    //
    
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    if (longueurTotale===0) {
	   return "rien";
    } 
    else if (longueurTotale==tabP[q][0]) {
	   mavoiture.transition({x:670},3000,"easeOutCubic");
	   return "juste";
    } 
    else {
	   return "faux";
    }
};

// Correction (peut rester vide)
exo.corriger = function() {
    for (i=1; i<tabP[q].length; i++) {
        aConteneurP[i].css({left:aPosX[i],top:aPosY[i]});
    }
    totalLx=78;
    for (i=0; i<tabPSol[q].length;i++) {
        aConteneurP[tabPSol[q][i]].css({left:totalLx,top:151});
        totalLx+=aConteneurP[tabPSol[q][i]].width();
    }
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    /*
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    */
    //
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeDifficulte",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[1,2,3,4,5]
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))