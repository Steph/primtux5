import os
import sys
import wx
import shutil

MAIN_WINDOW_DEFAULT_SIZE = (300,200)

class MyDialog(wx.Dialog):
 
    def __init__(self, parent, id, title):
        style=wx.DEFAULT_FRAME_STYLE ^ (wx.RESIZE_BORDER)   
        wx.Dialog.__init__(self, parent, id, title=title, size=MAIN_WINDOW_DEFAULT_SIZE, style=style)
        self.Center()
        self.panel = wx.Panel(self)
        self.panel.SetBackgroundColour('White')
        self.save_file()
 
    def OnExit(self, event):
        self.Destroy()
 
    def save_file(self):
        chemin = ""
        #filename = 'extrait%s'%ref
        filename = 'extrait.mp3'
        #print filename  ## Juste pour l'info lors du dev.
        wildcard = "Fichier MP3 (*.mp3)|*.mp3"
        selection = wx.FileDialog(self, 'Enregistrer sous', defaultFile = filename,  wildcard = wildcard, style = wx.SAVE)
        retour = selection.ShowModal()
        chemin = selection.GetPath()
        if os.path.isfile(chemin) == 1 :
            dlg = wx.MessageDialog(self, u'Ce projet existe d\xe9j\xe0. Voulez-vous le remplacer?', 'ATTENTION', wx.OK | wx.ICON_WARNING |wx.CANCEL)
            if dlg.ShowModal() != wx.ID_OK :
                chemin = ""
                return
        if chemin !="" :
            BASE_PATH = os.path.abspath(os.path.dirname(sys.argv[0]))
            sourcefile= os.path.join("../tempsound/temp.mp3")
            #print sourcefile
            #raw_input()
            shutil.copy2(sourcefile, chemin)
 
class App(wx.App):
 
    def OnInit(self):
        dia = MyDialog(None, -1, "Enregistrer sous")
        dia.Destroy()
        return True
 
if __name__ == "__main__":       
    app = App(redirect=False)
    #app = App(redirect=True, filename="wxerr.log")
    app.MainLoop()